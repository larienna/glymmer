@mainpage 
# Glymmer (Glyph Menu)

This project is a glyph based user interface displayed in graphic mode which should be mostly used in video games, but it could be used for other projects. It use regular ascii characters to create window outline and display choices to the user.

The project will be coded in plain C for portability and simplicity.

## Artifacts

The following artifacts are available for download or reading:

* [Demo](http://lariennalibrary.com/ArtifactDepot/glymmer/bin/)
* [Documentation](http://lariennalibrary.com/ArtifactDepot/glymmer/doc/)
* [Glyphs Bitmaps](http://lariennalibrary.com/ArtifactDepot/glymmer/glyph/)
* [Bitmap fonts](http://lariennalibrary.com/ArtifactDepot/glymmer/font/)


## Dependencies

The following libraries are required:

* Allegro (essential for now, until SDL is supported)
* Sqlite
* Cmake (to build the library)

## Introduction

Almost all user interface are event based, it required the program to react to use input which changes the flow of the program. Glymmer is a non-event based user interface that get things done by asking the user a series of questions. It relies on the fact that video games have little data entry but have a lot of operations to manipulate that data. You don't want the user to easily modify the game's data as it would be cheating. So there is no need to have input forms to add and modify data. Instead, it's only a series of choice that the user must make based on information displayed to them. 

Glymmer takes advantage of this and gain the following benefits:

* No need to handle events, the user choose where he wants to go once, then he is on a track.
* No need to define listener, the program flow remains normal, you ask a question and wait for an answer
* No need to handle notifications
* No need to use MVC structure and other object oriented absurdities
* No need for many widgets to create a window, everything is built by drawing text
* No need for custom crowded windows, you reuse many different dialogs to achieve what you want, but there is little need for custom code.
* No need of object oriented programming, Entity Component Ssystem  will be used internally alsmost transparently to the user.
* The interface is suitable for keyboard, mouse and joystick.
* Most of the work is done directly in the database

The disadvantages:

* Players are most of the time on track. They generally make a choice using a menu, then answer a series of questions to perform the operation.
* The same window structure will be reused over and over. Which could be boring after some time.
* Designed for video games, so little data to insert but lof of commands to execute. Could be problematic in other type of applications.
* It require and SQLite database for most of the operations. The library is limited without SQLite.

## Build Software

The build process has been simplified by using a script and cmake. Into the root folder, type

`./build.sh`

To run the script that will build the make files and the software. If successful, you can install the library and the demo by going into the `_build` folder and launching the installation as root.

```
cd _build
sudo make install
```

It should copy the .a library and the include files into the local directories. The demo program `evilair` will be copied at the root of the project. To use the library in your projects, simply add `-lglymmer` to your linker command line.

## Build Documentation

The documentation is made with doxygen, you simply need to type 

`doxygen Doxyfile`

to generate the documentation. The web site entry point will be located in :

`doc/html/index.html`

## Demo

The demonstration application used for functionnal testing. I allows exploring the various features of the library. It does not do anything in particular. You can launch it by typing in linux:

`./evilair`

You will need the following files that can be downloaded from my website. See links at the top of the documents:

```
GameIcon.bmp
GreenGold.bmp
wizardry10x15.bmp
```

Example programs are also available for learning purpose:

```
example01text
example02list
```
