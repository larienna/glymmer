/**
   Wizardry Legacy
   Wui.c

   @author: Eric Pietrocupo
   @date: december 29th, 2018
   @license: GNU general public license
*/

#include <allegro.h>
#include <allegrohelp.h>
#include <textoutpatch.h>
#include <stdbool.h>
#include <math.h>
#include <video.h>
#include <wui.h>
#include <syslog.h>
#include <system.h>
#include <assets.h>
#include <stdarg.h>
#include <stdio.h>
#include <sqlite3.h>
#include <sqlwrap.h>


/*-------------------------------------------------------------------------*/
/*-                         Private Global Variables                      -*/
/*-------------------------------------------------------------------------*/

s_wui_ecs_dialog dialogs[WUI_MAX_NB_DIALOG];
int wui_active_dialog = -1; //index that points on the last active dialog

//Variables for taking screen shots with various numbers
char shotstr [ 18 ];
int shotID = 0;






/*-------------------------------------------------------------------*/
/*-                        Private Methods                          -*/
/*-------------------------------------------------------------------*/








/*short WinMessage::show ( void )
{
   short xchar = p_x_pos + 10;
   short ychar = p_y_pos + 6;
   short length;
   short i;
   short font_width = text_length ( text_font, "a" );
   short font_height = text_height ( text_font );
   int inst_x_backup;
   int inst_y_backup;
   int inst_order_backup;
//   string tmpstr;
   //char tmpchr;

   clear_keybuf();
   border_fill ();

   length = strlen ( p_message );

   i = 0;
   while ( i < length && p_message [ i ] != '$' )
   {
      if ( p_message [ i ] == '\n' || p_message [ i ] == '^' )
      {
         xchar = p_x_pos + 10;
         ychar = ychar + font_height;
      }
      else
      {
//         tmpchr = p_message [ i ]; // change a character in a string
         textprintf_old ( buffer, text_font, xchar, ychar,
            General_COLOR_TEXT, "%c", p_message [ i ] );
         xchar = xchar + font_width;
      }
      i++;
   }

// remove temporarily until instruction fusioned with window
//   draw_instruction ( Draw_INSTRUCTION_SELECT, centerx, ychar + font_height );

   inst_x_backup = p_inst_center_x;
   inst_y_backup = p_inst_y;
   inst_order_backup = p_inst_order;

   p_inst_order = Window_INSTRUCTION_SELECT;
   p_inst_center_x = p_center_x;
   p_inst_y = ychar + font_height;
   draw_instruction();
   copy_buffer ();

   while ( mainloop_readkeyboard() != SELECT_KEY );

   blit ( buffer, p_backup, p_x_pos, p_y_pos, 0, 0, p_width, p_height );

   p_inst_center_x = inst_x_backup;
   p_inst_y = inst_y_backup;
   p_inst_order = inst_order_backup;


   return ( 0 );
}*/








/*void dialog_draw_update ( BITMAP *background )
{  static int c = 0;
   int instructions;
   c = modulo ( c+1, 10) ;

   //printf ("DEBUG: color = %d\n", c);
   int color = bgcolor [c];

   //printf ("DEBUG: Before blit bg\n" );
   if (background != NULL ) blit ( background, buffer, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   //printf ("DEBUG: after blit bg\n" );

   for ( int i = 0 ; i <= wui_active_dialog; i++)
   {  window_draw_background ( &dialogs[i].win , color );
      if ( i == wui_active_dialog && has_component( &dialogs[i], WUI_COMPONENT_NAVIGATION))
      {   instructions = dialogs[i].nav.instructions;
      }
      else instructions = 0;
      if ( i == wui_active_dialog)
      {  window_text_clear ( &dialogs[i].win );
         window_print_border ( &dialogs[i].win, instructions );
         dialog_print_content ( &dialogs[i] );
      }
      window_draw_text ( &dialogs[i].win, color );
   }
   copy_buffer();
}*/

/*-------------------------------------------------------------------*/
/*-                        Public Methods                          -*/
/*-------------------------------------------------------------------*/



void data_init ( s_wui_data *dat, char *format, char *query, bool multirecord )
{  dat->format = format;
   dat->query = query;
   dat->multirecord = multirecord;
}





int dialog_show ( BITMAP* background, char *text_answer, int *values )
{
   int answer;

   //initial dialog draw
   dialog_draw( background );


   clear_keybuf(); //clear one before the read_navigation_key
   //read navigation and update content
   if ( has_component( &dialogs[wui_active_dialog], WUI_COMPONENT_NAVIGATION))
   {  bool answer_found = false;

      while ( !answer_found )
      {  answer_found = read_navigation_key( &dialogs[wui_active_dialog], &answer);
         //TODO: Maybe loop through windows and set reprint if side windows requires data update

         if ( answer_found == false )
         {  dialogs[wui_active_dialog].win.reprint = true;
            dialog_draw( background );
         }
      }

   }
   else //there is no navigation, wait simply for a enter. Normally used only in case of errors
   {
      mainloop_readkeyboard();
      answer = -1;
   }

   return answer;
}


