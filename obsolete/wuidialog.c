/**
   Wizardry Legacy
   Wuidialog.h

   @author: Eric Pietrocupo
   @date: September 17th, 2019
   @license: GNU general public license
*/

#include <allegro.h>
#include <stdbool.h>
#include <wui.h>
#include <wuidialog.h>

/*
#define WUI_COMPONENT_MENU       0b00000010
#define WUI_COMPONENT_TEXT       0b00000100
#define WUI_COMPONENT_DATA       0b00001000
#define WUI_COMPONENT_INPUT      0b00010000
#define WUI_COMPONENT_NAVIGATION 0b00100000
*/

/*-----------------------------------------------------------------*/
/*-                       Private Content                         -*/
/*-----------------------------------------------------------------*/

//Predefined menus for certain dialogs

s_wui_menu_item menu_question [] = { { 0, "Yes" },
                                 { -1, "No" }
                                 };

/*--------------------------------------------------------------------------------*/
/*-                        Public Methods                                        -*/
/*--------------------------------------------------------------------------------*/

s_wui_ecs_dialog *dialog_open_empty ( char *title, int x, int y, int w, int h )
{  s_wui_ecs_dialog *dialog = dialog_open ( 0 );
   if ( dialog != NULL )
   {  window_init ( &dialog->win, title, x, y, w, h );

   }
   return dialog;
}

s_wui_ecs_dialog *dialog_open_background ( char *title )
{  return dialog_open_empty ( title, 0, 0, WUI_SCREEN_WIDTH, WUI_SCREEN_HEIGHT  );
}


s_wui_ecs_dialog *dialog_open_text ( char *title, int x, int y, int w, int h, char *message, bool wrap )
{  s_wui_ecs_dialog *dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_NAVIGATION);
   if ( dialog != NULL )
   {  navigation_init( &dialog->nav, 0, 1, WUI_INS_ACCEPT );
      window_init ( &dialog->win, title, x, y, w, h );
      text_init( &dialog->txt, &dialog->win, message, wrap );

   }
   return dialog;

}

s_wui_ecs_dialog *dialog_open_pagedtext ( char *title, int x, int y, int w, int h, char *message )
{  s_wui_ecs_dialog *dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_NAVIGATION);
   if ( dialog != NULL )
   {  window_init ( &dialog->win, title, x, y, w, h );
      text_init( &dialog->txt, &dialog->win, message, true );
      int page_size = h-2;
      int nb_lines = anticipate_printwrap_nb_lines( message, w );
      int nb_page = ( nb_lines / page_size );
      if ( nb_lines % page_size > 0) nb_page++;
      navigation_init ( &dialog->nav, page_size, nb_page, WUI_INS_ACCEPT | WUI_INS_LEFTRIGHT );
   }
   return dialog;
}


s_wui_ecs_dialog *dialog_open_menu ( char *title, int x, int y, char *message, s_wui_menu_item *items,
                                     int length )
{  s_wui_ecs_dialog *dialog = NULL;
   int w=2, h=2, tmpw=0;

   if ( items != NULL )
   {  if ( message == NULL) dialog = dialog_open ( WUI_COMPONENT_MENU | WUI_COMPONENT_NAVIGATION);
      else dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_MENU | WUI_COMPONENT_NAVIGATION);
      if ( dialog != NULL)
      {  navigation_init( &dialog->nav, length, 1, WUI_INS_ACCEPT | WUI_INS_CANCEL | WUI_INS_UPDOWN );
         menu_init ( &dialog->mnu, items, length );
         //calculate width and length using length of title, message or menu
         if (message != NULL)
         {  tmpw = strlen ( message) + 4; //add border and space before/after
            h++; //TODO: potential bug if a \n is inserted in the question.
                 // could count the number of \n or ^ in the string.
                 //Maybe used wrapped text instead with anticipation of nb of lines
                 //OR add a flag to printf to interpret or not the \n char.
                 //could make 2 wrapper routines for a flag.
         }
         if ( tmpw > w ) w = tmpw;
         if ( title != NULL) tmpw = strlen (title) + 4;
         if ( tmpw > w ) w = tmpw;

         for ( int i = 0; i < dialog->mnu.length ; i++ )
         {  tmpw = strlen ( dialog->mnu.items[i].text ) + 3; //cursor + border
            if ( tmpw > w ) w = tmpw;
            h++;
         }

         window_init ( &dialog->win, title, x, y, w, h );

         if ( message != NULL ) text_init( &dialog->txt, &dialog->win, message, false );
      }
   }
   return dialog;
}

s_wui_ecs_dialog *dialog_open_list ( char *title, int x, int y, int w, int h, char *message, s_wui_menu_item *items,
                                    int length )
{  s_wui_ecs_dialog *dialog = NULL;
   int page_size = h - 2;

   if ( items != NULL )
   {  if ( message == NULL) dialog = dialog_open ( WUI_COMPONENT_MENU | WUI_COMPONENT_NAVIGATION);
      else
      {  dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_MENU | WUI_COMPONENT_NAVIGATION);
         page_size = h - 3;
      }

      if ( dialog != NULL)
      {  int nb_page = length / page_size;
         if ( length % page_size > 0 ) nb_page++;
         navigation_init( &dialog->nav, page_size, nb_page,
                         WUI_INS_ACCEPT | WUI_INS_CANCEL | WUI_INS_UPDOWN | WUI_INS_LEFTRIGHT );
         menu_init ( &dialog->mnu, items, length );

         window_init ( &dialog->win, title, x, y, w, h );

         if ( message != NULL ) text_init( &dialog->txt, &dialog->win, message, false );
      }
   }
   return dialog;
}

s_wui_ecs_dialog *dialog_open_database_menu ( char *title, int x, int y, char *header,
                                             char *format, char *query )
{  s_wui_ecs_dialog *dialog = NULL;
   int w=2, h=2, tmpw=0;

   if ( header == NULL)
   {  dialog = dialog_open ( WUI_COMPONENT_DATA | WUI_COMPONENT_NAVIGATION);
   }
   else dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_DATA | WUI_COMPONENT_NAVIGATION);
   if ( dialog != NULL)
   {  //TODO: calculate length
      //navigation_init( &dialog->nav, length, 1, WUI_INS_ACCEPT | WUI_INS_CANCEL | WUI_INS_UPDOWN );
      data_init ( &dialog->dat, format, query, true );
      //calculate width and length using length of title, message or menu
      if (header != NULL)
      {  tmpw = strlen ( header) + 4; //add border and space before/after
         h++; //TODO: potential bug if a \n is inserted in the question.
              // could count the number of \n or ^ in the string.
              //Maybe used wrapped text instead with anticipation of nb of lines

      }
      if ( tmpw > w ) w = tmpw;
      if ( title != NULL) tmpw = strlen (title) + 4;
      if ( tmpw > w ) w = tmpw;

      //TODO: Calculate the width and height according to a test querying
      /*for ( int i = 0; i < dialog->mnu.length ; i++ )
      {  tmpw = strlen ( dialog->mnu.items[i].text ) + 3; //cursor + border
         if ( tmpw > w ) w = tmpw;
         h++;
      }*/

      window_init ( &dialog->win, title, x, y, w, h );

      if ( header != NULL ) text_init( &dialog->txt, &dialog->win, header, false );
   }

   return dialog;
}
s_wui_ecs_dialog *dialog_open_database_list ( char *title, int x, int y, int w, int h,
                                             char *header, char *format, char *query )
{  s_wui_ecs_dialog *dialog = NULL;
   int page_size = h - 2;

   if ( header == NULL)
   {  dialog = dialog_open ( WUI_COMPONENT_NAVIGATION | WUI_COMPONENT_DATA);
   }
   else
   {  dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_NAVIGATION | WUI_COMPONENT_DATA);
      page_size = h - 3;
   }

   if ( dialog != NULL)
   {  //TODO: Calculate length
      //int nb_page = length / page_size;
      int nb_page = 1;
      //if ( length % page_size > 0 ) nb_page++;
      navigation_init( &dialog->nav, page_size, nb_page,
                      WUI_INS_ACCEPT | WUI_INS_CANCEL | WUI_INS_UPDOWN | WUI_INS_LEFTRIGHT );
      data_init ( &dialog->dat, format, query, true );

      window_init ( &dialog->win, title, x, y, w, h );

      if ( header != NULL ) text_init( &dialog->txt, &dialog->win, header, false );
   }

   return dialog;
}



s_wui_ecs_dialog *dialog_open_question ( char *title, char *message )
{  s_wui_ecs_dialog *dialog = NULL;
   int x = 1;
   int y=WUI_SCREEN_HEIGHT/3;
   int w=WUI_SCREEN_WIDTH-2;
   int h= 4 + anticipate_printwrap_nb_lines(message, w);

   dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_MENU | WUI_COMPONENT_NAVIGATION);
   if ( dialog != NULL )
   {  navigation_init( &dialog->nav, 2, 1, WUI_INS_ACCEPT | WUI_INS_CANCEL | WUI_INS_UPDOWN );
      menu_init ( &dialog->mnu, menu_question, 2 );

      window_init ( &dialog->win, title, x, y, w, h );

      text_init( &dialog->txt, &dialog->win, message, true );
   }
   //TODO: BUG the text also scroll, will need a different cursor for text position.
   return dialog;
}

s_wui_ecs_dialog *dialog_open_message ( char *title, char *message )
{  s_wui_ecs_dialog *dialog = NULL;
   int x = 1;
   int y=WUI_SCREEN_HEIGHT/3;
   int w=WUI_SCREEN_WIDTH-2;
   int h= 4 + anticipate_printwrap_nb_lines(message, w);

   dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_NAVIGATION);
   if ( dialog != NULL )
   {  navigation_init( &dialog->nav, 0, 1, WUI_INS_ACCEPT );
      window_init ( &dialog->win, title, x, y, w, h );
      text_init( &dialog->txt, &dialog->win, message, true );
   }

   return dialog;

}




/*void dialog_init_menu_from_constant ( s_wui_dialog *dialog, s_wui_dialog_menu_item *items )
{

}

void dialog_dispose ( s_wui_dialog *dialog)
{

}*/
