/**

   Wizardry Legacy
   Wui.h

   @author: Eric Pietrocupo
   @date: december 29th, 2018
   @license: GNU general public license

   This is the new gui called WUI: Wizardry User Interface which consist in a
   series of questions and menu. This system is one of the only non-event
   based programming GUI and will serve to create other non-event oriented
   programming GUI.

   The primary objective is to uncplusplusify the current GUI which currently
   use classes. The secondary objective if to reduce the amount of modules by
   combining the menu and the window together. Using stream of characters to
   display generic data without the need to manually position text. The third
   object is to simplify the manual building of menu by simply passing a
   constant array of menu items. The fourth objective is to simplify the
   management of the GUI by preventing the need to backup video buffers for
   each window and make it almost ready for realtime display. For for now, I
   don't expect to support realtime blitting, but the system design could be
   reused to create one.

   This file will contains the definitions common to the whole GUI and
   the documentation and notes that explains how the GUI system works. It will
   allow indirectly to learn more about how non-event bassed gui can be built.

   UNDER CONSTRUCTION: I am changing my implementation by using ECS approach
   instead of an object approach. Everything will be contained in a huge structure.
   List of items will allocate each field dynamically up to the size of the list.


*/

#ifndef WUI_H_INCLUDED
#define WUI_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/**
   ----- Introduction -----

   The existence of non-event based programming rely on the fact that video
   games has little data entry. It's only a series of choice that the user
   must make based on information displayed to them. So you'll never see long
   input forms in a video game.

   We can take advantage of this to build a GUI that will not require events.
   This can lead to many benefits:

   - No need to define listener, or almost
   - No need to handle notifications
   - No need to MVC structure
   - No need for many widgets to create a window
   - No need for custom windows, you reuse the standard window items
   - No need of object oriented programming, ECS will be used instead.
   - memory pre-allocation will be used to avoid mallocs
   - friendly for keyboard, mouse and joystick support.

   The disadvantages could be:

   - Players are most of the time on track. They generally make a choice using
     a menu, then answer a series of questions to perform the operation.
   - The same window structure will be reused over and over. Which could be
     boring after some time.
   - Designed for video games, so little data to insert but lof of commands to execute.

   Most of the content of this file will be used to build and manage the dialog. But when used
   from the game, the wuidialog.h will containe the methods used to manage the wui. So the methods
   in this file are more like internal methods.

*/


/** ----- ECS Dialogs -----
   The wui system will use an entity component system that will be preallocated to a fixed
   size defined by WUI_MAX_NB_DIALOG and allocated as a static array. Each dialog will be
   constained in a structure composed of multiple substructure. Not all of them will be used
   at the same time. A bitfield will be used to determine if a substructure is used or not
   since pointers are not used because there is no dynamic allocation.
*/



/** ----- Navigation Component -----
   Menu and scrolling text requires a form of navigation using the arrow keys. So information
   about selector position, pages, etc needs to be recorded to be remembered between each user input.
*/


/** ----- Items Component -----
   If the dialogs is composed of a series of static choices, then menu will have to be defined.
   They are used by menu, list, distributors, questions, etc. The list of items will have to be
   defined statically as a constant, and a pointer to that constant array will be used.Long list
   will use multipage display, which means that only a limited number of choice will be loaded at
   the same time.

   If you are looking for dynamic content, you can useuse a database component that will query
   and format choices from a DB. They are both mutually exclusive. Or you could allocated a menu
   dynamically and free it by yourself. Since I am only using a pointer, the API does not care
   if the source is dynamic or static.
*/

/** To determine the length of the menu, a macro can be used since once a contant array is
    converted as a pointer, the original size cannot be recovered anymore
*/

#define SIZEOFMENU(MENU) (sizeof((MENU)) / sizeof (s_wui_menu_item) )
//TODO: use NULL terminated aray to avoid manually passing the the list length


/** ----- Text Component -----
   The text component can either be scrollable text or text printed before any choices.
   The text could be printed with wrapped option, else only the first line is drawn.
   Navigation components could be used to change the starting point, or change the page
   position of the text to display.
*/

/** ----- Data Component -----
    The data component is a very important component that links the content printed in the
    WUI with information queried from the database. There is 2 type of information pull from
    the DB: Multiple records, where each record is printed on a line. Single records where
    only the first record is printed on a line. Most of the time, you need to use carriage
    returns in the format to make sure the information is distributed on a separate line.
    This is why a format needs to be supplied, like used in printf, that will be used by
    the window_text_printf() method internally, so color codes in the format are valid.
    The SQL data will be put into a vector that will be sent to be printed on the screen.
    If the output is larger than the window, it will simply be cropped. So the window character
    buffer will act as a targer buffer space, so no need to use snprint() routines.
*/

typedef struct s_wui_data
{  char *format; //printf format to print on the screen, should contain constants
   char *query; //SQL query to use, should be constant, use bound parameters instead of dynamic allocated string
   bool multirecord; //set to true if more than 1 record must be displayed

}s_wui_data;
//TODO: add bound to active window, 1 parameter bind.

void data_init ( s_wui_data *dat, char *format, char *query, bool multirecord );

/** Then when the dialogs are built, it is time to show them to the user. The show method
    will display the dialog to the user, handle the input and wait for an answer. The show
    method will only return when a choice has been made. It will make necessary updates due
    to menu navigation.

    There will be various return value methods, this is why the function returns a key, but
    has also various parameters passed in reference to hold the answer. Set those variables
    to NULL if no answer is wanted.

    Show also requires a background BITMAP to display under the window stack. This is to
    accommodate the need of drawing windows over the dungeon or city areas, etc.
*/
//TODO included inside the engine
int dialog_show ( BITMAP* background, char *text_answer, int *values );

/** In certain rare situation, you just want to copy the whole stack of window on the
    screen buffer, but you do not want input to be handled. In this case, you can use
    dialog_draw() which is also used internally by dialog_show
*/
//TODO included inside the engine
void dialog_draw ( BITMAP *background );






/*
   NOTES to consider

OK - Remove the cascading system
OK - Fusion Menu and List together, no linked chains anymore
OK - Not sure if fusion all dialog together: Message, question, input. They
     share a lot in common besides the input method.
OK - Empty and title could be fusionned together. In fact they reuse empty data stream window
OK _ Maybe titles could be available for all windows. Title is now part of the frame
 OK- Create a text stream window instead of custom data methods. Handle everything as text
 OK- Use window size and positionning as characters coordinates rather than pixel.
     especially for the content inside the window.
OK - Allow the use of constant item list to build up menu. See if I can get length of array.
      could use an varlength array, must be null terminated.
?  - Could even allow the preconstruction of menu trees by holding a pointer to other menu
     array.
   - List builders can be separate from list. Allowing build from constant, dynamic, database.
     If use recordset, Could greatly simplify the display of recordset as list.
   - Certain window might depend on the cursor of the selected list item. Might need
     pointer to window item that contains the current selected choice. Not sure
     if the data of both window will be in a recordset or if the DB is constantly
     requeried after each cursor movement.
OK - Instructions are very problematic, maybe they should be positionned inside the window
     if user input is required. Could be put into the bottom frame of the window
     to reduce the number of pixels required to display the instructions. They are
     also only show on the top active window. (use small icons and a box |____| in bottom border
OK - Instruction display type:
      1- Line always at the bottom of the screen in a thin frame.
      2- Small icons inside the bottom frame: Arrow, letter, but no description
        Arrow, AZX, + - (page up/down) Carriage Return arrow
      3- Font will have custom characters to display instructions in border
OK - The display of the maze under the windows should be the only exception. Still,
     there should be no backup of the display, instead the maze will be draw after each
     movement of the cursor. Hope it's not too slow.
OK - Idea: Group windows together as a screen/form. Allow activating/disabling them at once.
     When swithich screen. So there will be a screen stack and a window stack. A layer ID will
     be used, and the last window in the start will be active
OK - I am wondering if textured window is really interesting, seems to disturb reading. REMOVE

*/

#ifdef __cplusplus
}
#endif



#endif // WUI_H_INCLUDED
