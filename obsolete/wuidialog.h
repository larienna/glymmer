/**
   Wizardry Legacy
   wuidialog.h

   @author: Eric Pietrocupo
   @date: January 17th, 2019
   @license: GNU general public license

   This is the new dialog system for the wizardry user interface using
   only plain C and the new textbased window system. I'll try to group
   all dialog here and reduce redundancy. Look at wui.h for more details.

*/
#ifndef WUIDIALOG_H_INCLUDED
#define WUIDIALOG_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/** The dialogs are actually functions that initialise the data structure in different
    ways. The systems of the WUI will make them behave according to the set parameters.
    It is possible to create your own configured dialog within the features of the WUI
    using dialog_open(), but the dialog routines in this file will simplify the process.
    This is the routines that should be used during the game for building the user interface.
    They are the main entrance door. All dialog functions return a pointer on the dialog but
    it is rarely needed, it's there just in case. It's in fact a pointer on the array item,
    so do not call free on this pointer. Everything is pre_allocated here.

    Once all the dialogs are created, you call dialog_show() and wait for your answer. The dialogs
    below are simple dialog that does not have nagigation.
*/

s_wui_ecs_dialog *dialog_open_empty ( char *title, int x, int y, int w, int h );
s_wui_ecs_dialog *dialog_open_background ( char *title );
s_wui_ecs_dialog *dialog_open_text ( char *title, int x, int y, int w, int h, char *message, bool wrap );
s_wui_ecs_dialog *dialog_open_pagedtext ( char *title, int x, int y, int w, int h, char *message );


/* This function series is used for list and menus. Menu can be constant array or menu items
   or it can be declared dynamically. But for real dynamic content menu and list, it is
   recommented to load from a database. Since an simple dynamic array can be used if the data
   is not available in the database.

   Menus will ajust itselt to it's size and will not have paging. While list will draw a fixed
   size, but will allow paging. The length is the number of items in the menu. You should use
   the SIZEOFMENU() macro if no constants or variables are available.
*/

s_wui_ecs_dialog *dialog_open_menu ( char *title, int x, int y, char *message, s_wui_menu_item *items,
                                    int length );
s_wui_ecs_dialog *dialog_open_list ( char *title, int x, int y, int w, int h, char *message, s_wui_menu_item *items,
                                    int length );

/** The variant below introduce the possibility to create list and menus from the content of
    a database. The information will have to be requeried after each cursor movement since to
    simplify buffer management, the window character buffer will be used. That prevents using
    dynamic memory allocation. Those methods requires a format and a query. All records will
    be printed 1 by line. Overflow will not be printed. Make sure not to use carriage return
    characters as it could screw up the display. Header is a kind of message string but it is
    limited to a single line again not to screw up the display. Make sure the number of fields
    match the nb of variables in the printf format, else it will crash.
*/

s_wui_ecs_dialog *dialog_open_database_menu ( char *title, int x, int y, char *header,
                                             char *format, char *query );
s_wui_ecs_dialog *dialog_open_database_list ( char *title, int x, int y, int w, int h,
                                             char *header, char *format, char *query );



/* These are simple dialogs to ask questions or show messages on the screen. They have a fixed
   width which is centered on the screen. Text will be wrapped appropriately. This is why no
   sizes are required
*/

s_wui_ecs_dialog *dialog_open_question ( char *title, char *message );
s_wui_ecs_dialog *dialog_open_message ( char *title, char *message );

//TODO: List (can rename paged menu), question

//TODO: Menu and list based on database content. Try non DB dialogs first to test in game




//TODO include in this file only dialog management routines
// they will set the ECS parameter appropriately according to the kind of display they want
// they will setup a routine to handle answers or input. Still most navigation input could be
// handled internally with proper instructions.


//--- functions --

//TO REDEFEINE
/*void dialog_init_menu_from_constant ( s_wui_dialog *dialog, s_wui_dialog_menu_item *items );
void dialog_print ( s_wui_dialog *dialog, s_wui_window *window );

void dialog_dispose ( s_wui_dialog *dialog);*/

#ifdef __cplusplus
}
#endif


#endif // WUIDIALOG_H_INCLUDED
