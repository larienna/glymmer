#!/bin/bash
BINDIR=/mnt/build/glymmer
SRCDIR=`pwd`

#create directory if not exist (on the ramdisk)
mkdir -p $BINDIR

#remove any files previously created
#ln -s /mnt/build/fxlauncher _buildtmp
rm -rf $BINDIR/*

#build the project with cmake
cd $BINDIR && cmake $SRCDIR

#build all targets
make

