#!/bin/bash

#create directory if not exist
mkdir -p _build_mingw/win32

#remove any files previously created
cd _build_mingw/win32 && rm -r *

#build the project with cmake
cmake ../.. -DCMAKE_TOOLCHAIN_FILE=../../win32cc.cmake

#build all targets
make

