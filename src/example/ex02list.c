/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: August 14th, 2022
   @license: Apache 2.0 License

   This example shows how to create various kinds of list without the need of a database.

*/

#include <stdio.h>
#include <stdbool.h>
#include <allegro.h>
#include <glymmer.h>
#include <excommon.h>

void static_list_dialog_example ( gms_glymmer *gm )
{  //We are creating a large list of fruits with colors
   gms_item fruits [] =        { { 0, 0, "\vFTomato" },
                                    { 1, 0, "\vDBanana" },
                                    { 2, 0, "\vFStrawberry" },
                                    { 3, 0, "\vFRaspberry" },
                                    { 4, 0, "\vEMango" },
                                    { 5, 0, "\vFCherry" },
                                    { 6, 0, "\vEPapaya" },
                                    { 7, 0, "\vJPear" },
                                    { 8, 0, "\vDStarfruit" },
                                    { 9, 0, "\vJKiwi" },
                                    { 10, 0, "\vdPineapple" },
                                    { 11, 0, "\veOrange" },
                                    { 12, 0, "\vjGrape" },
                                    { 13, 0, "\vfApple" },
                                    { 14, 0, "\vdLemon" },
                                    { 15, 0, "\vjLime" },
                                    { 16, 0, "\vhBlueberry" },
                                    { 17, 0, "\vfCranberries" },
                                    { -1, 0, "Cancel" },
                                    { 0, 0, NULL } //terminator
                                    };

   //Creating a small list of vegetables
   gms_item vegetables [] =     {   { 0, 0, "\vECarrots" },
                                    { 1, 0, "\vgSugar beet" },
                                    { 2, 0, "\vBCauliflower" },
                                    { 3, 0, "\vJBrocoli" },
                                    { 4, 0, "\vAMushrooms" },
                                    { 5, 0, "\vJZucchini" },
                                    { 0, 0, NULL} //terminator
                                 };

   //Create a background window to cover the screen
   gm_dialog_open_background(gm, "Static List examples");

   //Create a regular list of vegetables in the background
   gm_dialog_open_list (gm, "Regular list", 1, 1, "Vegetables", vegetables );

   //Create a paged list of fruits on the top
   gm_dialog_open_pagedlist ( gm, "Paged list", 10, 10, 20, 8, "Fruits", fruits );

   int answer;
   answer = gm_show_dialogs( _bg, gm);

   printf ("The first answer selected was %d\n", answer );

   gm_dialog_close_all( gm);

}

void dynamic_list_dialogs_example ( gms_glymmer *gm )
{  //creating a background dialog not covering the entire screen. Will make _bg getting displayed
   gm_dialog_open_empty(gm, "Dynamic List example", 0, 0, 50, 30);

   //Create a dynamic page list that will hold a list of filres
   gms_dialog *dialog = gm_dialog_open_dynamic_pagedlist( gm, "File list", 1, 1, 40, 10, "Please chose a file from the list");

   //_____ Read file list and populate the dialog _____

   //I am using Allegro structures and routines for file reading
   struct al_ffblk info;
   int key = 0;

   //if no files can be found, exit the program
   if (al_findfirst("*", &info, 0) != 0)
   {  printf ("ERROR: Cannot find any files\n");
      exit(1);
   }

   do //add the name of the file to the dynamic list dialog
   {  gm_list_item_add( gm, dialog, key, 0, info.name );
      key++;
   }
   while (al_findnext(&info) == 0); //while there is still files available

   al_findclose(&info);

   //_____ End read file list _____

   int answer;
   answer = gm_show_dialogs( _bg, gm);

   printf ("The second answer selected was %d\n", answer );

   gm_dialog_close_all( gm); //optional since we are at the end of the program
}



int main ( int argc, char *args[])
{  //Group all game library initialisation here to unclutter the code
   initialise_library();

   //create the glymmer data structure which hold all the engine information
   gms_glymmer gm;

   /*Initialise the data structure with speficied value.
      gm: the structure to initialise
      _fnt: a global variable font loaded during initialise_library();
      16: The designed initial size of the dialog stack. The stack is auto extended if exceeding.
      GM_INPUT_KEYBOARD: Input system desired to be used.
   */
   //TODO: Add glyph bitmap
   gm_initialise( &gm, _fnt, NULL, NULL, GM_INPUT_KEYBOARD, &_color_s,SCREEN_W, SCREEN_H );

   static_list_dialog_example( &gm);
   dynamic_list_dialogs_example( &gm);

   //Closing the Glymemr engine to dispose resources
   gm_dispose( &gm);

   //Closing the game library to dispose resources and return to text mode
   exit_library();
   return 0;
}
END_OF_MAIN() //specific to Allegro GL, helps convert main() to winmain()



