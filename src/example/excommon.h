/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: August 14th, 2022
   @license: Apache 2.0 License

   Contains definitions common to all examples to avoid redundancy of code. It should
   also unclutter the examples with library specific code.

*/

#ifndef EXCOMMON_H_INCLUDED
#define EXCOMMON_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

extern FONT* _fnt; // text font
extern BITMAP *_bg; // background acting as double buffer
extern BITMAP *_glyph; // Glyph set to display the boxes

extern gms_color_scheme _color_s;

void initialise_library ( void );
void exit_library ( void );

#ifdef __cplusplus
}
#endif

#endif // EXCOMMON_H_INCLUDED
