/**
   assets.c

   @author: Eric Pietrocupo
   @date:   December 3st, 2018
   @license: GNU general public license
*/

#include <allegro.h>
#include <stdbool.h>
#include <stdio.h>
#include <syslog.h>
#include <system.h>
#include <video.h>
//#include <datafileindex.h>
#include <assets.h>
#include <allegrohelp.h>
#include <textoutpatch.h>
#include <config.h>

/*--------------------------------------------------------------------*/
/*-                      Private Constants                           -*/
/*--------------------------------------------------------------------*/

#define DATBMP_EDITOR   1
#define DATBMP_IMAGE    2
#define DATFNT_FONT     3
#define DATMID_MUSIC    4
#define DATWAV_SOUND    7
//public portions defined in asset.h

#define NB_DATAFILE  16


/*--------------------------------------------------------------------*/
/*-                       Private structure                          -*/
/*--------------------------------------------------------------------*/


//internal structure used to keep information about datafiles
//this allow loading files sequentially with a loop instead of manually
typedef struct s_assets_datafilelist_item
{
   DATAFILE *datf; // pointer on the datafile to be loaded
   const char filename [ 51 ];
   bool loaded;
   const char display_text [ 101 ]; //the spacing is normal, it's to make sure previous text is overwritten
   //int filesize;
}s_assets_datafilelist_item;


/*--------------------------------------------------------------------*/
/*-                      Private variables                           -*/
/*--------------------------------------------------------------------*/

//default assets to be return in case of a missing asset or wrong file name.
//call build_default_assets to initialise.
BITMAP *default_256;
BITMAP *default_160;
BITMAP *default_16;

//this data structure usage is a bit weird, it contains contant information
//and variable information. It is modified during the loading
s_assets_datafilelist_item datafilelist [ NB_DATAFILE ] =
{
    { NULL, "datafile//monster.dat", false, " Loading monster pictures" },
    { NULL, "datafile//editor.dat", false,  "Loading map editor images" },
    { NULL, "datafile//image.dat", false,   "      Loading Images     " },
    { NULL, "datafile//font.dat", false,    "      Loading Fonts      " },
    { NULL, "datafile//music.dat", false,   "   Loading MIDI music    " },
    { NULL, "datafile//object.dat", false,  "  Loading Maze Objects   " },
    { NULL, "datafile//texture.dat", false, "  Loading Maze Textures  " },
    { NULL, "datafile//sound.dat", false,   "     Loading Sounds      " },
    { NULL, "", false, "" },
    { NULL, "", false, "" },
    { NULL, "", false, "" },
    { NULL, "", false, "" },
    { NULL, "", false, "" },
    { NULL, "", false, "" },
    { NULL, "", false, "" },
    { NULL, "", false, "" }
};


/*--------------------------------------------------------------------*/
/*-                        Private Methods                           -*/
/*--------------------------------------------------------------------*/

/* // to be used if a loading bar is required
void load_datafiles__caculate_size( void)
{   datafile_total_size = 0;
   double progress = 2;
   double chunk = 0;
   //int eval = 0;

   for ( i = 0 ; i < System_NB_DATAFILE ; i++)
   {
      if ( strcmp ( datafilelist [ i ] . filename , "") != 0 )
      {
         datafilelist [ i ] . filesize = file_size_ex ( datafilelist [ i ] . filename);
         datafile_total_size += datafilelist [ i ] . filesize;
         //printf ("Sizing %s\n", datafilelist [ i ].filename );
         //printf ("total size is=%d, File Size=%d\n", total_size, datafilelist [ i ] . filesize );
         //printf ( "error number: %d\n", errno );

      }
   }

}*/

void build_default_assets_prepare_bitmap ( BITMAP *bmp )
{  clear_bitmap (bmp);
   int color = makecol (255, 255, 255 );
   rect ( bmp, 0, 0, bmp->w-1, bmp->h-1, color );
   line ( bmp, 0, 0, bmp->w-1, bmp->h-1, color );
   line ( bmp, 0, bmp->h-1, bmp->w-1, 0, color );
}

/*--------------------------------------------------------------------*/
/*-                        Public Methods                            -*/
/*--------------------------------------------------------------------*/

bool load_game_datafiles ( void )
{
   clear ( screen );

   textout_centre_old ( screen, fg_font,
      "Prepare yourself for the ultimate fantasy game!",
      320, 230, General_COLOR_TEXT );

   textprintf_centre_old ( screen, fg_font, 320, 360, General_COLOR_TEXT,
      "Wizardry Legacy  v%s  %s", VERSION, YEAR );

   text_mode_old ( 0 ); //disable transparency

   //restore if need a loading bar
   //rect ( screen, 218, 398, 421, 416, General_COLOR_TEXT );

   textout_centre_old ( screen, fg_font, "Loading Datafiles, please wait ..." , 320, 440, General_COLOR_TEXT );

   for ( int i = 0 ; i < NB_DATAFILE ; i++ )
   {  if ( strcmp ( datafilelist [ i ] . filename, "") != 0 )
      {  textout_centre_old ( screen, fg_font, datafilelist[i].display_text , 320, 460, General_COLOR_TEXT );
         datafilelist [i].datf = load_datafile ( datafilelist [i].filename );
         //printf ("DEBUG: Loading %s\n", datafilelist [ i ].filename );
         if ( datafilelist [i] . datf == NULL )
         {  text_mode_old ( -1 );
            return false;
         }


         //printf ("total size is=%d", total_size);
         //commented code is used in case loading bars are required again
         /*chunk = ( datafilelist [ i ] . filesize * 200 ) ;
         chunk = chunk / datafile_total_size;
         progress += chunk;*/

         //progress++;
         //eval = ( progress * 200 ) / total_size;
         //if ( eval > drawval )
         //{
            //drawval = eval;

         //rectfill ( screen, 219, 399, 219 + (int) progress, 415, makecol ( 225, 175, 0 ) );
         //rest (1000);
         //}

         datafilelist [ i ] . loaded = true;
      }

   }

   text_mode_old ( -1 );
   return true;

}

void unload_game_datafiles ( void )
{
   for ( int i = 0 ; i < NB_DATAFILE ; i++ )
   {
      if (  datafilelist [ i ] . loaded == true )
      {
         unload_datafile ( datafilelist [ i ] . datf );
         datafilelist [ i ] . loaded = false;
      }
   }

}

bool build_default_assets ( void )
{  default_256 = create_bitmap ( 256, 256 );
   default_160 = create_bitmap ( 160, 160 );
   default_16 = create_bitmap ( 16, 16 );

   if ( default_160 == NULL || default_16 == NULL|| default_16 == NULL ) return false;

   build_default_assets_prepare_bitmap( default_256);
   build_default_assets_prepare_bitmap( default_160);
   build_default_assets_prepare_bitmap( default_16);

   return true;
}

void free_default_assets ( void )
{  destroy_bitmap( default_256);
   destroy_bitmap( default_160);
   destroy_bitmap( default_16);
}

void print_loaded_datafiles ( void )
{  for ( int i = 0 ; i < NB_DATAFILE ; i++ )
   {  if ( strcmp ( datafilelist [ i ] . filename, "") != 0 )
      {  if ( datafilelist [ i ] . loaded )
         {  printf ("    Datafile: [%d] %s loaded\n", i, datafilelist [i].filename);
         }
         else
         {  printf ("    Datafile: [%d] %s NOT LOADED\n", i, datafilelist [i].filename );
         }
      }
   }
}


BITMAP *asset_monster ( const char *name )
{  DATAFILE *entry = find_datafile_object( datafilelist [DATBMP_MONSTER].datf, name );
   if ( entry != NULL ) return (BITMAP*)entry->dat;
   printf ("ASSET: Could not find monster with name '%s'\n", name);
   return default_160;
}

BITMAP *asset_texture ( const char *name )
{  DATAFILE *entry = find_datafile_object( datafilelist [DATBMP_TEXTURE].datf, name );
   if ( entry != NULL ) return (BITMAP*)entry->dat;
   printf ("ASSET: Could not find texture with name '%s'\n", name);
   return default_256;
}

BITMAP *asset_editor  ( const char *name )
{  DATAFILE *entry = find_datafile_object( datafilelist [DATBMP_EDITOR].datf, name );
   if ( entry != NULL ) return (BITMAP*)entry->dat;
   printf ("ASSET: Could not find icon with name '%s'\n", name);
   return default_16;
}

BITMAP *asset_image   ( const  char *name )
{  DATAFILE *entry = find_datafile_object( datafilelist [DATBMP_IMAGE].datf, name );
   if ( entry != NULL ) return (BITMAP*)entry->dat;
   printf ("ASSET: Could not find image with name '%s'\n", name);
   return default_16;
}

BITMAP *asset_object  ( const char *name )
{  DATAFILE *entry = find_datafile_object( datafilelist [DATBMP_OBJECT].datf, name );
   if ( entry != NULL ) return (BITMAP*)entry->dat;
   printf ("ASSET: Could not find object with name '%s'\n", name);
   return default_16;
}

//TODO: Not sure if need an additional NULL check here for the BITMAP or FONT pointer
FONT *asset_font ( const char *name )
{  DATAFILE *entry = find_datafile_object( datafilelist [DATFNT_FONT].datf, name );
   if ( entry != NULL ) return (FONT*)entry->dat;
   printf ("ASSET: Could not find font with name '%s'\n", name);
   return fg_font;
}

void asset_play_music ( const char *name, bool loop )
{
   //temporary list of wizardry song to prevent the need of configuration
 //  static const int w2songs[] = { 22, 14, 15, 21, 23, 24, 25, 18, 16, 19 };
   int intloop = 0;
   if ( loop ) intloop = 1; // Set to 1 for looping
   DATAFILE *entry = find_datafile_object( datafilelist [DATMID_MUSIC].datf, name );
   if ( entry != NULL )
   {  play_midi ( (MIDI*)entry->dat , intloop );
   }
   else printf ("ASSET: Could not find music with name '%s'\n", name);
}

void asset_play_sound ( const char *name )
{  DATAFILE *entry = find_datafile_object( datafilelist [DATWAV_SOUND].datf, name );
   if ( entry != NULL )
   {  play_sample ( (SAMPLE*)entry->dat, config.audio_volume, 128, 1000, 0 );
   }
   else printf ("ASSET: Could not find sound with name '%s'\n", name);
}

BITMAP* asset_default_bitmap256 ( void )
{  return default_256;
}
BITMAP* asset_default_bitmap160 ( void )
{  return default_160;
}
BITMAP* asset_default_bitmap16 ( void )
{  return default_16;
}

void init_iterator ( s_datafile_iterator *iter, int datafileid )
{  iter->datafileid = datafileid;
   iter->index = 0;
}

BITMAP* get_iterator_asset ( s_datafile_iterator *iter )
{  if (iter->datafileid >= NB_DATAFILE || iter->datafileid < 0 ) return default_16;

   DATAFILE *dat = datafilelist [iter->datafileid].datf;
   if ( dat[iter->index].type == DAT_END ) return default_16;

   return (BITMAP*) dat[iter->index].dat;
}

bool inc_iterator ( s_datafile_iterator *iter )
{  if (iter->datafileid >= NB_DATAFILE || iter->datafileid < 0 ) return NULL;

   DATAFILE *dat = datafilelist [iter->datafileid].datf;
   if ( dat[iter->index].type != DAT_END )
   {   iter->index++;
      return true;
   }
   return false;
}

BITMAP* asset_bitmap_byindex ( int datafileid, int index )
{  if (datafileid >= NB_DATAFILE || datafileid < 0 ) return NULL;

   BITMAP *tmpbmp = (BITMAP*) datafilelist[datafileid].datf[index].dat;
   if ( tmpbmp == NULL ) return default_16;
   return tmpbmp;

}

//TODO: quick fix
FONT *text_font;

//////////////////////////////////// OLD CODE //////////////////////////////////////

//DATAFILE *datfaudio; // Audio datafile
//DATAFILE *datfmaze; // Maze textures datafile
//DATAFILE *datfimage; // Image datafile
//DATAFILE *datfennemy; // Ennemy Images
//DATAFILE *datfeditor; // Editor datafile
//DATAFILE *datffont; // font datafile
//DATAFILE *adatf; // main datafile
//DATAFILE *datfcharacter;

//DATAFILE *datafile_monster; // monster pictures (new format)

/*s_datafile_index datidx_monster;
s_datafile_index datidx_texture;
s_datafile_index datidx_editoricon;
//s_datafile_index datidx_sky;
s_datafile_index datidx_image;
//s_datafile_index datidx_portrait;
s_datafile_index datidx_font;
//s_datafile_index datidx_character;
s_datafile_index datidx_musicmidi;
s_datafile_index datidx_object;
s_datafile_index datidx_unidentified;
s_datafile_index datidx_sound;
//s_datafile_index datidx_storyimg;
//DatafileReference<BITMAP*> datref_floorceiling;*/

//BITMAP *datref2_monster [ 256 ];



/*s_assets_adventure_subdatafile advdatafilelist [ System_NB_ADVDATAFILE ] =
{
    { NULL, NULL, "IMG_STORY_DAT", false }, // 0 : ADATF_STORYIMG
    { NULL, NULL, "", false }, // 1 :
    { NULL, NULL, "", false }, // 2 :
    { NULL, NULL, "", false }, // 3 :
    { NULL, NULL, "", false }, // 4 :
    { NULL, NULL, "", false }, // 5 :
    { NULL, NULL, "", false }, // 6 :
    { NULL, NULL, "", false }, // 7 :
    { NULL, NULL, "", false }, // 8 :
    { NULL, NULL, "", false }, // 9 :
    { NULL, NULL, "", false }, // 10 :
    { NULL, NULL, "", false }, // 11 :
    { NULL, NULL, "", false }, // 12 :
    { NULL, NULL, "", false }, // 13 :
    { NULL, NULL, "", false }, // 14 :
    { NULL, NULL, "", false }, // 15 :
};
*/
/*-------------------------------------------------------------------------*/
/*-                           Fonts                                       -*/
/*-------------------------------------------------------------------------*/

/*
s_font_list FONT_LIST [ System_NB_FONT ]=
{
    { "Ambrosia 24       ", ASSETS_FONT_AMBROSIA24 },
    { "Black Chancery 16 ", ASSETS_FONT_BLACKCHANCERY16 },
    { "Black Chancery 24 ", ASSETS_FONT_BLACKCHANCERY24 },
    { "Elgar 24          ", ASSETS_FONT_ELGAR24 },
    { "Elgar 32          ", ASSETS_FONT_ELGAR32 },
    { "Freehand 16       ", ASSETS_FONT_FREEHAND16 },
    { "Freehand 24       ", ASSETS_FONT_FREEHAND24 },
    { "Helena 24         ", ASSETS_FONT_HELENA24 },
    { "Helvetica 22      ", ASSETS_FONT_HELVETICA22 },
    { "Kaufman 16        ", ASSETS_FONT_KAUFMAN16 },
    { "Kaufman 24        ", ASSETS_FONT_KAUFMAN24 },
    { "Print                        "},
    { "Script                       "},
    { "Small                        "},
    { "Book Antiqua Bold 16         "},
    { "Book Antiqua Bold 24         "},
    { "Bookman Old Style 16         "},
    { "Bookman Old Style 24         "},
    { "Deutschegothic Bold 32       "},
    { "Deutschegothic Bold 48       "},
    { "Elementary Gothic Bookhand 24"},
    { "Elementary Gothic Bookhand 32"},
    { "Haettenscheweiler 16         "},
    { "Haettenscheweiler 24         "},
    { "Helena 24                    "},
    { "Helena 32                    "},
    { "Monotype Corsiva 16          "},
    { "Monotype Corsiva 24          "},
    { "Vecker Bold 16               "},
    { "Vecker Bold 24               "},
    { "Verdana Bold 16              "},
    { "Verdana Bold 24              "},
    { "Water Gothic 32              "},
    { "Water Gothic 48              "}
};

*/

/*-------------------------------------------------------------------------*/
/*-                          Music                                        -*/
/*-------------------------------------------------------------------------*/
/*
s_music_track MUSIC_LIST [ System_NB_MUSIC ] =
{
    { " 0-[W1-camp]     Music                        " },//w1camp.mid
    { " 1-[W1-city]     Music                        " },//w1city.mid
    { " 2-[W1-fight]    Engaging the Ennemy          " },//w1combat.mid
    { " 3-[W1-credit]   Celebrating the Victory      " },//w1credit.mid
    { " 4-[W1-dungeon]  Mystery in the Darkness      " },//w1dungon.mid
    { " 5-[W1-edgetown] Music                        " },//w1edge.mid
    { " 6-[W1-ending]   The Ascension Ceremony       " },//w1ending.mid
    { " 7-[W1-inn]      Stories around the fireplace " },//w1inn.mid
    { " 8-[W1-intro]    Music                        " },//w1intro.mid
    { " 9-[W1-lvl10]    Nightmare of the 10th Level  " },//w1lvl10.mid
    { "10-[W1-shop]     Music                        " },//w1shop.mid
    { "11-[W1-tavern]   Music                        " },//w1tavern.mid
    { "12-[W1-temple]   Hoping for the Best          " },//w1temple.mid
    { "13-[W1-victory]  Victory is ours !            " },//w1victor.mid
    { "14-[W2-camp]     Music                        " },//w2camp.mid
    { "15-[W2-city]     Music                        " },//w2city.mid
    { "16-[W2-death]    Helas! He has passed away    " },//w2death.mid
    { "17-[W2-dungeon]  Puzzling Exploration         " },//w2dungon.mid
    { "18-[W2-edgetown] Music                        " },//w2edge.mid
    { "19-[W2-ending]   Music                        " },//w2ending.mid
    { "20-[W2-fight]    Dreadfull Fight              " },//w2fight.mid
    { "21-[W2-inn]      Warm Sweet Home              " },//w2inn.mid
    { "22-[W2-intro]    Tales and Legends            " },//w2intro.mid
    { "23-[W2-shop]     It's now on sale !           " },//w2shop.mid
    { "24-[W2-tavern]   Music                        " },//w2tavern.mid
    { "25-[W2-temple]   Music                        " },//w2temple.mid
    { "26-[W3-boss]     L'Kbreth                     " },//w3boss.mid
    { "27-[W3-camp]     Adventurer's in the Maze     " },//w3camp.mid    //camp fields
    { "28-[W3-city]     Legacy of Llylgamyn          " },//w3city.mid    // Legacy of Llylgamyn
    { "29-[W3-death]    The Fallen Heroes            " },//w3death.mid   // all is dead
    { "30-[W3-dungeon]  Dungeon Theme                " },//w3dungon.mid  // DUngeon Theme
    { "31-[W3-edgetown] Edge of Town                 " },//w3edge.mid    // Edge of Town
    { "32-[W3-ending]   Ending Theme                 " },//w3ending.mid  // ending theme
    { "33-[W3-fight]    Unexpected Encounter         " },//w3fight.mid   //Fighting Sights
    { "34-[W3-inn]      Adventurer's Gathering       "},//w3inn.mid     // Rest of Adventurer's inn
    { "35-[W3-intro]    The Legacy Continues         "},//w3intro.mid   // Openning Theme
    { "36-[W3-shop]     Boltac Trading Post corner   "},//w3shop.mid    // Boltac Trading post corner
    { "37-[W3-tavern]   Gilgamesh Tavern             "},//w3tavern.mid  // Gilgamesh Tavern 2ns scene
    { "38-[W3-temple]   Temple of Cant               "},//w3temple.mid  // temple of cant second scene
};
*/
/*-------------------------------------------------------------------*/
/*-                          Texture                                -*/
/*-------------------------------------------------------------------*/

/*
const char STR_TEXTURE_SET [ System_NB_TEXTURE_SET ][ 21 ] =
{
    {"Adventure"},
    {"Hexen/Heretic Floor"},
    {"Hexen/Heretic Wall"},
    {"Basic"},
    {"Dave GH"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},
    {"Undefined"},

};*/
