/**
   Glymmer

   @author: Eric Pietrocupo
   @date: January 16th, 2022
   @license: Apache 2.0 License

   Demonstration program of the library to test it's features.
*/



#include <stdio.h>
#include <stdbool.h>
#include <allegro.h>
#include <glymmer.h>
#include <gm_input.h> //TODO: Not sure if want to force include internal header
#include <debug_test.h>



FONT* _fnt;
BITMAP* _bg;
BITMAP* _icon;

gms_color_scheme _color_s = {
   .text= {.r=255, .g=255, .b=255},
   .border={.r=0, .g=100, .b=0},
   .underline={.r=150, .g=150, .b=250},
   .cursor={.r=50, .g=250, .b=50},
   .disabled={.r=150, .g=150, .b=150},
   .message={.r=100, .g=255, .b=0},
   .sidetext={.r=210, .g=210, .b=210}
};


void init_demo ( void )
{
   assert_or_exit(allegro_init() == 0, 1, "Could not initialise Allegro GL");
   /*if ( allegro_init() != 0 )
   {  printf("ERROR: initialising allegro");
      exit(1);
   }*/

   assert_or_exit( install_keyboard() == 0, 1, "Could not install keyboard");
   /*if ( install_keyboard() != 0 )
   {
      printf("ERROR: installing keyboard");
      exit(1);
   }*/
   set_color_depth ( 16 );
   //text_mode_old ( -1 ); //requires textout wrapper (maybe not necessary was for backward compat)
   //if ( set_gfx_mode ( GFX_AUTODETECT, 640, 480, 0, 0 ) != 0)
   assert_or_exit(set_gfx_mode ( GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0 ) == 0, 1, "Could not set up graphic mode");
   /*if ( set_gfx_mode ( GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0 ) != 0)
   {  printf("ERROR: Could not set up graphic mode");
      exit(1);
   }*/

   //TODO: tests
   set_projection_viewport ( 0 , 0 , 640 , 480 ); //not sure if necessary
   set_trans_blender( 0, 0, 0, 255 ); //not sure if necessary

   //load a font into memory
   _fnt = load_font( "wizardry10x15.bmp", NULL, NULL);
   //make sur the loading worked
   assert_or_exit( _fnt != NULL, 1, "Could not load font");

   //load a glyph set into memory
   _bg = load_bitmap("GreenGold.bmp", NULL);
   assert_or_exit( _bg != NULL, 2, "Could not load glyph set");
   //load a icon glyph set into memory
   _icon = load_bitmap("GameIcon.bmp", NULL);
   assert_or_exit( _bg != NULL, 2, "Could not load icon glyph set");
   /*if (!_fnt)
   { printf("ERROR: Couldn't load font!");
     exit(1);
   }*/



   //TODO: Add database engine initialisation. Not sure if use an SQL wrapper, else would require
   // an independent library wrapper if other modules than glymmer needs acces to the database (yes indeed)

}

void exit_demo ( void )
{
   set_gfx_mode ( GFX_TEXT, 80, 25, 0, 0 );
   allegro_exit();
}


/*!
   Main method.

*/



int main ( /*int argc, char *args[]*/ void)
{  init_demo();

   gms_glymmer gm;

   gm_initialise( &gm, _fnt, _bg, _icon, GM_INPUT_KEYBOARD, &_color_s, SCREEN_W, SCREEN_H );

   //debug_test_input( &gm);
   //debug_test_empty_dialogs( &gm);
   debug_test_list_dialogs(&gm);
   //debug_test_dynamic_dialogs(&gm);
   gm_dispose( &gm);
   exit_demo();
   return 0;
}
END_OF_MAIN() //specific to Allegro GL, helps convert main() to winmain()
