/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: August 14th, 2022
   @license: Apache 2.0 License

   Contains definitions common to all examples to avoid redundancy of code. It should
   also unclutter the examples with library specific code.

   There is no header file to simplify compilation.

*/

#include <stdbool.h>
#include <allegro.h>
#include <gm_common.h>

FONT* _fnt;
BITMAP *_bg;
BITMAP *_glyph;

/*! @brief Setup function to initialise the necessary GL stuff
   assert_or_exit() is an home made function that verifies a condition or kills the program.
   The objective to to initialise the game library features used by the examples. If a feature
   cannot be initialised, will stop the program.
*/

void tool_initialise_library ( int color_depth )
{  //TODO: Not sure if requires allegro init since only a command line tool

   //Initialise allegro (do not need the graphic accelerated version)
   assert_or_exit(install_allegro(SYSTEM_NONE, &errno, atexit) == 0, 1, "Could not initialise Allegro GL");
   //initialise the keyboard routines
   // assert_or_exit( install_keyboard() == 0, 1, "Could not install keyboard");
   //set the color depth to 16 bits
   set_color_depth ( color_depth );
   //set the graphic mode in 640x480
   //assert_or_exit(set_gfx_mode ( GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0 ) == 0, 1, "Could not set up graphic mode");
   //not sure if essential, set the view port the same size as the screen.
   //set_projection_viewport ( 0 , 0 , 640 , 480 );
   //not sure if essential, so translucency to black with alpha opaque
   //set_trans_blender( 0, 0, 0, 255 );

   //load a fon into memory
   //_fnt = load_font( "WizardrySmall.bmp", NULL, NULL);
   //TODO GLYPH to load in option
   //make sur the loading worked
   //assert_or_exit( _fnt != NULL, 1, "Could not load font");

   //create a bitmap that will be used as background behind the dialogs
   _bg = NULL; //
//   _bg = create_bitmap( SCREEN_W, SCREEN_H );
   //paint the packground with a green color
   //rectfill ( _bg, 0, 0, SCREEN_W, SCREEN_H, makecol(0, 240, 0 ) );
}

/*! @brief Dispose the game library
   Called before exiting the prograpm to dispose all library content and set back the video
   mode into text mode.
*/

void tool_exit_library ( void )
{  //deallocate the bitmap
   //destroy_bitmap( _bg);
   //deallocate the font
   //destroy_font( _fnt);
   //set the graphic mode back into text
   set_gfx_mode ( GFX_TEXT, 80, 25, 0, 0 );
   //dispose the game library
   allegro_exit();
}
