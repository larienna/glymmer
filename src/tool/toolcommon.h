/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: August 14th, 2022
   @license: Apache 2.0 License

   Contains definitions common to all examples to avoid redundancy of code. It should
   also unclutter the examples with library specific code.

*/

#ifndef EXCOMMON_H_INCLUDED
#define EXCOMMON_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

//TODO Depending on the tools, might not need everything like the examples.
extern FONT* _fnt;
extern BITMAP *_bg;
extern BITMAP *_glyph;
extern gms_color_scheme _color_s;

void tool_initialise_library ( void );
void tool_exit_library ( void );

#ifdef __cplusplus
}
#endif

#endif // EXCOMMON_H_INCLUDED
