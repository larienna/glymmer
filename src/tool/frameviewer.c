/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: August 14th, 2022
   @license: Apache 2.0 License

   This is a very simple example that display multiple dialogs on the screen containing
   only text with a notification message at the top.

*/

#include <stdio.h>
#include <stdbool.h>
#include <allegro.h>
#include <glymmer.h>

#include <toolcommon.h>

gms_color_scheme _color_s = {
   .text= {.r=225, .g=225, .b=225},
   .border={.r=0, .g=100, .b=0},
   .underline={.r=250, .g=250, .b=200},
   .cursor={.r=200, .g=200, .b=255},
   .disabled={.r=125, .g=125, .b=125},
   .message={.r=100, .g=255, .b=0}
};


void text_dialogs_example ( gms_glymmer *gm )
{
   //creating various dialogs that will be stacked
   gm_dialog_open_background(gm, "Example 01 Text dialogs");
   gm_dialog_open_empty( gm, "Empty box", 5, 5, 20, 5);
   gm_dialog_open_text ( gm, NULL, -25, -10, 25, 10, "Dwarf\n\vDWizard\n\vELevel 12\n\vFHP 105/108\n\vGMP  50/ 75\n\vHPoisoned", false );
   gm_dialog_open_message ( gm, "This is an important message", "This is a simple notification messagenothing to worry about. \nPush select key to close" );

   //Display dialogs and wait for user input
   gm_show_dialogs( _bg, gm);

   //Close the last 3 dialogs, all except the background
   gm_dialog_close( gm, 3 );

   //Display a paged text lorem ipsum
   gm_dialog_open_pagedtext(gm, "Lorem Ipsum", 1, 2, 27, 10,
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor orci eu posuere posuere. Morbi at ultricies metus. Duis in dui condimentum, volutpat purus non, consectetur lectus. Fusce ultricies ac sapien non posuere. Aliquam erat volutpat. Fusce id placerat tortor. Morbi ullamcorper vehicula metus at porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id eros lacus. In pulvinar diam sed ipsum maximus, id commodo mauris consectetur.\n\
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer placerat, erat at iaculis hendrerit, velit sem commodo odio, eget semper turpis augue et ante. Ut varius rutrum ipsum sed volutpat. Donec at augue eu tellus tincidunt ultricies. Duis hendrerit justo eget libero molestie, vitae fringilla mi iaculis. Phasellus nisi velit, varius quis malesuada luctus, suscipit et nunc. Morbi luctus vel mi quis rutrum. Praesent vel tempus enim, ut dignissim mauris. Duis eu lorem non quam euismod hendrerit eget quis dolor. Suspendisse porttitor condimentum erat at dapibus. Cras vel pellentesque metus, at volutpat est. In hac habitasse platea dictumst. Quisque vestibulum tortor quis ligula maximus efficitur.\n\
Donec pellentesque risus urna, sed lobortis ante dignissim sed. Integer mollis egestas nunc vitae volutpat. Donec finibus ante id auctor egestas. Sed ac ornare est, non facilisis tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam et lacus nec diam viverra vehicula et ut dolor. Suspendisse molestie non neque hendrerit euismod. ");

   gm_show_dialogs( _bg, gm);

   //Close the last dialog
   gm_dialog_close( gm, 1 );

   //create a confirmation
   gm_dialog_open_confirm(gm, "I need a confirmation!", "Do you really want to do that?\nI really need to know!\nIt would help me a lot");

   int answer;
   //Print dialogs, read input and save the answer
   answer = gm_show_dialogs( _bg, gm);

   printf ("The answer selected was %d\n", answer );

}

int main ( int argc, char *args[])
{  //Group all game library initialisation here to unclutter the code
   tool_initialise_library();

   //create the glymmer data structure which hold all the engine information
   gms_glymmer gm;

   /*Initialise the data structure with speficied value.
      gm: the structure to initialise
      _fnt: a global variable font loaded during initialise_library();
      16: The designed initial size of the dialog stack. The stack is auto extended if exceeding.
      GM_INPUT_KEYBOARD: Input system desired to be used.
   */
   //TODO: Add glyph bitmap
   gm_initialise( &gm, _fnt, NULL, NULL, GM_INPUT_KEYBOARD, &_color_s, SCREEN_W, SCREEN_H );

   text_dialogs_example ( &gm );

   //Closing the Glymemr engine to dispose resources
   gm_dispose( &gm);

   //Closing the game library to dispose resources and return to text mode
   tool_exit_library();
   return 0;
}
END_OF_MAIN() //specific to Allegro GL, helps convert main() to winmain()

