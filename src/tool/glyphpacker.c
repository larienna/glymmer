/*!
   Glyph Packer

   @author: Eric Pietrocupo
   @date: October 28th, 2022
   @license: Apache 2.0 License

   Simple tool that takes up to 256 separate glyph bitmaps and fusion them into a
   single bitmap atlas of 16 by 16 glyphs.

*/

#include <stdio.h>
#include <stdbool.h>
#include <allegro.h>
#include <glymmer.h>
#include <getopt.h>
#include <toolcommon.h>


//Error exit code
#define GMGP_ERROR_INITGL        1
#define GMGP_ERROR_MISSING_ARG   2
#define GMGP_ERROR_UNKNOWN_OPT   3
#define GMGP_ERROR_PARSE_OPT     4
#define GMGP_ERROR_GLYPH_SIZE    5
#define GMGP_ERROR_FILENAME      6
#define GMGP_ERROR_NB_GLYPH_LINES   7

#define NB_GLYPH_PER_LINE           16
#define MAX_NB_GLYPH_LINES          16

const char help_message[] = "Glymmer: Glyph Packer tool\n\
Takes multiple glyph bitmap and fusion them into a single bitmap atlas. \n\n\
Usage: gpack [-c 00] -h 00 -w 00 -l 00 output_file [input_directory] \n\
   -c : Color depth of output (palette not supported, default: 24 bits) \n\
   -h : Glyph height \n\
   -w : Glyph width \n\
   -l : Number of lines, each line contains 16 glyphs (max 16 lines) \n\
   output_file : name of the output bitmap \n\
   input_directory : path to the file list (default: ./) \n\
";
//test mode is deactivated for now. Bellow is the message string
/*
-t : Test mode. Compare the result with the output file, does not create any file.\n\
*/
/*! @brief Structure that holds command line parameters
*/

typedef struct s_arguments
{  int color_depth; //!< Color depth of the output bitmap
   int glyph_width;  //!<Width of each glyph,
   int glyph_height; //!<Height of each glyph
   int nb_line; //!<lumber of lines of 16 glyphs
   bool test; //!<In test mode, no file is create, it return 0 if the output file match the packing.
   char *input_directory; //!< Name of the directory to read the files from
   char *output_filename; //!< Filename to save the atlas into, extension should autoselect output format.
}s_arguments;


/*! @brief: Special assertion method that display usage message
*/

void assert_parse_error ( bool condition, int exitcode, char *message )
{  if ( condition == false)
   {  printf ("PARSE ERROR: %s\n", message );
      puts(help_message);
      exit (exitcode);
   }
}

/*! @brief Read the command line arguments and set the structure information.
*/

void parse_arguments ( int argc, char *args [], s_arguments *arguments )
{
   int option;

   //printf ("DEBUG: argc=%d\n", argc);

   assert_parse_error( argc >= 6, GMGP_ERROR_MISSING_ARG, "Not enough arguments");

   //default options
   arguments->color_depth = 24; //will not support paletted mode so far
   arguments->glyph_width = -1; //break if missing
   arguments->glyph_height = -1; //break if missing
   arguments->nb_line = 0; //break if not set to a value
   arguments->test = false; //no testing by default
   arguments->output_filename = NULL; //must be specified
   arguments->input_directory = "./"; //open current directory by default

   while ( (option = getopt ( argc, args, "+c:w:h:l:t")) != -1 )
   {  switch ( option )
      {  case 'c':
            arguments->color_depth = atoi(optarg);
         break;
         case 'w':
            arguments->glyph_width = atoi (optarg);
         break;
         case 'h':
            arguments->glyph_height = atoi (optarg);
         break;
         case 'l':
            arguments->nb_line = atoi (optarg);
         break;
         /*case 't':
            arguments->test = true;
         break;*/
         case '?':
            assert_parse_error( false, GMGP_ERROR_UNKNOWN_OPT, "unknown option");
         break;
         default:
            assert_parse_error( false, GMGP_ERROR_PARSE_OPT, "parsing options");
         break;
      }
   }


   //printf("DEBUG optind=%d\n", optind);
   arguments->output_filename = args[optind];
   if ( argc > optind+1) arguments->input_directory = args[optind+1];

   assert_parse_error( arguments->glyph_width != -1 && arguments->glyph_height != -1,
      GMGP_ERROR_GLYPH_SIZE, "Glyph size missing");
   assert_parse_error( arguments->output_filename != NULL, GMGP_ERROR_FILENAME, "Missing filename" );

   assert_parse_error( arguments->nb_line > 0 && arguments->nb_line <= MAX_NB_GLYPH_LINES,
      GMGP_ERROR_NB_GLYPH_LINES, "Number of glyphs lines must be between 1 and 16" );
   printf ("Glyph width = %d\n", arguments->glyph_width);
   printf ("Glyph height = %d\n", arguments->glyph_height);
   printf ("Color depth = %d\n", arguments->color_depth);
   printf ("Nb of glyphs to read = 16 x %d = %d\n", arguments->nb_line,
      arguments->nb_line * NB_GLYPH_PER_LINE);
   //printf ("Test mode %s\n", arguments->test ? "activated" : "deactivated" );
   printf ("Output file = %s\n", arguments->output_filename);
   printf ("Input directory = %s\n", arguments->input_directory);

}


/*void text_dialogs_example ( gms_glymmer *gm )
{
   //creating various dialogs that will be stacked
   gm_dialog_open_background(gm, "Example 01 Text dialogs");
   gm_dialog_open_empty( gm, "Empty box", 5, 5, 20, 5);
   gm_dialog_open_text ( gm, NULL, -25, -10, 25, 10, "Dwarf\n\vDWizard\n\vELevel 12\n\vFHP 105/108\n\vGMP  50/ 75\n\vHPoisoned", false );
   gm_dialog_open_message ( gm, "This is an important message", "This is a simple notification messagenothing to worry about. \nPush select key to close" );

   //Display dialogs and wait for user input
   gm_show_dialogs( _bg, gm);

   //Close the last 3 dialogs, all except the background
   gm_dialog_close( gm, 3 );

   //Display a paged text lorem ipsum
   gm_dialog_open_pagedtext(gm, "Lorem Ipsum", 1, 2, 27, 10,
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor orci eu posuere posuere. Morbi at ultricies metus. Duis in dui condimentum, volutpat purus non, consectetur lectus. Fusce ultricies ac sapien non posuere. Aliquam erat volutpat. Fusce id placerat tortor. Morbi ullamcorper vehicula metus at porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id eros lacus. In pulvinar diam sed ipsum maximus, id commodo mauris consectetur.\n\
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer placerat, erat at iaculis hendrerit, velit sem commodo odio, eget semper turpis augue et ante. Ut varius rutrum ipsum sed volutpat. Donec at augue eu tellus tincidunt ultricies. Duis hendrerit justo eget libero molestie, vitae fringilla mi iaculis. Phasellus nisi velit, varius quis malesuada luctus, suscipit et nunc. Morbi luctus vel mi quis rutrum. Praesent vel tempus enim, ut dignissim mauris. Duis eu lorem non quam euismod hendrerit eget quis dolor. Suspendisse porttitor condimentum erat at dapibus. Cras vel pellentesque metus, at volutpat est. In hac habitasse platea dictumst. Quisque vestibulum tortor quis ligula maximus efficitur.\n\
Donec pellentesque risus urna, sed lobortis ante dignissim sed. Integer mollis egestas nunc vitae volutpat. Donec finibus ante id auctor egestas. Sed ac ornare est, non facilisis tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam et lacus nec diam viverra vehicula et ut dolor. Suspendisse molestie non neque hendrerit euismod. ");

   gm_show_dialogs( _bg, gm);

   //Close the last dialog
   gm_dialog_close( gm, 1 );

   //create a confirmation
   gm_dialog_open_confirm(gm, "I need a confirmation!", "Do you really want to do that?\nI really need to know!\nIt would help me a lot");

   int answer;
   //Print dialogs, read input and save the answer
   answer = gm_show_dialogs( _bg, gm);

   printf ("The answer selected was %d\n", answer );

}
*/

/*! @brief Concatenate 2 string into a new dynamically allocated string.
   Internally, strncat and strncpy are used. Resulting pointer must be deallocated. String will be
   null terminated
*/

char *new_strcat ( const char *s1, const char *s2)
{
   int length_s1 = strlen ( s1 );
   int length_s2 = strlen ( s2 );
   char *newstr = (char*)malloc (  sizeof(char) * (length_s1 + length_s2 + 1));

   strncpy ( newstr, s1, length_s1 +1 );
   strncat ( newstr, s2, length_s2 +1 );
   //printf ("DEBUG new_strcat Length=%d, str='%s'\n", length_s1 + length_s2, newstr );

   return newstr;
}

/*! @brief Converts interger to hexadecimal
   helper function to conver an integer to a single digit hex, could not find a libc equivalent

   @param value integer value to convert
   @param high pointer on the character for the high value
   @param low pointer on the character of the low value
*/

void itohex ( unsigned char value, char *high, char *low )
{  char hex[16] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

   int inthigh = value / 16;
   int intlow = value % 16;

   *high = hex[inthigh];
   *low = hex[intlow];

}

/*! @brief Compares if 2 bitmaps are the same pixel per pixel
*/

void compare_bitmaps ( BITMAP* bmp_a, BITMAP* bmp_b)
{  unsigned char *ptr_a = (unsigned char*)bmp_a->dat; // convert as a byte pointer for byte per byte comparison
   unsigned char *ptr_b = (unsigned char*)bmp_b->dat;

   ptr_a +=0; //NOTE: removes warning
   ptr_b +=0; //NOTE: removes warning
   //NOTE: POSTPONE because complicated to know the size of the buffer to compare. depends
   // on the color depth and the number of pixels. Very GL specific.
}

/*! @brief take multiple glyph and copy them into a single file
*/

void fusion_bitmaps ( s_arguments *arguments)
{
   //struct al_ffblk fileinfo;
   int out_width = arguments->glyph_width * NB_GLYPH_PER_LINE;
   int out_height = arguments->glyph_height * arguments->nb_line;
   char *tmpath;
   int charoffset = strlen ( arguments->input_directory );

   //create output bitmap
   BITMAP *out = create_bitmap( out_width, out_height);
   rectfill ( out, 0, 0, out_width, out_height, makecol(255, 0, 255 ) );

   //Makes sur the last char in path is a / to have valid search path
   /*printf("DEBUG -1=[%c], -2=[%c], -3=[%c]\n", arguments->input_directory[charoffset-1],
      arguments->input_directory[charoffset-2],
      arguments->input_directory[charoffset-3] );*/


   //Makes sure there are bitmaps to load (code seems buggy, detect manually instead)
   /*printf ("Searching for files in: \n%s\n", tmpath );
   assert_or_exit( al_findfirst(tmpath, &fileinfo, FA_ALL) != 0,
                   GMGP_ERROR_NOBMP_FOUND, "No bitmap files found in directory");
   free(tmpath);*/

   //adds trailing slash to path if missing. Kind of failsafe
   if ( arguments->input_directory[charoffset-1] == '/' ) tmpath = new_strcat( arguments->input_directory, "00.bmp");
   else tmpath = new_strcat( arguments->input_directory, "/00.bmp");

   int yoffset = 0;
   int xoffset = 0;
//   char charnum[2];
//   charnum[1] = '\0'; //terminate the 1 char string.

   //DEBUG test fileopen
   /*BITMAP *testbmp = load_bitmap( "./10.bmp", NULL );
   if ( testbmp == NULL ) printf ("TEST BITMAP FAILED TO OPEN");*/


   int count_open = 0;
   for ( int i = 0 ; i < arguments->nb_line * NB_GLYPH_PER_LINE; i++ )
   {  //Modified the filename in the path to avoid allocating a new string for each file
      itohex ( i, &tmpath[charoffset], &tmpath[charoffset+1]);
      yoffset = i / 16;
      xoffset = i % 16;
      //printf ("DEBUG %d: %s: offset (%d, %d)", i, tmpath, yoffset, xoffset );

      BITMAP *glyph = load_bitmap( tmpath, NULL );
      if ( glyph != NULL )
      {  printf ("Reading: %s\n", tmpath);
         count_open++;
         blit ( glyph, out, 0, 0, xoffset * arguments->glyph_width, yoffset * arguments->glyph_height,
            arguments->glyph_width, arguments->glyph_height );
         destroy_bitmap( glyph);
      }

      //if ( glyph == NULL ) printf ("DEBUG Error\n"); else printf ("DEBUG OK\n");

   }
   //
   /*{   charnum[0] = fileinfo.name[0]; // get the first char of the file name
       yoffset = strtol ( charnum, NULL, 16); //convert it in base 16
       charnum[0] = fileinfo.name[1]; // get 2nd char of the file name to convert it.
       xoffset = strtol ( charnum, NULL, 16 );
       printf ("Found %s: offset (%d, %d)\n", fileinfo.name, yoffset, xoffset );



       if ( glyph == NULL )
       { printf ("ERROR: File %s could not be opened\n", fileinfo.name );
         exit (6);
       }

       blit ( glyph, out, 0, 0, xoffset * arguments->glyph_width, yoffset * arguments->glyph_height,
            arguments->glyph_width, arguments->glyph_height );


      destroy_bitmap( glyph);
   }
   */
   //while (al_findnext( &fileinfo) == 0);

   printf ("%d glyph files read\n", count_open );
   save_bmp( arguments->output_filename, out, NULL);

   free (tmpath );
   destroy_bitmap( out );
}




int main ( int argc, char *args[])
{

   printf ("Glymmer: Glyph Packing Tool\n");

   s_arguments arguments;
   parse_arguments( argc, args, &arguments );

   assert_or_exit(install_allegro(SYSTEM_NONE, &errno, atexit) == 0, GMGP_ERROR_INITGL, "Could not initialise Allegro GL");
   set_color_depth ( arguments.color_depth ); //deos not seem to work, file size if the same.

   set_color_conversion(COLORCONV_TOTAL); //Tried to convert all loaded files to current color depth

   fusion_bitmaps( &arguments );

   allegro_exit();

   return 0;
}
END_OF_MAIN() //specific to Allegro GL, helps convert main() to winmain()

