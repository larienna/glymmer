/**
   Glymmer
   Unit test registry

   @author: Eric Pietrocupo
   @date: August 16th, 2022
   @license: Apache 2.0 License

   This is a series of test suite registerng routines that will register each test individually.
   scattered among the test suite.

   Also conatains common test routines to compare bitmaps and text buffer used by tests.

*/


#ifndef UNITEST_H_INCLUDED
#define UNITEST_H_INCLUDED

//testing with very small screen
#define GM_TEST_SCREEN_W   64
#define GM_TEST_SCREEN_H   48

extern gms_glymmer *_engine;
extern BITMAP *_test_screen;

bool compare_text_buffer_letter ( const char* str );
bool spotcheck_bgglyph ( int x, int y, unsigned char glyph );
bool spotcheck_textcolor ( int x, int y, unsigned char color );
bool spotcheck_is_underlined ( int x, int y );
bool spotcheck_icon ( int x, int y, unsigned char icon );

void TS00_register_suite ( void );
void TS01_register_suite ( void );
int run_all_test ( void );
void all_test_setup ( void );
void all_test_teardown ( void );


#endif // UNITEST_H_INCLUDED
