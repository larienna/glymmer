/**
   Glymmer
   Unit test suite 01

   @author: Eric Pietrocupo
   @date: August 16th, 2022
   @license: Apache 2.0 License
*/

#include <stdbool.h>
#include <allegro.h>
#include <glymmer.h>
#include <glymmer_private.h>
#include <CUnit/CUnit.h>
#include <component.h>
#include <textprint.h>
#include <unitest.h>

int TS01_setup ( void )
{  return 0;
}

int TS01_teardown ( void )
{  return 0;
}

void TS01_multiple_dialog ( void )
{  gm_dialog_close_all( _engine);
   gm_dialog_open_text(_engine, "Win1", 0, 0, 8, 3, "Msg1", true);
   gm_dialog_open_text(_engine, "Win2", 0, 3, 8, 3, "Msg2",false);
   // need to access directly due to reallocation
   gms_dialog *d1 = &_engine->dstack.dlg[0];
   gms_dialog *d2 = &_engine->dstack.dlg[1];
   assert_or_exit( d1 != NULL, 2,"ERROR: Cannot open dialog" );
   assert_or_exit( d2 != NULL, 2,"ERROR: Cannot open dialog" );
   CU_ASSERT_EQUAL( _engine->dstack.nb_dialog, 2);


   //assert presence of components
   CU_ASSERT_EQUAL ( d1->components, GM_COMPONENT_WINDOW
                    + GM_COMPONENT_NAVIGATION
                    + GM_COMPONENT_TEXT );

   CU_ASSERT_EQUAL ( d2->components, GM_COMPONENT_WINDOW
                    + GM_COMPONENT_NAVIGATION
                    + GM_COMPONENT_TEXT );

   //assert window component
   CU_ASSERT_EQUAL ( d1->win.x, 0 );
   CU_ASSERT_EQUAL ( d1->win.y, 0 );
   CU_ASSERT_EQUAL ( d1->win.w, 8 );
   CU_ASSERT_EQUAL ( d1->win.h, 3 );
   CU_ASSERT_EQUAL ( d1->win.line_cursor, 1);
   CU_ASSERT_EQUAL ( strcmp (d1->win.title, "Win1"), 0);
   CU_ASSERT_EQUAL ( d2->win.x, 0 );
   CU_ASSERT_EQUAL ( d2->win.y, 3 );
   CU_ASSERT_EQUAL ( d2->win.w, 8 );
   CU_ASSERT_EQUAL ( d2->win.h, 3 );
   CU_ASSERT_EQUAL ( d2->win.line_cursor, 1);
   CU_ASSERT_EQUAL ( strcmp (d2->win.title, "Win2"), 0);
   //assert navigation component
   CU_ASSERT_EQUAL ( d2->nav.cursor, 0 );
   CU_ASSERT_EQUAL ( d2->nav.nb_item, 0 );
   CU_ASSERT_EQUAL ( d2->nav.nb_page, 1 );
   CU_ASSERT_EQUAL ( d2->nav.page_size, 0 );
   CU_ASSERT_EQUAL ( d2->nav.page_offset, 0 );
   CU_ASSERT_EQUAL ( d2->nav.instructions, GM_INSTRUCTION_ACCEPT );
    //assert text component
   CU_ASSERT_EQUAL ( d1->txt.dynamic, false );
   CU_ASSERT_EQUAL ( strcmp (d1->txt.message, "Msg1"), 0 );
   CU_ASSERT_EQUAL ( d1->txt.nb_lines, 1 );
   CU_ASSERT_EQUAL ( d1->txt.wrap, true );
   CU_ASSERT_EQUAL ( d2->txt.dynamic, false );
   CU_ASSERT_EQUAL ( strcmp (d2->txt.message, "Msg2"), 0 );
   CU_ASSERT_EQUAL ( d2->txt.nb_lines, 1 );
   CU_ASSERT_EQUAL ( d2->txt.wrap, false );

   gm_clear_text_buffer(&_engine->screen);
   gm_draw_dialog_stack(_test_screen, _engine );
   char screenstr[ GM_TEST_SCREEN_H * GM_TEST_SCREEN_W + 1];
   sprintf( screenstr, "  Win1   Msg1             Win2   Msg2        %c  "
      , GM_SCHAR_ACCEPT );
   CU_ASSERT_TRUE ( compare_text_buffer_letter(screenstr) );

   CU_ASSERT_TRUE( spotcheck_textcolor(1,1,GM_COLOR_SIDETEXT));
   CU_ASSERT_TRUE( spotcheck_textcolor(1,4,GM_COLOR_MESSAGE));


}

void TS01_dummy ( void )
{  CU_ASSERT_TRUE ( true );
}

void TS01_register_suite ( void )
{  CU_pSuite TS01 = CU_add_suite("01 Structures?", TS01_setup, TS01_teardown);

   if (TS01 != NULL)
   {  CU_add_test (TS01, "Multiple dialogs", TS01_multiple_dialog );
      CU_add_test (TS01, "Dummy", TS01_dummy );

   }
}
