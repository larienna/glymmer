/**
   Glymmer
   Unit test registry

   @author: Eric Pietrocupo
   @date: August 16th, 2022
   @license: Apache 2.0 License
*/

#include <stdbool.h>
#include <allegro.h>
#include <glymmer.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <CUnit/CUError.h>
#include <unitest.h>

gms_glymmer *_engine;
BITMAP *_test_screen;
gms_color_scheme _test_color_s = {
   .text= {.r=225, .g=225, .b=225},
   .border={.r=0, .g=100, .b=0},
   .underline={.r=250, .g=250, .b=200},
   .cursor={.r=200, .g=200, .b=255},
   .disabled={.r=125, .g=125, .b=125},
   .message={.r=200, .g=255, .b=200}
};

/*! @brief internal function to help debuggin text buffer output
*/

void print_text_buffer_letter ( void )
{  printf("\nDEBUG: Textbuffer letters: \n|");
   int i = 0;
   for (int y = 0; y < _engine->screen.text_h; y++)
   {  for ( int x = 0 ; x < _engine->screen.text_w; x++)
      {  printf("%c", _engine->screen.text_buffer[i].letter);
         i++;
      }
      printf("|\n|");
   }
}

/*! @brief Compares the letter drawn in the text buffer with the current string.
   Internally, the text buffer is a single dimension array, so the comparison is very straight forward.
   Note: that instructions will be non printable characters, so tests should stop before
   instruction display. Use appropriate instruction testing method.
*/
bool compare_text_buffer_letter ( const char* str )
{  int i = 0;
   //use unsigned char for beyond 128 char support, conflic signature, could create own strlen
   int len = strlen(str);
   for ( int y = 0; y < _engine->screen.text_h; y++)
      for ( int x = 0 ; x < _engine->screen.text_w; x++)
      {  unsigned char tmpc = str[i]; // conversion from signed to unsigned
         if ( _engine->screen.text_buffer[i].letter != tmpc)
         {  print_text_buffer_letter(); //DEBUG
            printf ("ERROR: Char %d(%d,%d): %c(%d) != %c(%d)\n", i, x, y,
               _engine->screen.text_buffer[i].letter,
               _engine->screen.text_buffer[i].letter,
               tmpc, tmpc );
            return false;
         }
         i++;
         if ( i >= len ) return true;
      }
   return true;
}

bool spotcheck_bgglyph ( int x, int y, unsigned char glyph )
{  int index = y * _engine->screen.text_w + x;
   return _engine->screen.text_buffer[index].bg == glyph;
}
bool spotcheck_textcolor ( int x, int y, unsigned char color )
{  int index = y * _engine->screen.text_w + x;
   return _engine->screen.text_buffer[index].color == color;
}
bool spotcheck_is_underlined ( int x, int y )
{  int index = y * _engine->screen.text_w + x;
   return _engine->screen.text_buffer[index].underlined;
}
bool spotcheck_icon ( int x, int y, unsigned char icon )
{  int index = y * _engine->screen.text_w + x;
   return _engine->screen.text_buffer[index].icon == icon;
}

int run_all_test ( void )
{  int nb_test_failed = 0;
   if ( CU_initialize_registry() == CUE_SUCCESS)
   {  TS00_register_suite ();
      //printf("%d test failed\n", CU_get_number_of_tests_failed());
      TS01_register_suite ();
      //printf("%d test failed\n", CU_get_number_of_tests_failed());

      CU_basic_set_mode(CU_BRM_VERBOSE);
      CU_basic_run_tests();
   }
   nb_test_failed = CU_get_number_of_tests_failed();
   /*printf ("number of suites completed %d\n", CU_get_number_of_suites_run () );
 	printf ("number of suites which failed to initialize %d\n", CU_get_number_of_suites_failed () );
 	printf ("number of tests completed %d\n", CU_get_number_of_tests_run ());
 	printf ("number of tests which contained failed assertions %d\n", CU_get_number_of_tests_failed ());
   printf ("number of assertions processed %d\n", CU_get_number_of_asserts ());
 	printf ("number of successful assertions %d\n", CU_get_number_of_successes ());
 	printf ("number of failed assertions %d\n", CU_get_number_of_failures ());
 	printf ("number failure records created %d\n", CU_get_number_of_failure_records ());*/

   CU_cleanup_registry();

   CU_ErrorCode errorcode = CU_get_error();
   if ( errorcode != CUE_SUCCESS)
   {  printf("Cunit error code %d: %s\n", errorcode, CU_get_error_msg());
   }
   return nb_test_failed;
}

void all_test_setup ( void )
{  //printf ("DEBUG: Before launching allegro\n");
   int errno;
   assert_or_exit( install_allegro(SYSTEM_NONE, &errno, atexit) == 0, 1,"ERROR: Cannot initialise allegro" );
   _engine = (gms_glymmer*) malloc ( sizeof(gms_glymmer) );
   //TODO Add glyph bitmapcomputer generated to avoid loading a file
   gm_initialise (_engine, font, NULL, NULL, GM_INPUT_KEYBOARD, &_test_color_s,
                  GM_TEST_SCREEN_W, GM_TEST_SCREEN_H );

   _test_screen = create_bitmap(GM_TEST_SCREEN_W, GM_TEST_SCREEN_H);
}

void all_test_teardown ( void )
{
   //printf ("DEBUG: Before disposing glymmer\n");
   gm_dispose (_engine);
   free (_engine);
   allegro_exit();
}


