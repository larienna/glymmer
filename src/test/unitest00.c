/**
   Glymmer
   Unit test suite 00

   @author: Eric Pietrocupo
   @date: August 16th, 2022
   @license: Apache 2.0 License

   Validate that components and their dialogs are correctly initialised.
*/

#include <stdbool.h>
#include <allegro.h>
#include <glymmer.h>
#include <glymmer_private.h>
#include <CUnit/CUnit.h>
#include <component.h>
#include <textprint.h>
#include <unitest.h>



/*! @brief Initialisation of allegro and glymmer
   The graphic mode is not set and a tiny screen is used using system font to avoid file opening.
   The screen will be 64x48 pixel, with an 8x8 font that creates an 8x6 text screen.
*/

int TS00_setup ( void )
{
   return 0;
}

int TS00_teardown ( void )
{
   return 0;
}

void TS00_empty_dialog ( void )
{  gm_dialog_close_all( _engine);
   gms_dialog *d = gm_dialog_open_empty (_engine, "Hello", 0, -5, 6, 4 );
   assert_or_exit( d != NULL, 2,"ERROR: Cannot open dialog" );

   //assert presence of components
   CU_ASSERT_EQUAL ( d->components, GM_COMPONENT_WINDOW + GM_COMPONENT_NAVIGATION );
   //assert window component
   CU_ASSERT_EQUAL ( d->win.x, 0 );
   CU_ASSERT_EQUAL ( d->win.y, 1 );
   CU_ASSERT_EQUAL ( d->win.w, 6 );
   CU_ASSERT_EQUAL ( d->win.h, 4 );
   CU_ASSERT_EQUAL ( d->win.line_cursor, 1);
   CU_ASSERT_EQUAL ( strcmp (d->win.title, "Hello"), 0 );
   //assert navigation component
   CU_ASSERT_EQUAL ( d->nav.cursor, 0 );
   CU_ASSERT_EQUAL ( d->nav.nb_item, 0 );
   CU_ASSERT_EQUAL ( d->nav.nb_page, 1 );
   CU_ASSERT_EQUAL ( d->nav.page_size, 0 );
   CU_ASSERT_EQUAL ( d->nav.page_offset, 0 );
   CU_ASSERT_EQUAL ( d->nav.instructions, GM_INSTRUCTION_ACCEPT );

   gm_clear_text_buffer(&_engine->screen);
   gm_draw_dialog_stack(_test_screen, _engine );

   char screenstr[ GM_TEST_SCREEN_H * GM_TEST_SCREEN_W + 1];
   sprintf( screenstr, "          He                       %c   ", GM_SCHAR_ACCEPT );
   CU_ASSERT_TRUE ( compare_text_buffer_letter(screenstr) );
}

void TS00_background_dialog ( void )
{  gm_dialog_close_all( _engine);
   gms_dialog *d = gm_dialog_open_background(_engine, NULL);
   assert_or_exit( d != NULL, 2,"ERROR: Cannot open dialog" );

   //assert presence of components
   CU_ASSERT_EQUAL ( d->components, GM_COMPONENT_WINDOW + GM_COMPONENT_NAVIGATION );
   //assert window component
   CU_ASSERT_EQUAL ( d->win.x, 0 );
   CU_ASSERT_EQUAL ( d->win.y, 0 );
   CU_ASSERT_EQUAL ( d->win.w, 8 );
   CU_ASSERT_EQUAL ( d->win.h, 6 );
   CU_ASSERT_EQUAL ( d->win.line_cursor, 1);
   CU_ASSERT_EQUAL ( d->win.title, NULL );
   //assert navigation component
   CU_ASSERT_EQUAL ( d->nav.cursor, 0 );
   CU_ASSERT_EQUAL ( d->nav.nb_item, 0 );
   CU_ASSERT_EQUAL ( d->nav.nb_page, 1 );
   CU_ASSERT_EQUAL ( d->nav.page_size, 0 );
   CU_ASSERT_EQUAL ( d->nav.page_offset, 0 );
   CU_ASSERT_EQUAL ( d->nav.instructions, GM_INSTRUCTION_ACCEPT );

   gm_clear_text_buffer(&_engine->screen);
   gm_draw_dialog_stack(_test_screen, _engine );

   char screenstr[ GM_TEST_SCREEN_H * GM_TEST_SCREEN_W + 1];
   sprintf( screenstr, "                                             %c  ", GM_SCHAR_ACCEPT );
   CU_ASSERT_TRUE ( compare_text_buffer_letter(screenstr) );


}

void TS00_text_dialog ( void )
{  gm_dialog_close_all( _engine);
   gms_dialog *d = gm_dialog_open_text(_engine, "Msg", -7, 0, 7, 5, "Cost is a 5\a018\a02", true);
   assert_or_exit( d != NULL, 2,"ERROR: Cannot open dialog" );
   //assert presence of components
   CU_ASSERT_EQUAL ( d->components, GM_COMPONENT_WINDOW
                    + GM_COMPONENT_NAVIGATION
                    + GM_COMPONENT_TEXT );
   //assert window component
   CU_ASSERT_EQUAL ( d->win.x, 1 );
   CU_ASSERT_EQUAL ( d->win.y, 0 );
   CU_ASSERT_EQUAL ( d->win.w, 7 );
   CU_ASSERT_EQUAL ( d->win.h, 5 );
   CU_ASSERT_EQUAL ( d->win.line_cursor, 1);
   CU_ASSERT_EQUAL ( strcmp (d->win.title, "Msg"), 0);
   //assert navigation component
   CU_ASSERT_EQUAL ( d->nav.cursor, 0 );
   CU_ASSERT_EQUAL ( d->nav.nb_item, 0 );
   CU_ASSERT_EQUAL ( d->nav.nb_page, 1 );
   CU_ASSERT_EQUAL ( d->nav.page_size, 0 );
   CU_ASSERT_EQUAL ( d->nav.page_offset, 0 );
   CU_ASSERT_EQUAL ( d->nav.instructions, GM_INSTRUCTION_ACCEPT );
    //assert text component
   CU_ASSERT_EQUAL ( d->txt.dynamic, false );
   CU_ASSERT_EQUAL ( strcmp (d->txt.message, "Cost is a 5\a018\a02"), 0 );
   CU_ASSERT_EQUAL ( d->txt.nb_lines, 3 );
   CU_ASSERT_EQUAL ( d->txt.wrap, true );

   gm_clear_text_buffer(&_engine->screen);
   gm_draw_dialog_stack(_test_screen, _engine );
   char screenstr[ GM_TEST_SCREEN_H * GM_TEST_SCREEN_W + 1];
   sprintf( screenstr, "   Msg    Cost    is  a   5 8        %c  ", GM_SCHAR_ACCEPT );
   CU_ASSERT_TRUE ( compare_text_buffer_letter(screenstr) );

   CU_ASSERT_TRUE( spotcheck_icon(3,3,1));
   CU_ASSERT_TRUE( spotcheck_icon(5,3,2));

}

void TS00_message_dialog ( void )
{  gm_dialog_close_all( _engine);
   gms_dialog *d = gm_dialog_open_message(_engine, "Note", "\vWarning\v Message");
   assert_or_exit( d != NULL, 2,"ERROR: Cannot open dialog" );
   //assert presence of components
   CU_ASSERT_EQUAL ( d->components, GM_COMPONENT_WINDOW
                    + GM_COMPONENT_NAVIGATION
                    + GM_COMPONENT_TEXT );
   //assert window component
   CU_ASSERT_EQUAL ( d->win.x, 1 );
   CU_ASSERT_EQUAL ( d->win.y, 2 );
   CU_ASSERT_EQUAL ( d->win.w, 6 );
   CU_ASSERT_EQUAL ( d->win.h, 4 );
   CU_ASSERT_EQUAL ( d->win.line_cursor, 1);
   CU_ASSERT_EQUAL ( strcmp (d->win.title, "Note"), 0);
   //assert navigation component
   CU_ASSERT_EQUAL ( d->nav.cursor, 0 );
   CU_ASSERT_EQUAL ( d->nav.nb_item, 0 );
   CU_ASSERT_EQUAL ( d->nav.nb_page, 1 );
   CU_ASSERT_EQUAL ( d->nav.page_size, 0 );
   CU_ASSERT_EQUAL ( d->nav.page_offset, 0 );
   CU_ASSERT_EQUAL ( d->nav.instructions, GM_INSTRUCTION_ACCEPT );
    //assert text component
   CU_ASSERT_EQUAL ( d->txt.dynamic, false );
   CU_ASSERT_EQUAL ( strcmp (d->txt.message, "\vWarning\v Message"), 0 );
   CU_ASSERT_EQUAL ( d->txt.nb_lines, 2 );
   CU_ASSERT_EQUAL ( d->txt.wrap, true );

   gm_clear_text_buffer(&_engine->screen);
   gm_draw_dialog_stack(_test_screen, _engine );
   char screenstr[ GM_TEST_SCREEN_H * GM_TEST_SCREEN_W + 1];
   sprintf( screenstr, "                   No     Warn    Mess      %c   ", GM_SCHAR_ACCEPT );

   CU_ASSERT_TRUE ( compare_text_buffer_letter(screenstr) );
   CU_ASSERT_TRUE( spotcheck_textcolor(4,2,GM_COLOR_BORDER));
   CU_ASSERT_TRUE( spotcheck_textcolor(2,3,GM_COLOR_UNDERLINE));
   CU_ASSERT_TRUE( spotcheck_textcolor(4,4,GM_COLOR_TEXT));
   CU_ASSERT_TRUE( spotcheck_bgglyph(2,2,GM_GLYPH_TOP_START));
   CU_ASSERT_TRUE( spotcheck_bgglyph(5,2,GM_GLYPH_TOP_END));
   CU_ASSERT_TRUE( spotcheck_bgglyph(3,5,GM_GLYPH_BOTTOM_START));
   CU_ASSERT_TRUE( spotcheck_bgglyph(5,5,GM_GLYPH_BOTTOM_END));
   CU_ASSERT_TRUE( spotcheck_bgglyph(6,5,GM_GLYPH_CORNER_BOTTOMRIGHT));

}

gms_item _itemlist[] = { {12,0,"abc"},{34,0,"def"},{56,0,"ghi"},{78,0,"jkl"},{90,0,"mno"},
                        {112,0,"pqr"},{234,0,"stu"},{356,0,"vwx"},{478,0,"yz"},{0,0,NULL}};

gms_item _shortitemlist[] = { {12,0,"abc"},{34,0,"def"},{56,0,"ghi"},{0,0,NULL}};

void TS00_list_dialog ( void )
{  gm_dialog_close_all( _engine);
   gms_dialog *d = gm_dialog_open_list(_engine, "List", 0,0,"msg",_shortitemlist);
   assert_or_exit( d != NULL, 2,"ERROR: Cannot open dialog" );

   //assert presence of components
   CU_ASSERT_EQUAL ( d->components, GM_COMPONENT_WINDOW
                    + GM_COMPONENT_NAVIGATION
                    + GM_COMPONENT_TEXT + GM_COMPONENT_LIST);
   //assert window component
   CU_ASSERT_EQUAL ( d->win.x, 0 );
   CU_ASSERT_EQUAL ( d->win.y, 0 );
   CU_ASSERT_EQUAL ( d->win.w, 8 );
   //printf("ANSWER=%d\n", d->win.w);
   CU_ASSERT_EQUAL ( d->win.h, 6 );
   //printf("ANSWER=%d\n", d->win.h);
   CU_ASSERT_EQUAL ( d->win.line_cursor, 1);
   CU_ASSERT_EQUAL ( strcmp (d->win.title, "List"), 0);
   //assert navigation component
   CU_ASSERT_EQUAL ( d->nav.cursor, 0 );
   CU_ASSERT_EQUAL ( d->nav.nb_item, 3 );
   CU_ASSERT_EQUAL ( d->nav.nb_page, 1 );
   CU_ASSERT_EQUAL ( d->nav.page_size, 3 );
   CU_ASSERT_EQUAL ( d->nav.page_offset, 0 );
   CU_ASSERT_EQUAL ( d->nav.instructions, GM_INSTRUCTION_ACCEPT + GM_INSTRUCTION_CANCEL + GM_INSTRUCTION_UPDOWN );
    //assert text component
   CU_ASSERT_EQUAL ( d->txt.dynamic, false );
   CU_ASSERT_EQUAL ( strcmp (d->txt.message, "msg"), 0 );
   CU_ASSERT_EQUAL ( d->txt.nb_lines, 1 );
   CU_ASSERT_EQUAL ( d->txt.wrap, false );
   //assert list component
   CU_ASSERT_EQUAL ( d->lst.length, 3 );
   CU_ASSERT_EQUAL ( d->lst.dynamic, false );
   CU_ASSERT_EQUAL ( d->lst.size, 0 );
   CU_ASSERT_EQUAL ( strcmp(d->lst.items[2].text, "ghi"), 0 ); //spotcheck

   gm_clear_text_buffer(&_engine->screen);
   gm_draw_dialog_stack(_test_screen, _engine );
   char screenstr[ GM_TEST_SCREEN_H * GM_TEST_SCREEN_W + 1];
   sprintf( screenstr, "  List   msg     %cabc     def     ghi      %c%c%c    ",
    GM_SCHAR_ARROW_RIGHT, GM_SCHAR_UPDOWN, GM_SCHAR_CANCEL, GM_SCHAR_ACCEPT );
   CU_ASSERT_TRUE ( compare_text_buffer_letter(screenstr) );

   CU_ASSERT_TRUE( spotcheck_textcolor(1,1,GM_COLOR_MESSAGE));
   CU_ASSERT_TRUE( spotcheck_textcolor(1,2,GM_COLOR_CURSOR));
   CU_ASSERT_TRUE( spotcheck_textcolor(5,0,GM_COLOR_BORDER));
   CU_ASSERT_TRUE( spotcheck_textcolor(2,2,GM_COLOR_TEXT));
   CU_ASSERT_TRUE( spotcheck_bgglyph(1,0,GM_GLYPH_TOP_START));
   CU_ASSERT_TRUE( spotcheck_bgglyph(6,0,GM_GLYPH_TOP_END));
   CU_ASSERT_TRUE( spotcheck_bgglyph(7,5,GM_GLYPH_CORNER_BOTTOMRIGHT));
}

//TODO: Add paged list in navigation, since more relevant over there
//TODO: Add dynamic list when shortcut dialogs like file list would be available.

void TS00_register_suite ( void )
{  CU_pSuite TS00 = CU_add_suite("00 Components and dialogs", TS00_setup, TS00_teardown);

   if (TS00 != NULL)
   {  CU_add_test (TS00, "Empty Dialog", TS00_empty_dialog );
      CU_add_test (TS00, "Background Dialog", TS00_background_dialog );
      CU_add_test( TS00, "Text Dialog", TS00_text_dialog);
      CU_add_test( TS00, "Message Dialog", TS00_message_dialog);
      CU_add_test( TS00, "List Dialog", TS00_list_dialog);
   }
}
