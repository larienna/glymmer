/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: June 20th, 2022
   @license: Apache 2.0 License

*/
//#define GM_ADD_DATABASE_SUPPORT

#include <allegro.h>
#include <stdio.h>
#include <stdbool.h>
#include <glymmer.h>
#include <gm_struct.h>
#include <gm_dialog_factory.h>
#include <component.h>
#include <gm_dialog_stack.h>
#include <textprint.h>


void debug_test_input ( /*gms_glymmer *gm */ void)
{
   printf ("DEBUG reading the keyboard test ... press ENTER\n");
   /*int key = 0;
   while ( key != KEY_ENTER )
   {  printf ("reading key\n" );
      key = readkey() >> 8;
      printf ("Key read is %d\n", key );
   }*/

   gms_input input;

   while ( input.kb_scancode != KEY_ENTER )
   {  printf ("reading key\n" );
      gm_reset_input( &input, GM_INPUT_KEYBOARD);
      gm_read_input( &input);
      printf ("Key read is %d\n", input.kb_scancode );
   }
   printf ("DEBUG please press SPACE BAR\n");
   gm_wait_for_keypress( KEY_SPACE);
}

void debug_test_empty_dialogs ( gms_glymmer *gm )
{

   BITMAP *bg = create_bitmap( SCREEN_W, SCREEN_H );

   rectfill ( bg, 0, 0, SCREEN_W, SCREEN_H, makecol(0, 240, 0 ) );

   gm_dialog_open_background(gm, "Background");
   gm_dialog_open_empty( gm, "Bonjour", 5, 5, 20, 10);
   gm_dialog_open_text ( gm, "CharName", -12, 5, 12, 10, "Dwarf\n\vDWizard\n\vELevel 12\n\vFHP 105/108\n\vGMP  50/ 75\n\vHPoisoned", true );
   //gm_dialog_open_message ( gm, "This is an important message", "This is only a test demonstration message\nnothing to worry about" );
   gm_dialog_open_confirm(gm, "I need a confirmation!", "Do you really want to do that?\nI really need to know!\nIt would help me a lot");

   //gm_dialog_open_pagedtext(gm, "Lorem Ipsum", 0, -20, 27, 10,
/*" Word1 Word2 Word3 Word4 Word5,  Word6 Word7 Word8 Word9 Word10,\n \
Word11 Word12 Word13 Word14 Word15,  Word16 Word17 Word18 Word19 Word20,^ \
Word21 Word22 Word23 Word24 Word25,  Word26 Word27 Word28 Word29 Word30,\n \
Word31 Word32 Word33 Word34 Word35,  Word36 Word37 Word38 Word39 Word40,^ \
Word41 Word42 Word43 Word44 Word45,  Word46 Word47 Word48 Word49 Word50, \n\
Word101 Word102 Word103 Word104 Word105,  Word106 Word107 Word108 Word109 Word110,\n \
Word111 Word112 Word113 Word114 Word115,  Word116 Word117 Word118 Word119 Word120,^ \
Word121 Word122 Word123 Word124 Word125,  Word126 Word127 Word128 Word129 Word130,\n \
Word131 Word132 Word133 Word134 Word135,  Word136 Word137 Word138 Word139 Word140,^ \
Word141 Word142 Word143 Word144 Word145,  Word146 Word147 Word148 Word149 Word150");*/

/*"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor orci eu posuere posuere. Morbi at ultricies metus. Duis in dui condimentum, volutpat purus non, consectetur lectus. Fusce ultricies ac sapien non posuere. Aliquam erat volutpat. Fusce id placerat tortor. Morbi ullamcorper vehicula metus at porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id eros lacus. In pulvinar diam sed ipsum maximus, id commodo mauris consectetur.\n\
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer placerat, erat at iaculis hendrerit, velit sem commodo odio, eget semper turpis augue et ante. Ut varius rutrum ipsum sed volutpat. Donec at augue eu tellus tincidunt ultricies. Duis hendrerit justo eget libero molestie, vitae fringilla mi iaculis. Phasellus nisi velit, varius quis malesuada luctus, suscipit et nunc. Morbi luctus vel mi quis rutrum. Praesent vel tempus enim, ut dignissim mauris. Duis eu lorem non quam euismod hendrerit eget quis dolor. Suspendisse porttitor condimentum erat at dapibus. Cras vel pellentesque metus, at volutpat est. In hac habitasse platea dictumst. Quisque vestibulum tortor quis ligula maximus efficitur.\n\
Donec pellentesque risus urna, sed lobortis ante dignissim sed. Integer mollis egestas nunc vitae volutpat. Donec finibus ante id auctor egestas. Sed ac ornare est, non facilisis tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam et lacus nec diam viverra vehicula et ut dolor. Suspendisse molestie non neque hendrerit euismod. ");
*/



   //printf ("Launching gm_show_dialogs\n" );
   int answer;
   answer = gm_show_dialogs( bg, gm);

   printf ("The answer selected was %d", answer );

   destroy_bitmap( bg);
}


void debug_test_list_dialogs ( gms_glymmer *gm )
{  gms_item listitems [] =        { { 0, 0, "00-Allo" },
                                    { 1, 0, "01-\vBonjour\v" },
                                    { 2, 0, "02-y�h�" },
                                    { 3, 0, "03-\vCa va" },
                                    { 4, 0, "04-On va " },
                                    { 5, 0, "05-\vBien\v" },
                                    { 6, 0, "06-Pas si" },
                                    { 7, 0, "07-pire" },
                                    { 8, 0, "08-\vapr�s\v tout" },
                                    { -1, 0,"09-Cancel" },
                                    { 0, 0, "10-Allo\v" },
                                    { 1, 0, "11-Bonjour" },
                                    { 2, 0, "12-Comment" },
                                    { 3, 0, "13-Ca va" },
                                    { 4, 0, "14-On va " },
                                    { 5, 0, "15-Bien" },
                                    { 6, 0, "16-Pas si" },
                                    { 7, 0, "17-pire" },
                                    { 8, 0, "18-apr�s tout" },
                                    { -1, 0,"19-Cancel" },
                                    { 0, 0, NULL } //terminator
                                    };

   gms_item smalllistitems [] = {   { 0, 0, "Allo" },
                                    { 1, 0, "\vBonjour\v" },
                                    { 2, 0, "Comment" },
                                    { 3, 0, "\vCa\v va" },
                                    { 4, 0, "On \vva\v" },
                                    { 5, 0, "Bien" },
                                    { 0, 0, NULL} //terminator
                                 };

   BITMAP *bg = create_bitmap( SCREEN_W, SCREEN_H );

   rectfill ( bg, 0, 0, SCREEN_W, SCREEN_H, makecol(0, 240, 0 ) );

   gm_dialog_open_background(gm, NULL);

   int answer;
   gm_dialog_open_list (gm, "Regular list", 1, 1, "This is an example program", smalllistitems );
   answer = gm_show_dialogs( bg, gm);

   gm_dialog_open_question_yesno( gm, "Make a choice", "Are you sure you want to do this ~super dangerous~ operation? `It will cost you 5\a01 and \v10\a02 to\v perform this operation.");
   answer = gm_show_dialogs( bg, gm);

   gm_dialog_open_pagedlist ( gm, "Paged list", 1, 20, 20, 9, "Another type of list", listitems );
   answer = gm_show_dialogs( bg, gm);

   printf ("The answer selected was %d", answer );


   destroy_bitmap( bg);
}

void debug_test_dynamic_dialogs ( gms_glymmer *gm )
{  BITMAP *bg = create_bitmap( SCREEN_W, SCREEN_H );

   rectfill ( bg, 0, 0, SCREEN_W, SCREEN_H, makecol(0, 240, 0 ) );

   gm_dialog_open_background(gm, "Dynamic List test");

   //gm_dialog_open_list (gm, "Regular list", 1, 1, "Make a choice", smalllistitems );
   //gm_dialog_open_pagedlist ( gm, "Paged list", 1, 1, 20, 9, "Make a choice", listitems );

   gms_dialog *dialog = gm_dialog_open_dynamic_pagedlist( gm, "File list", 1, 1, 40, 10, "Please chose a file from the list");


   //_____ Read file list _____

   struct al_ffblk info;
   int key = 0;

   if (al_findfirst("*", &info, 0) != 0)
   {  printf ("ERROR: Cannot find any files\n");
      exit(1);
   }

   do
   {  gm_list_item_add( gm, dialog, key, 0, info.name );
      //printf ("DEBUG: found file [%d]: %s\n", key, info.name );
      key++;
   }
   while (al_findnext(&info) == 0);

   al_findclose(&info);

   //_____ End read file list _____


   //_____ Reading readme file and displaying it into a dialog _____

   int size = 8;
   int nb_char = 0;
   char *msg = (char*) malloc (sizeof(char) * size );

   FILE *f = fopen ( "README.md", "r");
   while ( !feof ( f ) )
   {  msg[nb_char] = fgetc( f );
      nb_char++;
      if ( nb_char >= size )
      {  size = size *2;
         msg = (char*) realloc (msg, sizeof(char) * size );
      }
   }
   fclose ( f );
   msg[nb_char] = '\0';

   //printf ( "_____ Content of the file _____\n%s", msg );

   //_____ En read file _____

   gm_dialog_open_dynamic_pagedtext( gm, "README.md", 20, 2, 30, 30, msg);


   int answer;
   answer = gm_show_dialogs( bg, gm);

   printf ("The answer selected was %d", answer );
   destroy_bitmap( bg);
}

//Imported test for database
/*

void test_wui_data_dialogs ()
{  int init_status = init_game ();

   //printf ("DEBUG: Before bitmap\n" );
   BITMAP *bg = create_bitmap ( SCREEN_W, SCREEN_H );

   //printf ("DEBUG: Before floodfill\n" );
   floodfill ( bg, 0, 0, makecol(200, 200, 0 ));

   if ( init_status == INIT_SUCCESS )
   {  SQLopen ( "test.sqlite" );
      //SQLcreate ( "test.sqlite" );

      //Run once
      //SQLexec ("CREATE TABLE class ( pk INTEGER PRIMARY KEY, name TEXT, hp INTEGER, mp INTEGER )");
      //SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Fighter', 12, 0 )");
      //SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Thief',   8, 0 )");
      //SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Ninja',   6, 4 )");
      //SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Ranger',  10, 0 )");
      //SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Cleric',  8, 10 )");
      //SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Wizard',   4, 12 )");
      //SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Alchemist', 6, 8 )");
      //SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Psionic', 6, 8 )");
      //SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Samurai', 10, 6 )");

      dialog_open_database_list( "Classes", 5, 5, 20, 20,
                                "Name       HP  MP", "%10s D%2d D%2d",
                                "SELECT pk, name, hp, mp FROM class" );

      //TODO: Potential bug, must link each menu item to a primary key, probably
      //the first field, or the pk field, not sure

      int answer = dialog_show( bg, NULL, NULL );
      printf ("The answer was %d\n", answer );
      SQLclose ( );
   }

   destroy_bitmap ( bg );
   exit_game();
}


*/
