/**
   Glymmer

   @author: Eric Pietrocupo
   @date: August 16th, 2022
   @license: Apache 2.0 License

   Unitest main program. Isolated from the rest of the tests to avoid clashes of main.
*/

#include <stdbool.h>
#include <allegro.h>
#include <glymmer.h>
#include <CUnit/CUError.h>
#include <unitest.h>

int main ( void )
{
   all_test_setup();

   int nb_test_failed =  run_all_test();

   all_test_teardown();


   //printf("DEBUG: %d test failed", nb_test_failed);
   return nb_test_failed;
   //return 33;
}
