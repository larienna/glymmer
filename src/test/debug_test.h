/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: June 20th, 2022
   @license: Apache 2.0 License

   This is a series for manual tests to debug features of the project as they are developped.
*/

#ifndef DEBUG_TEST_H_INCLUDED
#define DEBUG_TEST_H_INCLUDED

void debug_test_input ( /*gms_glymmer *gm */ void);
void debug_test_empty_dialogs ( gms_glymmer *gm );
void debug_test_list_dialogs ( gms_glymmer *gm );
void debug_test_dynamic_dialogs ( gms_glymmer *gm );


#endif // DEBUG_TEST_H_INCLUDED
