/**
   Glymmer
   GLymmer private method definitions

   @author: Eric Pietrocupo
   @date: August 16th, 2022
   @license: Apache 2.0 License

   Test that wants to validate an hidden method in the library, will redefine the method here
   to be accessible

*/

#ifndef GLYMMER_PRIVATE_H_INCLUDED
#define GLYMMER_PRIVATE_H_INCLUDED


#ifdef __cplusplus
extern "C"
{
#endif



#ifdef __cplusplus
}
#endif


#endif // GLYMMER_PRIVATE_H_INCLUDED
