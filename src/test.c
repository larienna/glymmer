/************************************************************************/
/*                                                                      */
/*                        T E S T . C                                   */
/*                                                                      */
/*   Content:  test methods                                             */
/*   Programmer: Eric Pietrocupo                                        */
/*   Starting Date: October, 14th, 2017                                 */
/*                                                                      */
/*   List of testing method to be called from the main in order to test */
/*   various features. Also liberate the main from this junk code.      */
/*                                                                      */
/************************************************************************/

//#include <grpsys.h>
//#include <grpstd.h>
//#include <grpsql.h>
//#include <grpdbobj.h>
//#include <grpinterface.h>
//#include <grpengine.h>
#include <stdbool.h>
#include <stdio.h>
#include <allegro.h>
#include <video.h>
#include <allegrohelp.h>
#include <test.h>
#include <init.h>
#include <sqlite3.h>
#include <sqlwrap.h>
#include <textoutpatch.h>

#include <syslog.h>
#include <system.h>
#include <assets.h>
#include <wui.h>
#include <wuidialog.h>
#include <editor.h>
#include <mazetile.h>
#include <maze3d.h>
#include <mazefile.h>



int test_callback ( void )
{
   return 0;
}

void alternate_testing_main (void)
{
   //Menu testmenu ("allo");
   //List testlist ("Select your class", 4 , true);
   //Option opt_test ("Select your option");
   //int init_status;
   //int answer;
   //int answer2;
   //int error;
   //int loctime;
   //int clocktime;
   //int i;
   //int j;
   //int answer;
   //Race tmprace;
   //Game tmpgame;
   //Party tmparty;
   //CClass tmpclass;
   //Character tmpcharacter;
   //Item tmpitem;
   //Monster tmpmonster;
   //char tmpstr [100];
   //char tmpstr2 [100];



   //int value;
//   int i;

   //allegro_init();

   //init_sound();
   /*if ( detect_midi_driver ( MIDI_DIGMID ) > 0)
      printf ("Digimid is available\n");

   if ( detect_midi_driver ( MIDI_OSS ) > 0)
      printf ("Open Sound is available\n");

   if ( detect_midi_driver ( MIDI_ALSA ) > 0)
      printf ("Alsa is available\n");

   set_config_string ( "sound", "patches", "/home/ericp/DataPartition/dev/Wizardry/digmid.dat");

   set_config_string ( "sound", "midi_volume", "255" );
   set_config_string ( "sound", "midi_voices", "-1" );


   //set_config_string ( "sound", "patches", "digmid.dat");

   printf ("- Adding Digital and Midi audio\r\n");
   value = install_sound ( DIGI_AUTODETECT, MIDI_AUTODETECT, NULL);

   if ( value != 0 )
   {
      printf ( "ERROR: %s\n", allegro_error);
   }
   else
   {*/

   //}
   //s_Texture_palette testpal [32];

   //int i;

   //EnemyGroup tmpgroup;
   //List lst_temp ("Select your Enemy Group");

   //SQLactivate_errormsg();

//   int init_status = init_game ();
   //s_Texture_palette texpal;

//   if ( init_status == INIT_SUCCESS )
//   {
      //sound test

      /*draw_sprite ( buffer, datref_image[1], 0, 0 );

      copy_buffer();

      rest (1000);

      show_transition_slide_fade ( datref_image[1], Screen_FADE_OUT, Screen_SLIDE_RIGHT, 8);

      show_transition_slide_fade ( datref_image[1], Screen_FADE_IN, Screen_SLIDE_RIGHT, 8);

      draw_sprite ( buffer, datref_image[1], 0, 0 );

      copy_buffer();

      rest (1000);*/


     /* set_volume ( i, 255 );
      play_midi ( 1 );
      for ( i = 55; i <= 255 ; i += 50)
      {

         set_volume ( i, 255 );
         play_sound ( 1027);
         rest (5000);
      }*/


      //rest (3000);








      /*FILE * fp;

      fp = fopen ("anchor.txt", "w+");

      for ( i = 0 ; i < 15 ; i++)
      {
         fprintf (fp, "   { // Tile %d\n", i );

         for ( j = 0 ; j < 9 ; j++)
         {


            FLOOR_OBJECT [i][j] . anchor_x = OBJECT [i][j] . x + ( OBJECT[i][j] .width / 2);
            FLOOR_OBJECT [i][j] . anchor_y = OBJECT [i][j] . y;// + OBJECT[i][j].height;
            FLOOR_OBJECT [i][j] . scale = ( OBJECT [i][j] .width * 100 ) / 256;
            fprintf (fp, "      { %4d, %4d, %4d },\n",
                    FLOOR_OBJECT [i][j] . anchor_x,
                    FLOOR_OBJECT [i][j] . anchor_y,
                    FLOOR_OBJECT [i][j] . scale );

         }
         fprintf (fp, "   },\n");
      }

      fclose(fp);*/

      //DATAFILE *tmpmusic = load_datafile_object ( "/home/ericp/DataPartition/dev/Wizardry/datafile/musicmidi.dat", "W1CAMP_MID");
//      DATAFILE *tmpmusic = load_datafile_object ( "/home/ericp/AllegroSetup/setup.dat", "TEST_MIDI");
      //play_midi ( (MIDI*) tmpmusic -> dat , 0);

      //play_midi ( datref_musicmidi [ 0 ], 0);

      //play_midi ( 0 );

      //rest (2000);

      //unload_datafile_object (tmpmusic);
   //}
      /*Map testmap;

      testmap.write ( 0, 0, 0 );
      testmap.write ( 1, 1, 0 );
      testmap.write ( 2, 2, 0 );
      testmap.write ( 3, 3, 0 );

      testmap.display( party.position() );*/

      /*for ( i = 0; i < 15; i++ )
      {
         clear (screen);
         draw_sprite ( screen, datref_character [ i ], 0, 0 );
         answer = mainloop_readkeyboard();
         if ( answer == KEY_SPACE)
            make_screen_shot ("loadpng.bmp");
      }*/

      /*BITMAP *testpng = load_png ("01_fighter.png", NULL);

      draw_sprite ( screen, testpng, 0, 0 );
answer = mainloop_readkeyboard();*/

      /*set_volume ( 255, 255);
      set_hardware_volume(255, 255);

      load_midi_patches ();
      play_midi ( ASSETS_MUSIC_w1intro );
      play_sample ( SMP_sound025, 255, 128, 1000, 0 );

      mainloop_readkeyboard();*/

      /*FONT* testfont;
      FONT* testfont2;
      PALETTE tmpal;

      //tmpal = load_palette ("")
      testfont = load_bitmap_font ("BookAntiquaBold24.bmp", tmpal, NULL);

      if ( testfont != NULL)
      {


         if ( font_has_alpha (testfont) == TRUE)
         {
            printf ("Font has alpha\n");

            textout_old ( screen, testfont, "This font has an alpha channel", 20, 20, General_COLOR_TEXT );
         }
         else
         {
            printf ("Font has no alpha\n");

            textout_old ( screen, testfont, "This font has no alpha", 20, 20, General_COLOR_TEXT );
         }

         //copy_buffer();
         //mainloop_readkeyboard();
      }
      else
         printf ("Font could not be loaded\n");

      testfont2 = load_bitmap_font ("BookAntiquaBold24aa.bmp", tmpal, NULL);

      make_trans_font(testfont2);
      set_alpha_blender();

      if ( testfont2 != NULL)
      {


         if ( font_has_alpha (testfont2) == TRUE)
         {
            printf ("Font has alpha\n");

            textout_old ( screen, testfont2, "This font has an alpha channel", 20, 50, General_COLOR_TEXT );
         }
         else
         {
            printf ("Font has no alpha\n");

            textout_old ( screen, testfont2, "This font has not alpha", 20, 50, General_COLOR_TEXT );
         }

         //copy_buffer();
         mainloop_readkeyboard();
      }
      else
         printf ("Font could not be loaded\n");


      destroy_font (testfont);
      destroy_font (testfont2);*/

//-------------------- combat roll testing ----------------------------

/*   int nb_hit_dnd [ 6 ];
   int nb_hit_myd20 [ 6 ];
   int nb_hit_myd20m [ 6 ];
   int i;
   int nbhit;
   int j;
   int bonus;
   int TN = 30;
   int tmpdice;

   // target number 20, level 20 fighter so +20/+15/+10/+5/+0

   for ( i = 0; i <=5 ; i++)
   {
      nb_hit_dnd [ i ] = 0;
      nb_hit_myd20 [ i ] = 0;
      nb_hit_myd20m [ i ] = 0;
   }

   for ( i = 0 ; i < 10000 ; i++ )
   {
      nbhit=0;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 20 ) > TN || tmpdice >= 19 ) nbhit++;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 15 ) > TN || tmpdice >= 19 ) nbhit++;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 10 ) > TN || tmpdice >= 19 ) nbhit++;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 5 ) > TN || tmpdice >= 19 ) nbhit++;
      tmpdice = dice ( 20 );
      if ( ( tmpdice + 0 ) > TN || tmpdice >= 19 ) nbhit++;
      nb_hit_dnd [ nbhit ] ++;
   }

   for ( i = 0 ; i < 10000 ; i++ )
   {
      nbhit = 0;
      bonus = 0;
      for ( j = 0 ; j < 5 ; j++)
      {
         tmpdice = dice (20);
         if ( ( tmpdice + 12 + bonus ) > TN || tmpdice >= 19 )
         {

            bonus = 0;
            nbhit++;
         }
         else
            bonus += 2;
      }
      nb_hit_myd20 [ nbhit] ++;

   }

   for ( i = 0 ; i < 10000 ; i++ )
   {
      nbhit = 0;
      bonus = 0;
      for ( j = 0 ; j < 5 ; j++)
      {
         tmpdice = dice ( 20 );
         if ( ( tmpdice + 12 + bonus ) > TN  || tmpdice >= 19 )
         {

            bonus -= 2;
            nbhit++;
         }
         //else
         //   bonus = 0;
      }
      nb_hit_myd20m [ nbhit] ++;

   }

   printf ("Nb hit:     | 0  | 1  | 2  | 3  | 4  | 5  |\n");
   printf ("D&D system: |%4d|%4d|%4d|%4d|%4d|%4d|\n"
           , nb_hit_dnd [ 0 ],nb_hit_dnd [ 1 ],nb_hit_dnd [ 2 ],nb_hit_dnd [ 3 ],nb_hit_dnd [ 4 ],nb_hit_dnd [ 5 ]);
   printf ("My D20:     |%4d|%4d|%4d|%4d|%4d|%4d|\n",
            nb_hit_myd20 [ 0 ],nb_hit_myd20 [ 1 ],nb_hit_myd20 [ 2 ],nb_hit_myd20 [ 3 ],nb_hit_myd20 [ 4 ],nb_hit_myd20 [ 5 ]);
   printf ("My D20 malus|%4d|%4d|%4d|%4d|%4d|%4d|\n",
            nb_hit_myd20m [ 0 ],nb_hit_myd20m [ 1 ],nb_hit_myd20m [ 2 ],nb_hit_myd20m [ 3 ],nb_hit_myd20m [ 4 ],nb_hit_myd20m [ 5 ]);
*/
//--------------- Database reading test code --------------------------


//      Text tmptext;
      //ActiveEffect tmpeffect;
/*      Monster tmpmons;
      int error;

      SQLactivate_errormsg();
      error = SQLopen ( "adventure//DebuggingDemo//database.sqlite");

      if ( error == SQLITE_OK )
      {
         //printf ("I passed open \n");

         //tmpspell.SQLprocedure ( Spell_CB_PRINTF_NAME, "");


         error  = tmpmons.template_SQLprepare ( "");

         List lst_monster ("List of Monsters", 16);
         lst_monster.header ("Name                         HpDice Resistance");

         int i = 0;

         if ( error == SQLITE_OK )
         {
            //printf ("I passed prepare \n");
            error = tmpmons.SQLstep();

            while ( error == SQLITE_ROW)
            {

               //printf ( "%12s: cost:%3d : Hand: %2d : Dmgy %d\n", tmpitem.name(), tmpitem.price(), tmpitem.hand (), tmpitem.dmgy () );
               i++;
               lst_monster.add_itemf ( i, "%-20s %2d %6d %s ", tmpmons.name(), tmpmons.hpdice(),
                                      tmpmons.resistance(), tmpmons.resistance_str() );
               if ( i % 50 == 0)
                  tmpmons.SQLinsert();

               error = tmpmons.SQLstep();
            }
         }

         WinList wlst_effect ( lst_monster, 20, 20 );
         Window::show_all();

      }

      tmpmons.SQLfinalize();
*/


         //tmpspell.SQLfinalize ();

         /*for ( i = 0 ; i < 22; i++)
            tmpeffect.set_int ( i , i);

         tmpeffect.set_str ( 0, "2nd Test Effect");
         tmpeffect.set_str ( 2, "Cond Test");

         tmpeffect.SQLinsert ();*/

         //tmpeffect.template_SQLselect ( 2 );


         //tmpeffect.SQLinsert();
//
         //tmpeffect.print_table_structure ();

         /*error = tmptext.SQLprepare ();



         if ( error == SQLITE_OK )
         {
            printf ("I passed prepare \n");
            error = tmptext.SQLstep();

            if ( error == SQLITE_ROW)
            {
               printf ("I passed step \n");*/
               /*lst_temp.add_itemf (0, "f=%d,a=%d,r=%d,b=%d,t=%d,p=%d,e1=%d,e2=%d,e3=%d,e4=%d,e5=%d,e6=%d,e7=%d,e8=%d",
                                 tmpgroup.floor(),
                                 tmpgroup.area(),
                                 tmpgroup.repeat(),
                                 tmpgroup.bonus(),
                                 tmpgroup.bonus_target(),
                                 tmpgroup.probability(),
                                 tmpgroup.enemy ( 0 ),
                                 tmpgroup.enemy ( 1 ),
                                 tmpgroup.enemy ( 2 ),
                                 tmpgroup.enemy ( 3 ),
                                 tmpgroup.enemy ( 4 ),
                                 tmpgroup.enemy ( 5 ),
                                 tmpgroup.enemy ( 6 ),
                                 tmpgroup.enemy ( 7 )                 );*/

    //           rect ( buffer, 0, 0, 639, 479, General_COLOR_TEXT );
/*
               textout_old ( buffer, text_font, tmptext.content(), 0, 0, General_COLOR_TEXT );

               FONT *tmpfnt = ASSETS_FONT_bookantiquabold24;
//ASSETS_FONT_deutschegothicbold48
//ASSETS_FONT_elementarygothicbookhand32
//bookmanoldstyle24
//haettenscheweiler24
//monotypecorsiva24
//bookantiquabold24
               tmptext.format ( 640, tmpfnt  );

               char *tmpstr = tmptext.readline_start();
               int tmpy = 0;// text_height ( tmpfnt);

               while ( tmpstr != NULL)
               {
                  tmpy += text_height ( tmpfnt);
                  textout_justify_old ( buffer, tmpfnt, tmpstr, 0, 640, tmpy, 256,General_COLOR_TEXT);
                  tmpstr = tmptext.readline_next();
               }



               error = tmptext.SQLstep();
            }

            tmptext.SQLfinalize();
         }
      }*/
/*
//      WinList wlst_temp ( lst_temp, 0, 00 );
//      Window::show_all();

      copy_buffer();

      mainloop_readkeyboard();
      mainloop_readkeyboard();*/

//----------------------------------------------------------------

      //printf ("Main: Pass 1\n" );
/*
      ImageList imglst_test ( "Select a Picture", 4, 3,128, 128, true );

     // printf ("Main: Pass 2\n" );
      //for ( i = 2048 ; i < 2248 ; i++ )
      imglst_test.add_datref ( datref_portrait );

      //printf ("Main: Pass 3\n" );

      //answer = imglst_test.show ( 0, 0 );
      WinImageList wimglst_test ( imglst_test, 100, 10 );

      answer = Window::show_all();

      //printf ("Main: Pass 4\n" );

      printf ("Answer was %d\n", answer );
*/
     // for ( i = 0; i < 32; i++ )
       //  build_texture_palette (testpal[i], i);

      /*maze.load_hardcoded();

      party.position ( 0, 0, 0, Party_FACE_NORTH );
      maze.start( true );*/

      /*TexPalette floor;
      BITMAP* floorbmp;


      floor.texcode ( 0, 2048 );
      floor.texposition ( 0, TexPalette_TILING_BOTH + TexPalette_ZOOM_X2, 0, 0 );
      floor.texcode ( 1, 1027 );
      floor.texposition (1, TexPalette_TILING_BOTH + TexPalette_ZOOM_X4, 8, 4 );


      floorbmp = floor.build();


      blit ( floorbmp, screen, 0, 0, 0, 0, floorbmp->w, floorbmp->h );

      while ( mainloop_readkeyboard() != KEY_ENTER );*/


//   }

   /*Syslog systemlog;

   for ( i = 0 ; i < 10 ; i++)
      systemlog.writef ( "Message number: %d", i );

   for ( i = 0; i <5; i++ )
      printf ("%d: %s\n", i, systemlog.read(i));

   while ( mainloop_readkeyboard() != KEY_ENTER );
*/

   //Event tmpevent;
   //tmpevent.start();
   /*answer = SQLopen ( "adventure//template//database.sqlite");

   //tmpevent.start();

  SQLerrormsg_active = true;

  if ( answer == SQLITE_OK)
  {
     printf ("before select\n");
     answer = tmpevent.SQLselect ( 1 );

     if ( answer == SQLITE_ROW )
     {
        printf ("query works\n");
        tmpevent.start ();
     }
  }
  else
  {


     printf ("could not open database\n error=%d", answer);
  }*/

  //}
/*   if ( answer == SQLITE_OK)
   {
      List lst_monster ("Select an monster", 8);

      i = 0;

      error = tmpmonster.SQLprepare("");
      printf ("\r\npass 0");
      if ( error == SQLITE_OK )
      {
         printf ("\r\npass 1");
         error = tmpmonster.SQLstep();
         tmpmonster.SQLerrormsg();

         while ( error == SQLITE_ROW )
         {
            printf ("\r\npass 2");


            lst_monster.add_itemf ( 0, "name=%s, level=%d, size=%d, AD=%d clumsy=%d like_front=%d, ",
                                tmpmonster.name(),
                                tmpmonster.level(),
                                tmpmonster.size(),
                                tmpmonster.AD(),
                                tmpmonster.clumsiness(),
                                tmpmonster.like_front () );

*/
            /*printf ( "name=%s, price=%d, type=%d, category=%d, dmgz=%d AD=%d\r\n",
                                tmpitem.name(), tmpitem.price(), tmpitem.type(), tmpitem.category(),
                                tmpitem.attribute(), tmpitem.xyzstat(XYZSTAT_DMG, XYZMOD_ZEQUIP),
                    tmpitem.d20stat(D20STAT_AD));*/

/*


            error =  tmpmonster.SQLstep();
         }
      }

      tmpmonster.SQLfinalize();

      WinList wlst_monster ( lst_monster, 20, 50);

      Window::show_all();


   }

*/
/*   printf ("- closing SQL Database\r\n");
   SQLclose ();
   printf (" after SQL close\n");
*/

}



// This is a procedure used for converting bitmap for datafile processing
// but it is not used by the game once released
void tmpmain_convert_bitmap ( void )
{
   /*REFACTORING: requires cpp support for now

   DATAFILE *tmpdatf;
   int init_status = init_game ();
   int i = 0;
   bool filecomplete = false;
   int x = 18;
   int answer;
   BITMAP *tmpcopy = create_bitmap ( 144, 144 );
   BITMAP *tmpstretch = create_bitmap (128, 128);
   PALETTE pal;
   char tmpstr [100] = "";

   if ( init_status == INIT_SUCCESS )
   {
      tmpdatf = load_datafile ("tmpwiz8portrait.dat");

      while ( filecomplete == false)
      {


         clear_to_color ( screen, makecol (0, 0, 0));
         rectfill ( screen, 0, 0, 200, 328, makecol (200, 200, 200));

         draw_sprite ( screen, (BITMAP*) tmpdatf [ i ] . dat, 10, 10 );
         rect ( screen, x+10, 10, x + 154, 154, makecol ( 255, 255, 255));

         answer = mainloop_readkeyboard();

         switch ( answer )
         {
            case KEY_ENTER:

               blit ( (BITMAP*) tmpdatf [ i ] . dat, tmpcopy,
                     x, 0, 0, 0, 144, 144 );
               stretch_sprite ( tmpstretch, tmpcopy, 0, 0, 128, 128 );


               //draw_sprite ( screen, tmpstretch, 10, 200 );

               sprintf ( tmpstr, "output//portrait%03d.bmp", i);
               save_bmp ( tmpstr, tmpstretch, pal);



               //mainloop_readkeyboard();

               i++;
               x=18;

            break;
            case KEY_LEFT:
               if ( x > 0)
                 x--;
            break;
            case KEY_RIGHT:
               if ( x < 35)
               x++;
         }


         if ( tmpdatf [ i ] . type == DAT_ID('i','n','f','o') )
         {
            filecomplete = true;
         }

      }

      unload_datafile (tmpdatf);
   }
   */
}

void recordset_test_main ( void )
{
   /*REFACTORING require cpp support for now

   //s_SQLrecordset rs;
   //int sqlerror;

   printf ("Test method has been called\n");

   SQLopen ("adventure/DebuggingDemo/database.sqlite" );

   //SQLrs_create ( &rs, SQLdb, "SELECT * FROM monster_category");
   //SQLrs_createf ( &rs, SQLdb, "SELECT %c FROM %s WHERE pk=%d ORDER BY %s"
   //   , '*', "monster_category", 22 , "name" );

  // SQLrs_print ( rs );

  // SQLrs_free( rs );

   if ( SQLpreparef( "SELECT %s,%s FROM %s" , "name", "pictureID", "monster_category" )
        == SQLITE_OK )
   {
      while ( SQLstep() == SQLITE_ROW)
      {
         printf ("%s: %d\n", SQLcolumn_text(0), SQLcolumn_int(1));
      }
   }

   SQLclose (  );
   */
}

void test_wui ()
{  //NOTE: need game init functions, so cannot initiate WUI tests before datafilereference
   //and the entire system group is converted.
   s_wui_window win;
   s_wui_window win2;
   s_wui_window winbg;

   window_init (&win, "A TITLE", 1, 1, 18, 25);
   window_init (&win2, "Another title-", 13, 1, 25, 25);
   window_init (&winbg, NULL, 0, 0, 40, 30 );

   printf ("\nDEBUG:final height = %d", win.h );
   printf ("\nDEBUG:final height = %d", win2.h );

   window_text_print ( &win, "012345678901234567890123456789" );
   //window_text_print ( &win, "NEW LINE" );
   //window_text_print ( &win, "AGAIN^NL" );
   window_text_printwrap ( &win, "Another BCDBCD BCD-BCD of text that should overflow the window size even if wrapped correctly!", 0 );
   //window_text_printf ( &win, "STR %s", "BONJOUR" );

   window_text_print ( &win2, "012345678901234567890123456789" );
   //window_text_printlinef ( &win2, "WWINDOW %d", 1 );
   //window_text_print ( &win2, "CR\nNEW LINE" );
   //window_text_print ( &win2, "AGAIN^NL" );
   window_text_printwrap ( &win2, "Another BCD<br>\n BC-BBCD CBDB<br>\n BC of text that<br>\n should overflow the<br>\n window size even<br>\n if wrapped correctly!", 0 );
   window_text_printf ( &win2, "STR %s", "BONJOUR" );

   //printf ("\nDEBUGGING window content\n%s", win.text );
   //printf ("\nDEBUGGING window content\n%s", win2.text );

   int init_status = init_game ();

   if ( init_status == INIT_SUCCESS )
   {
      /*textprintf_old( buffer, text_font, 0, 0,
         General_COLOR_TEXT, "This is a window,");
      textprintf_old( buffer, text_font, 0, 12,
         General_COLOR_TEXT, "is this text to measure it");
      textprintf_old( buffer, text_font, 0, 24,
        sGeneral_COLOR_TEXT, "I am inserting");
      textprintf_old( buffer, text_font, 0, 36,
         General_COLOR_TEXT, "multiple lines");
      textprintf_old( buffer, text_font, 0, 48,
         General_COLOR_TEXT, "to be able to ");
      textprintf_old( buffer, text_font, 0, 60,
         General_COLOR_TEXT, "measure the y axis");*/

//      window_draw ( &winbg,  makecol (0,  50, 0 ), true);

//      window_draw ( &win,  makecol (0,  50, 0 ), true);

      //window_draw ( &win2,  makecol ( 0, 0, 50 ), true );
     //window_draw_text ( &win2, makecol ( 0, 0, 50 ) );

      copy_buffer();
  //    rest(5000);
      while ( (readkey() >> 8) != KEY_ENTER );
      //printf ("a key was presses\n");
   }

   //window_dispose ( &win );
   //window_dispose ( &win2 );

   exit_game();

}

void test_wui_dialog ()
{ s_wui_menu_item menuitems [] = { { 0, "~AAllo" },
                                    { 1, "~BBonjour" },
                                    { 2, "~CComment" },
                                    { 3, "~DCa va" },
                                    { 4, "~EOn va " },
                                    { 5, "~FBien" },
                                    { 6, "~GPas si" },
                                    { 7, "~Hpire" },
                                    { 8, "~Iapres tout" },
                                    { -1, "~JCancel" },
                                    { 0, "~aAllo" },
                                    { 1, "~bBonjour" },
                                    { 2, "~cComment" },
                                    { 3, "~dCa va" },
                                    { 4, "~eOn va " },
                                    { 5, "~fBien" },
                                    { 6, "~gPas si" },
                                    { 7, "~hpire" },
                                    { 8, "~iapres tout" },
                                    { -1, "~jCancel" }

                                    };


   int init_status = init_game ();

   //printf ("DEBUG: Before bitmap\n" );
   BITMAP *bg = create_bitmap ( SCREEN_W, SCREEN_H );

   //printf ("DEBUG: Before floodfill\n" );
   floodfill ( bg, 0, 0, makecol(200, 200, 0 ));
   //printf ("DEBUG: after bitmap\n" );

   if ( init_status == INIT_SUCCESS )
   {  dialog_open_background ("Wizardry Legacy" );
      //dialog_open_empty ("Windows 2 ", 2, 2, 20, 10 );
//      dialog_open_text  ("A Window", 1 , 15, 38, 10, "This is a ~Emessage~A that I want to print inside the  window that should be ~Gjustified~A wrappred apropriately with the right ~Dcolor codes~A excluded from the justification." );
      dialog_open_text  ("CharName", -12, 5, 12, 12, "Dwarf\n~DWizard\n~ELevel 12\n~FHP 105/108\n~GMP  50/ 75\n~HPoisoned", true );
      /*dialog_open_pagedtext("Lorem Ipsum", 0, -20, 27, 10,
" Word1 Word2 Word3 Word4 Word5,  Word6 Word7 Word8 Word9 Word10,\n \
Word11 Word12 Word13 Word14 Word15,  Word16 Word17 Word18 Word19 Word20,^ \
Word21 Word22 Word23 Word24 Word25,  Word26 Word27 Word28 Word29 Word30,\n \
Word31 Word32 Word33 Word34 Word35,  Word36 Word37 Word38 Word39 Word40,^ \
Word41 Word42 Word43 Word44 Word45,  Word46 Word47 Word48 Word49 Word50, \n\
Word101 Word102 Word103 Word104 Word105,  Word106 Word107 Word108 Word109 Word110,\n \
Word111 Word112 Word113 Word114 Word115,  Word116 Word117 Word118 Word119 Word120,^ \
Word121 Word122 Word123 Word124 Word125,  Word126 Word127 Word128 Word129 Word130,\n \
Word131 Word132 Word133 Word134 Word135,  Word136 Word137 Word138 Word139 Word140,^ \
Word141 Word142 Word143 Word144 Word145,  Word146 Word147 Word148 Word149 Word150");*/

/*"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas porttitor orci eu posuere posuere. Morbi at ultricies metus. Duis in dui condimentum, volutpat purus non, consectetur lectus. Fusce ultricies ac sapien non posuere. Aliquam erat volutpat. Fusce id placerat tortor. Morbi ullamcorper vehicula metus at porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed id eros lacus. In pulvinar diam sed ipsum maximus, id commodo mauris consectetur.\n\
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer placerat, erat at iaculis hendrerit, velit sem commodo odio, eget semper turpis augue et ante. Ut varius rutrum ipsum sed volutpat. Donec at augue eu tellus tincidunt ultricies. Duis hendrerit justo eget libero molestie, vitae fringilla mi iaculis. Phasellus nisi velit, varius quis malesuada luctus, suscipit et nunc. Morbi luctus vel mi quis rutrum. Praesent vel tempus enim, ut dignissim mauris. Duis eu lorem non quam euismod hendrerit eget quis dolor. Suspendisse porttitor condimentum erat at dapibus. Cras vel pellentesque metus, at volutpat est. In hac habitasse platea dictumst. Quisque vestibulum tortor quis ligula maximus efficitur.\n\
Donec pellentesque risus urna, sed lobortis ante dignissim sed. Integer mollis egestas nunc vitae volutpat. Donec finibus ante id auctor egestas. Sed ac ornare est, non facilisis tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam et lacus nec diam viverra vehicula et ut dolor. Suspendisse molestie non neque hendrerit euismod. ");*/


      //dialog_open_menu ("Menu", 1, 1, "Make a choice", menuitems, SIZEOFMENU(menuitems) );
      dialog_open_list ("List", 1, 1, 20, 10, "Make a choice", menuitems, SIZEOFMENU(menuitems) );
      //dialog_hide( 2 );
      //dialog_open_message( "A Message", "Here is your question that is a bit too long for the testing.");
      //dialog_open_question( "A Question", "Here is your question that is a bit too long for the testing.");
      int answer = dialog_show( bg, NULL, NULL );
      printf ("The answer was %d\n", answer );

      //int nblineA = anticipate_printwrap_nb_lines( "This is a ~Emessage~A that I want to print inside the  window that should be ~Gjustified~A wrappred apropriately with the right ~Dcolor codes~A excluded from the justification.", 30);
      //int nblineB = anticipate_printwrap_nb_lines( "Dwarf\n~DWizard\n~ELevel 12\n~FHP 105/108\n~GMP  50/ 75\n~HPoisoned", 12 );
      //printf ("The nb of lines were %d, %d\n", nblineA, nblineB );
   }

   //printf ("%d mod %d = %d\n", -12, WUI_SCREEN_WIDTH, modulo (-12,WUI_SCREEN_WIDTH) );
   //printf ("Sizeof menuitems = %d\n", SIZEOFMENU(menuitems) /*sizeof (menuitems)/ sizeof (s_wui_menu_item)*/ );

   destroy_bitmap ( bg );
   exit_game();
}

void test_wui_smallfont ()
{  int init_status = init_game ();

   if ( init_status == INIT_SUCCESS )
   {  FONT* fnt = text_font; //ASSETS_FONT_wizardrysmall;
      int fgcolor = makecol ( 225, 225, 225 );

      textprintf_old ( buffer, fnt, 0, 0, fgcolor, "Bonjour comment ca va" );
      textprintf_old ( buffer, fnt, 0, 12, fgcolor, "Ceci est un exemple de texte" );
      textprintf_old ( buffer, fnt, 0, 24, fgcolor, "Affiche avec des polices plus petites " );
      textprintf_old ( buffer, fnt, 0, 36, fgcolor, "IL A UNE LARGEUR MAXIMALE DE CINQUANTE TROIS CARACTERES" );

      copy_buffer();
  //    rest(5000);
      while ( (readkey() >> 8) != KEY_ENTER );
   }

   exit_game();
}

void test_debug_code ()
{  /*char *str = "Why\ndoes this stringoftext cut\ntoo soon";
   int len = strlen ( str );
   int pos = 0;
   int nb_char = 0;

   while ( pos < len )
   {
      nb_char = wrapped_nb_char_on_line ( str, len, 12, &pos );
      printf ("DEBUG: nb_char= %d, pos= %d\n", nb_char, pos );
   }*/

   sqlite3 *db;
   int error = sqlite3_open_v2("test.sqlite", &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL );

   if ( error == SQLITE_OK )
   {  sqlite3_exec ( db, "CREATE TABLE class ( pk INTEGER PRIMARY KEY, name TEXT, hp INTEGER, mp INTEGER )", NULL, NULL, NULL );

      sqlite3_close_v2(db);
   }
   else
   {  printf("Error: %s\n", sqlite3_errmsg(db));
   }

}

void test_wui_data_dialogs ()
{  int init_status = init_game ();

   //printf ("DEBUG: Before bitmap\n" );
   BITMAP *bg = create_bitmap ( SCREEN_W, SCREEN_H );

   //printf ("DEBUG: Before floodfill\n" );
   floodfill ( bg, 0, 0, makecol(200, 200, 0 ));

   if ( init_status == INIT_SUCCESS )
   {  SQLopen ( "test.sqlite" );
      //SQLcreate ( "test.sqlite" );

      /*SQLexec ("CREATE TABLE class ( pk INTEGER PRIMARY KEY, name TEXT, hp INTEGER, mp INTEGER )");
      SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Fighter', 12, 0 )");
      SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Thief',   8, 0 )");
      SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Ninja',   6, 4 )");
      SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Ranger',  10, 0 )");
      SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Cleric',  8, 10 )");
      SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Wizard',   4, 12 )");
      SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Alchemist', 6, 8 )");
      SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Psionic', 6, 8 )");
      SQLexec ("INSERT INTO class ( name, hp, mp ) VALUES ( 'Samurai', 10, 6 )");*/

      dialog_open_database_list( "Classes", 5, 5, 20, 20,
                                "Name       HP  MP", "%10s D%2d D%2d",
                                "SELECT pk, name, hp, mp FROM class" );

      //TODO: Potential bug, must link each menu item to a primary key, probably
      //the first field, or the pk field, not sure

      int answer = dialog_show( bg, NULL, NULL );
      printf ("The answer was %d\n", answer );
      SQLclose ( );
   }

   destroy_bitmap ( bg );
   exit_game();
}

void test_datafile ()
{  int init_status = init_game ();

   if ( init_status == INIT_SUCCESS )
   {
      BITMAP *bmp = asset_texture("DECORATION_FRESCO_SUN_BMP");
      BITMAP *img = asset_image("TOMB_BMP");
      //BITMAP *img = asset_default_bitmap16();
      //BITMAP *bmp = asset_default_bitmap256();

      blit(bmp, buffer, 0,0,0,0,bmp->w, bmp->h );
      blit(img, buffer, 0,0,320,240,img->w, img->h );

      //sleep (1);
      //asset_play_music( "W1INTRO_MID", true );
      //asset_play_sound( "VICTORY_WAV");

      copy_buffer();

      while ( (readkey() >> 8) != KEY_ENTER );

      //Test datafile iterators

      s_datafile_iterator iter;

      init_iterator ( &iter, DATBMP_TEXTURE );

      do
      {
         bmp = get_iterator_asset( &iter );
         blit(bmp, buffer, 0,0,0,0,bmp->w, bmp->h );
         copy_buffer();
         while ( (readkey() >> 8) != KEY_ENTER );
      }
      while ( inc_iterator( &iter ) == true && iter.index < 10 );

      bmp = asset_bitmap_byindex( DATBMP_MONSTER, 12 );
      blit(bmp, buffer, 0,0,0,0,bmp->w, bmp->h );
      copy_buffer();
      while ( (readkey() >> 8) != KEY_ENTER );

   }
    exit_game();
}

void test_editor( void )
{  int init_status = init_game ();

   if ( init_status == INIT_SUCCESS )
   {  editor_main();
   }
   exit_game();
}

void test_draw_3dmaze (void)
{  int init_status = init_game ();

   if ( init_status == INIT_SUCCESS )
   {  load_mazefile( "adventure/DebuggingDemo/maze.bin");
      s_camera camera = { .x = 0, .y = 0, .z = 0, .facing = 0 };

      draw_3dmaze( &camera, true );

      blit_background();
      copy_buffer();
      while ( (readkey() >> 8) != KEY_ENTER );

   }
   exit_game();
}

void test_function_launcher (void)
{
   fxl_init(4);
   character_add_fxl();

   fxl_interactive_command_line("[Wizardry] > ", 256);

   fxl_destroy();
   //TODO first try without any fxl engine, just call business logic make sure the operations
   //are performed into the database
   //TODO add a generic show command to display stored views
}











//NOTE: THis is some random testing code I found in the project.

//-------------------------------- Code Testing ---------------------
        /*
   short i;
   Item tmpitem;
   int tmptag;

   unsigned int index = db.search_table_entry ( DBTABLE_ITEM );
   char tmpstr [ 100 ];

   // random value for eachitem types : Weap, shld, armr, accs, exp, other
   int rndval [ 6 ] = { 4, 5, 2, 8, 20, 1 };
   int min [ 6 ] = { 2, 3, 1, 4, 5, 1 };
   int tmptype;
   short tmpvar;

   i = 0;
 //  j = 0;

   // the list of items will regenerate itself at each call.
   // make a sub function to generate
   while ( db.entry_table_tag ( index ) == DBTABLE_ITEM )
   {
      tmpitem.SQLselect ( index );
      tmptag = tmpitem.tag();
      if ( ( tmpitem.type() == Item_TYPE_EXPANDABLE )
         && tmptag.source() != DBSOURCE_SAVEGAME )
      {
//         p_item [ i ] . itemtag = tmpitem.tag();
//         p_item [ i ] . type = tmpitem.type();
         tmptype = tmpitem.type();
//         p_item [ i ] . quantity = 5;
         sprintf ( tmpstr, "%s W:%f S:%d R:%d P:%d", tmpitem.name(), tmpitem.weight(),
             tmpitem.status(), tmpitem.rarity(), tmpitem.price() );
         debug ( tmpstr );
         tmpvar = min [ tmptype ]+rnd( rndval [ tmptype ] );
         i++;
      }
      index++;
   }
      */

   //?? testing Super Interfaces

/*   SICommand ( "This is a test menu", SICMDITEM_TEST, 50, 50 );*/

   //?? testing adventure

//   clear_bitmap ( screen );
//   Adventure testadv;

//   testadv.show_intro  ();
//   testadv.show_ending ();

   //?? db tes ---

//   textout_centre_old( buffer, text_font,
//      "This is a database test", 320, 0, General_COLOR_TEXT );

/*   Ennemy tmpenn;
   unsigned int index = db.search_table_entry ( DBTABLE_MONSTER );
   short i = 0;
   bool accept_ennemy;
   short y = 0;


   short nb_ennemy = 0;

   do
   {
      tmpenn.alDBselect ( Ennemy_DATATYPE_MONSTER, index );
      accept_ennemy = false;

      if ( ( tmpenn.level() >= 1 )
         && ( tmpenn.level() <= 3 ) )
         accept_ennemy = true;

      //?? use mask to block additional monsters

      if ( accept_ennemy == true )
      {
         textprintf_old ( buffer, text_font, 20, y, General_COLOR_TEXT,
            "- %s | %s | level %2d | size %d |",
         tmpenn.cname(), tmpenn.cgroup_name(), tmpenn.level(), tmpenn.size() );
         i++;
         y+=16;
         nb_ennemy++;
      }
      index++;
   }
   while ( db.entry_table_tag ( index ) == DBTABLE_MONSTER );

   copy_buffer();
   while ( ( readkey()>>8) != KEY_ENTER );*/
