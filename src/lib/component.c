/**
   Glymmer

   @author: Eric Pietrocupo
   @date: January 17th, 2022
   @license: Apache 2.0 License

   Handles windows components of the dialog.
*/

#include <stdbool.h>
#include <stdio.h>
#include <stdarg.h>
#include <allegro.h>
#include <gm_struct.h>
#include <gm_input.h>
#include <draw.h>
#include <component.h>
#include <gm_common.h>
#include <textprint.h>

/*! @brief Helper function that verifies the presence of a component.
   Used to make sure the bitwise operator are used correctly by encapsulating the process.
   Each bit is a different component.
*/

bool has_component ( gms_dialog *dlg, int component )
{  return ( ( dlg->components & component ) > 0 );
}



//_______________________________ Window ________________________________

/*! @brief Initialise the window component.

   @param [in] e Engine structure containing screen and font information
   @param [in] win The window component to initialise
   @param [in] title String of text to add in the window frame. Set to NULL if no title
             is desired. Title cannot be larger than window.
   @param [in] x position of the window in nb of characters, negative values align the window
               the the right side of the screen
   @param [in] y position of the window, negative values align the window to the bottom
   @param [in] w width of the window in nb of character, must include frame (1 char each side)
   @param [in] h height of the window
*/

void gm_window_add ( gms_dialog *dlg, gms_screen *e, char *title, int x, int y, int w, int h)
{  dlg->components = dlg->components | GM_COMPONENT_WINDOW;
   dlg->win.x = modulo (x ,e->text_w); //modulo allows negative possitions to right align
   dlg->win.y = modulo (y ,e->text_h);
   dlg->win.h = h;
   dlg->win.w = w;
   dlg->win.title = title;
   dlg->win.line_cursor = 1;


}


/*! @brief Destroys the dynamically allocated content if necessary */
void gm_window_destroy ( gms_window *win )
{  //NOTE: hide warning, does nothing
   win+=0;
   //currently does nothing, unless "title" eventually gets dynamically allocated.
}

//___________________________________ Navigation _____________________________________

/*! @brief Bitwise verification for navigation instructions
*/

bool has_instruction ( gms_navigation *nav, int instruction )
{  return ( ( nav->instructions & instruction ) > 0 );
}

/*! @brief Initialise the navigation component
*/

void gm_navigation_add ( gms_dialog *dlg, int nb_item, int page_size, int nb_page, unsigned int instructions )
{  dlg->components = dlg->components | GM_COMPONENT_NAVIGATION;
   dlg->nav.cursor = 0;
   dlg->nav.nb_item = nb_item;
   dlg->nav.page_size = page_size;
   dlg->nav.instructions = instructions;
   dlg->nav.nb_page = nb_page;
   //dlg->nav.pos_in_page = 0;
   dlg->nav.page_offset = 0;
   //printf ("DEBUG add () instructions was %d\n", dlg->nav.instructions );
}

/*! @brief Apply the user input to the dialog's navigation
   The input must have been previously read and converted to standard game input

   @param nav[out] the navigation structure of the dialog
   @param answer[out] The navigation cursor will be returned, -1 in case of cancel.
   @param input[in] of the user to interpret.

   @return The return value is true if a choice has been made, else dialog update will
           be required.
*/

bool gm_navigation_apply_input ( gms_navigation *nav, int *choice, gms_input *input )
{  *choice = -1;
   static char shotstr [ 18 ]; //! temporary contains name of screenshot
   static int shotID = 0;      //! count screenshots
   //printf ("DEBUG insde apply_input() instructions was %d\n", nav->instructions );

   if ( (input->modified & GM_INPUT_KEYBOARD ) > 0 )
   {  switch ( input->kb_scancode )
      {  case KEY_Z:
            //NOTE: accept is the default key even if not shown, replaced enter
            //if ( has_instruction(dlg, WUI_INS_ACCEPT))
            //{  //TODO: if not masked, else ignore selection
            //TODO put menu interpretation into a separate function for lists only
               /*if ( has_component ( dlg, GM_COMPONENT_MENU ))
               {  *answer = dlg->mnu.items[dlg->nav.cursor].key;
               }
               else *answer = 0;*/
            //}
            *choice = nav->cursor;
            return true;
         break;
         case KEY_A:
            if ( has_instruction(nav, GM_INSTRUCTION_CANCEL))
            {  *choice = -1;
            }
            return true;
         break;
         case KEY_UP:
            if ( has_instruction(nav, GM_INSTRUCTION_UPDOWN))
            {  if ( nav->cursor > 0 ) nav->cursor--;
               if ( nav->page_size != 0 && nav->cursor < nav->page_offset )
               {  //nav->cursor = nav->page_offset;
                  nav->page_offset--;
               }
            }
            break;
         case KEY_DOWN:
            if ( has_instruction(nav, GM_INSTRUCTION_UPDOWN))
            {  if ( nav->cursor < ( nav->nb_item - 1) ) nav->cursor++;
               if ( nav->page_size != 0 && nav->cursor > nav->page_offset + nav->page_size -1)
               {  //nav->page_offset = nav->cursor;
                  nav->page_offset++;
               }
            }
            break;
         case KEY_SPACE:
            sprintf ( shotstr, "glymmer%03d.bmp", shotID);
            make_screen_shot ( shotstr );
            shotID++;
            break;
         case KEY_LEFT: //TODO: used to cycle query and YES/NO dialogs
            break;
         case KEY_PGUP:
            //printf ("DEBUG Received page down, instructions was %d\n", nav->instructions );
            if ( has_instruction(nav, GM_INSTRUCTION_PGUPDN))
            {  //int position = modulo ( nav->cursor, nav->page_size );
               nav->cursor -= nav->page_size;
               nav->page_offset -= nav->page_size;
               if ( nav->cursor < 0 )
               {  nav->cursor = 0;
                  nav->page_offset = 0;
               }
               //keep position within a page
               /*if ( position != modulo ( nav->cursor, nav->page_size ) )
               {  nav->cursor = nav->page_offset + position;
               }*/
               //limit page overflow
               if ( nav->page_offset < 0 ) nav->page_offset = 0;


            }
            break;
         case KEY_RIGHT:
            break;
         case KEY_PGDN:
            //printf ("DEBUG Received page down, instructions was %d\n", nav->instructions );
            if ( has_instruction(nav, GM_INSTRUCTION_PGUPDN))
            {  //int position = modulo ( nav->cursor, nav->page_size );
               nav->cursor += nav->page_size;
               nav->page_offset += nav->page_size;
               if ( nav->cursor >= nav->nb_item )
               {  nav->cursor = nav->nb_item-1;
                  nav->page_offset = nav->cursor - nav->page_size + 1;
               }
               //keep position within a page
               /*if ( position != modulo ( nav->cursor, nav->page_size ) )
               {  nav->cursor = nav->page_offset + position;
               }*/
               //limit page overflow
               int page_max = nav->nb_item - nav->page_size;
               if ( nav->page_offset > page_max  ) nav->page_offset = page_max;



               //TODO will have to convert lines as number of items for uniformity.
               /*else if ( has_component( dlg, GM_COMPONENT_TEXT ) )
               {  if ( dlg->nav.cursor >= dlg->txt.nb_lines ) dlg->nav.cursor -= dlg->nav.page_size;
               }*/
            }
            break;
      }
   }

   //printf ( "DEBUG Page size is %d, cursor is now %d, page=%d\n", nav->page_size, nav->cursor, nav->page_offset );

   return false;
}

/*! @brief Destroy navigation components
*/

void gm_navigation_destroy ( gms_navigation *nav )
{  //currently does nothing, just a place holder
   //NOTE: hide warning, does nothing
   nav+=0;
}


void gm_text_add ( gms_dialog *dlg, char *message, bool wrap, bool dynamic )
{  dlg->components = dlg->components | GM_COMPONENT_TEXT;
   dlg->txt.message = message;
   dlg->txt.wrap = wrap;
   dlg->txt.dynamic = dynamic;
   if ( wrap ) dlg->txt.nb_lines = gm_anticipate_printwrap_nb_lines(message, dlg->win.w);
   else dlg->txt.nb_lines = 1;
}

void gm_text_destroy ( gms_text *txt)
{  if ( txt->dynamic )
   {  free ( txt->message );
      //printf ("DEBUG: freeing message\n");
   }

}

void gm_list_static_add ( gms_dialog *dlg, gms_item *items, int length )
{  dlg->components = dlg->components | GM_COMPONENT_LIST;
   dlg->lst.length = length;
   dlg->lst.size = 0;
   dlg->lst.dynamic = false;
   dlg->lst.items = items;
}//TODO: Deduce length using NULL terminated array

void gm_list_dynamic_add ( gms_dialog *dlg)
{  dlg->components = dlg->components | GM_COMPONENT_LIST;
   dlg->lst.length = 0;
   dlg->lst.size = GM_DLIST_INIT_SIZE;
   dlg->lst.items = (gms_item*) malloc (sizeof(gms_item) * GM_DLIST_INIT_SIZE);
   dlg->lst.dynamic = true;
}




void gm_list_destroy ( gms_list *list)
{  if ( list->dynamic )
   {   for ( int i = 0 ; i < list->length; i++ )
      {  free(list->items[i].text);
         //printf ("DEBUG: freeing text string for dlist\n");
      }
      free(list->items);
      //printf ("DEBUG: freeing dlist\n");
   }


}






