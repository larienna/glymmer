/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 23rd, 2022
   @license: Apache 2.0 License

   Handle the text buffer and the drawing of text in the buffer
*/


#ifndef TEXTPRINT_H_INCLUDED
#define TEXTPRINT_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/*! List of glyphs, meaning of each character in the atlas. Glyphs = frame graphics, while font = letters
   The glyph set will contain 256 glyphs placed in a 16x6 glyph image atlas.
   This is how the size of the glyph can be deduced.

   The define belows are are relative offset, they will be used for the dialog
   in focus and out of focus.

   Glyph 0 is lost as it represents no glyph or a null value.
*/

#define GM_GLYPH_EMPTY              0x00
#define GM_GLYPH_CORNER_TOPLEFT     0x01
#define GM_GLYPH_CORNER_TOPRIGHT    0x02
#define GM_GLYPH_CORNER_BOTTOMRIGHT 0x03
#define GM_GLYPH_CORNER_BOTTOMLEFT  0x04
#define GM_GLYPH_EDGE_TOP           0x05
#define GM_GLYPH_EDGE_RIGHT         0x06
#define GM_GLYPH_EDGE_BOTTOM        0x07
#define GM_GLYPH_EDGE_LEFT          0x08
#define GM_GLYPH_TOP_START          0x09
#define GM_GLYPH_TOP_END            0x0A
#define GM_GLYPH_RIGHT_START        0x0B
#define GM_GLYPH_RIGHT_END          0x0C
#define GM_GLYPH_BOTTOM_START       0x0D
#define GM_GLYPH_BOTTOM_END         0x0E
#define GM_GLYPH_LEFT_START         0x0F
#define GM_GLYPH_LEFT_END           0x10
#define GM_GLYPH_HORIZONTAL         0x11
#define GM_GLYPH_VERTICAL           0x12
#define GM_GLYPH_BACKGROUND         0x13
#define GM_GLYPH_SELECTED           0x14
#define GM_GLYPH_TOP_T              0x15
#define GM_GLYPH_BOTTOM_T           0x16
#define GM_GLYPH_LEFT_T             0X17
#define GM_GLYPH_RIGHT_T            0X18
#define GM_GLYPH_VERTICAL_SPLIT     0X19
#define GM_GLYPH_HORIZONTAL_SPLIT   0X1A
#define GM_GLYPH_DROP_SHADOW        0X1B //currently not working and disabled.

//These are values to add to the codes above to select a different glyphset for innactive windows for example.
#define GM_GLYPH_OFFSET_ACTIVE      0x00
#define GM_GLYPH_OFFSET_INACTIVE    0x80

// Special characters in the foreground font in the 128-159 character range.
#define GM_SCHAR_ARROW_RIGHT        0x80
#define GM_SCHAR_ARROW_UP           0x81
#define GM_SCHAR_ARROW_LEFT         0x82
#define GM_SCHAR_ARROW_DOWN         0x83
#define GM_SCHAR_PAGEUP             0x84
#define GM_SCHAR_PAGEDOWN           0x85
#define GM_SCHAR_UPDOWN             0x86
#define GM_SCHAR_LEFTRIGHT          0x87
#define GM_SCHAR_PGUPDOWN           0x88
#define GM_SCHAR_SORT               0x89
#define GM_SCHAR_ACCEPT             0x8A
#define GM_SCHAR_CANCEL             0x8B
#define GM_SCHAR_CHECKED            0x8C
#define GM_SCHAR_UNCHECKED          0x8D
#define GM_SCHAR_RVBAR              0x8E // right vertical bar
#define GM_SCHAR_LVBAR              0x8F // left vertical bar
#define GM_SCHAR_LRBAR              0x90 // left right vertical bar
//#define GM_SCHAR_TOPSTUD            0x91
//#define GM_SCHAR_BOTTOMSTUD         0x92
#define GM_SCHAR_EMPTY              0x9F //Unused empty characters, used for errors so far
#define GM_SCHAR_HELP               0x3F //simple question mark





/** Windows can contains a series of instructions in their window frame to instruct the user
    which key are currently active in this dialog.
*/

#define GM_INSTRUCTION_ACCEPT    0b00000001
#define GM_INSTRUCTION_CANCEL    0b00000010
#define GM_INSTRUCTION_UPDOWN    0b00000100
#define GM_INSTRUCTION_LEFTRIGHT 0b00001000
#define GM_INSTRUCTION_PGUPDN    0b00010000
#define GM_INSTRUCTION_HELP      0b00100000
#define GM_INSTRUCTION_SORT      0b01000000
//#define WUI_INS_ESC         0b00100000
//#define WUI_INS_ENTER       0b01000000
//#define WUI_INS_SPACE       0b10000000 //seems to be used for screen shot, hidden and used by default
//TODO: Maybe a different LR for distributors, depends if list movement use LR or PGupdn

// Define special characters as constants. The charcode version is an alternate symbol in case
// the character codes are not interpreted from database data. I tried to use characters
// unlikely to be typed. Mostly accents so far. There is more char above 128, but they are hard to type.

#define GM_CODE_NEWLINE   '\n'
#define GM_CHARCODE_NEWLINE   '`'
#define GM_CODE_UNDERLINE  '\v'
#define GM_CHARCODE_UNDERLINE  '~'
#define GM_CODE_APPLICATION '\a'
#define GM_CHARCODE_APPLICATION '^'


/* Escape sequences in C
SOURCE: https://en.wikipedia.org/wiki/Escape_sequences_in_C

code  hex   purpose
\a 	07 	Alert (Beep, Bell) (added in C89)[1]
\b 	08 	Backspace
\e 	1B 	Escape character (Common non-standard code; see the Notes section below.)
\f 	0C 	Formfeed Page Break
\n 	0A 	Newline (Line Feed); see notes below
\r 	0D 	Carriage Return
\t 	09 	Horizontal Tab
\v 	0B 	Vertical Tab
\\ 	5C 	Backslash
\' 	27 	Apostrophe or single quotation mark
\" 	22 	Double quotation mark
\? 	3F 	Question mark (used to avoid trigraphs)
\nnn 	any 	The byte whose numerical value is given by nnn interpreted as an octal number
\xhh… 	any 	The byte whose numerical value is given by hh… interpreted as a hexadecimal number
\uhhhh 	none 	Unicode code point below 10000 hexadecimal (added in C99)[1]: 26 
\Uhhhhhhhh 	none 	Unicode code point where h is a hexadecimal digit
*/


void gm_clear_text_buffer ( gms_screen *e );


void gm_clear_text_window ( gms_screen *e, gms_window *win );
void gm_text_print      ( gms_screen *e, gms_window *win, unsigned char colorcode, const char *str);
void gm_text_printwrap  ( gms_screen *e, gms_window *win, unsigned char colorcode, const char *str, int line_offset );
void gm_text_printf ( gms_screen *e, gms_window *win, unsigned char colorcode, const char *format, ... );

int gm_anticipate_printwrap_nb_lines ( const char *str , int w);
int gm_count_carriage_return ( const char *str );
void gm_print_window_border (gms_screen *e, gms_window *win );
void gm_print_window_navigation (gms_screen *e, gms_window *win, gms_navigation *nav );
void gm_print_window_content_text ( gms_screen *scr, gms_dialog *dlg, bool is_active_dialog );
void gm_print_window_content_list ( gms_screen *scr, gms_dialog *dlg, bool is_active_dialog );





char gm_read_color_code ( char character );


#ifdef __cplusplus
}
#endif


#endif // TEXTPRINT_H_INCLUDED
