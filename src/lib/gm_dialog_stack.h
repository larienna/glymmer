/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 25th, 2022
   @license: Apache 2.0 License

   Allow storage and manipulation of a stack of dialogs.
*/


#ifndef DIALOG_STACK_H_INCLUDED
#define DIALOG_STACK_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

void gm_dialog_stack_init ( gms_dialog_stack *dstack, int size );
gms_dialog *gm_dialog_open ( gms_glymmer *engine );
gms_dialog *gm_dialog_get_active ( gms_glymmer *engine );
void gm_dialog_close ( gms_glymmer *engine, int nb );
void gm_dialog_close_all (gms_glymmer *engine);
void gm_dialog_stack_dispose ( gms_dialog_stack *dstack);

void gm_list_item_add (gms_glymmer *e, gms_dialog *dlg, int key, unsigned char mark, char *text);



#ifdef __cplusplus
}
#endif


#endif // DIALOG_STACK_H_INCLUDED
