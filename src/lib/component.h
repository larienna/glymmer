/**
   Glymmer

   @author: Eric Pietrocupo
   @date: January 17th, 2022
   @license: Apache 2.0 License

   Contains all methods to initialise and destroy components.


*/

#ifndef COMPONENT_H_INCLUDED
#define COMPONENT_H_INCLUDED


#define GM_COMPONENT_WINDOW     0b00000001  //somewhat essential to all dialog, so not really an option
#define GM_COMPONENT_LIST       0b00000010
#define GM_COMPONENT_TEXT       0b00000100
#define GM_COMPONENT_DATA       0b00001000
#define GM_COMPONENT_INPUT      0b00010000
#define GM_COMPONENT_NAVIGATION 0b00100000

#define GM_DLIST_INIT_SIZE   4  //! Initial size of a dynamic list


void gm_window_add ( gms_dialog *dlg, gms_screen *e, char *title, int x, int y, int w, int h);
void gm_window_destroy ( gms_window *win );
void gm_navigation_add ( gms_dialog *dlg, int nb_item, int page_size, int nb_page, unsigned int instructions );
//TODO: maybe move apply_input to the input module.
bool gm_navigation_apply_input ( gms_navigation *nav, int *choice, gms_input *input );
void gm_navigation_destroy ( gms_navigation *nav );
void gm_text_add ( gms_dialog *dlg, char *message, bool wrap, bool dynamic );
void gm_text_destroy ( gms_text *txt);
void gm_list_static_add ( gms_dialog *dlg, gms_item *items, int length );
void gm_list_dynamic_add ( gms_dialog *dlg);
void gm_list_destroy ( gms_list *list);

bool has_component ( gms_dialog *dlg, int component );


#endif // ECSWINDOW_H_INCLUDED
