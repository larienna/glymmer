# Library Usage




The gui is organised in a series of dialogs. Each dialog is
composed of something wrapped in a window frame. That something can be most
of the time a list of choice or just plain text

The goal of those list items is either to simply display information, or
ask the user to make a choice that can be used to perform additional
operation.

## Introduction

The existence of non-event based programming rely on the fact that video
games has little data entry. It's only a series of choice that the user
must make based on information displayed to them. So you'll never see long
input forms in a video game.

We can take advantage of this to build a GUI that will not require events.
This can lead to many benefits:

* No need to define listener
* No need to handle notifications
* No need for MVC structure
* No need to add many widgets to create a dialog
* No need for custom dialogs, you reuse the standard dialogss
* No need of object oriented programming, Entity Component System will be used instead.
* Reduced need of dynamic memory allocation. Most elements would be preallocated or expanded on demand.
* friendly for keyboard, mouse and joystick support.

The disadvantages could be:

* Players are most of the time on track. They generally make a choice using a menu, then answer a series of questions to perform the operation.
* The same window structure will be reused over and over again. Which could be boring after some time.
* The density of information in each dialog is lower, which could require multiple dialogs to perform an operation.
* Designed for video games, so little data to insert but lof of commands to execute.

## Basic principles

Look at the library's example for more details how to make the engine works. The general principle works this way

* Initialise the engine once at the start of the program. 

Then for each set of dialogs you want to display

* draw the background to be printed under the dialogs into a separate buffer
* create desired dialogs using factories
* display dialogs and wait for a user choice.
* close dialogs
   
----------------------

## Table of contents

@subpage dialog_stack
@subpage text_buffer
@subpage components
@subpage dialog_factories

-------------------------------
@page dialog_stack Dialog Stack

When the engine render his windows, he requires a stack of dialog that he will attempt to draw
on the screen. Multiple stacks can be defined but you will rarely need more than one. You need
to create a stack, put all your windows in them. Opening and closing of windows will be done
using that stack. Dialog factories will require a stack, open a window and add it to that stack for you. You must
still move that stack around with you where you need it.

Unless you want to design your own type of dialogs, most of this stack's methods will be used
by the glymmer engine especially the dialog factory.

The dialogs are stacked into a dynamically allocated array. It will be doubled in size if the
number of dialogs exeeds the limit. Although, it is recommended to allocate all the memory you
need right from the start. The reallocation is jsut a fail safe. Even when closed, the dialogs
are not deallocated, they return into the invalid pool of dialogs. The fields of the dialog
structure are zeroed when a new dialog is required.

To create a new dialog stack, instantiate an @ref gms_dialog_stack structure and initialise it
with method @ref gm_init_dialog_stack. When you have your stack you will at this point use
the dialog factories to create new dialog. But if for some reason, you need to create a dialog
youself, you can call @ref gm_dialog_open to initialise a new dialog and get a reference on that
dialog.

When you are finished, you can glose dialogs by calling @ref gm_dialog_close which will remove
X dialogs the top of the stack. @ref gm_dialog_close_all will actually close all the dialogs
currently in the stack.

---------------------------------------
@page text_buffer Text buffer

The display of the dialogs and it's content is done in 2 steps. First everything is printed
into a text buffer big enough to cover the entire screen in characters using the desired
font. All dialogs are printed into this text buffer and all over laps are done in that buffer.
The dialogs are printed from the bottom of the stack up to the top.

Then when ready, the second step is to draw each character in the text buffer into a
destination bitmap, which could be the screen or a double buffer and be printed on the screen
later. If anything should be be drawn under the stack of dialogs, first draw that background
content into the bitmap, then draw the Glymmer dialogs into the bitmap. If a character is 0
in the text buffer, no character will be drawn at that position, so there is no risk to
overwrite everything currently in the bitmap.

## Oooh! Colors

Every character also have a color code associated to it. This is the foreground color of the
character saved in a separate buffer. The number of colors available is limited to 10. When
printing text in the text buffer, colors can be encoded within strings with the '~' character
followed by the character of the color (ex: "~C" for color `C`). Colors can be inverted by using
small caps like "~c" for inverted color `C`. This will make the foreground color become the
background color and vice versa. The
background color will be set by the user and parametrised for each dialog. Allowing dialogs
of various colors but a single dialog has the same background color.

Colors available are only bright color to make it look good on darker background. It is recommended
to use dark backgrounds to avoid color clashes with the foreground color. You also have to
consider that color inversion with flip the colors for the dialog title and mouse overlay, so
the text must still be readable when both colors are inverted. There is no restriction to the
background color that can be used as no character encoding is available, but for the foreground,
these colors are available:

| Code | Name        |
| ---- | ----------- |
| A    | Light Gray  |
| B    | White       |
| C    | Gray        |
| D    | Yellow      |
| E    | Orange      |
| F    | Red         |
| G    | Purple      |
| H    | Blue        |
| I    | Cyan        |
| J    | Green       |


## Text printing

If you intend to make your own dialogs, there is a series of functions that are used to modify
the content of the text buffer according to the limits of the dialog's window (ex: @ref gm_text_print.
They will print
characters inside the frame of the window. The printing is done line by line, so each
function call will start a new line. If you want to concatenate stuff on the same line
use the @ref gm_text_printf function. The "\n" and "^" characters are used for carriage return, they will
skip additional lines, but they are replaced by regular spaces in the @ref gm_text_printwrap function.

The @ref gm_text_printwrap function is a special function that wrap text to the dialog's
frame to make sure words does not get cut. All other methods will cut any exceeding character
that would be drawn outside the window's frame. @ref gm_text_printwrap allows specifiying a
text offfet to prevent start printing characters from the first one. Useful when printing
multiple pages. @ref gm_clear_text_window will clear the color and the text according
the the dimensions of the window. While @ref gm_clear_text_buffer  clears the entire text buffer.

---------------------------------------------
@page components Components

Each dialog is composed of a series of components that could be present or not. Internally, each dialog will be
contained in a structure composed of multiple substructure. Not all of them could be used at the same time. 
A bitfield will be used to determine if a substructure is used or not since pointers are not used to reduce dynamic allocation.
Dialog factories functions will be used to define the components that will be present in a dialog. All those factory
methods reuses all the components listed below. It could be interesting to be familiar with those components.

## Window (WIN)
This component is essential and containts screen position in characters measure and will be used to print the border frame and background of the dialog.

## Navigation (NAV)
List  and scrolling text requires a form of navigation using the arrow keys. So information about cursor position, pages, etc needs to be recorded to be remembered between each user input.

## Text (TXT)
The text component can either be scrollable text or text printed before any choices.
The text could be printed with wrapped option, else only the first line is drawn.
Navigation components could be used to change the starting point, or change the page position of the text to display.

## List (LST)
If the dialogs is composed of a series of static choices, then list will have to be defined. They are used by list, questions, etc. The list of items will have to be
defined statically in the code using a null terminated list, or the size of the list can be expanded dynamically. The length of the items will be fixed to the width of the screen in characters. Long list can use use multipage display. Dynamic list are used when you do not know the amount of items at runtime (ex: a list of files in a directory).

## Database (DAT)
The data component is a very important component that queries from the database to print the list of choices. There is 2 type of information pull from
the DB: Multiple records, where each record is printed on a line. Single records where
only the first record is printed on a line. A format (a la printf) needs to be supplied, instead of a variadic list of variables, SQL fields will be used instead.
If the output is larger than the window, it will simply be cropped. So the text buffer will act as a targer buffer space.

---------------------------------------------
@page dialog_factories Dialog Factories


The dialogs are actually functions that initialise the data structure in different
ways. The glyummer engine will make them behave according to the set parameters.
It is possible to create your own dialog factories within the features of the glymmer
using @ref gm_dialog_open and using components _add() methods to initialise and add the desired components to the dialogs.
Still, a good amount of factories are already available for you to save simplify the process.
The factory routines should be used during the game for building the user interface.
All dialog functions return a pointer on the dialog but it is rarely needed, 
as the dialog is already integrated to the internal stack. Do not attempt to free this structure since internally, it's a pooled dynamic array.
Call instead @ref gm_dialog_close to close the last dialog on the stack or @gm_dialog_close_all to close the entire stack.

Once all the dialogs are created, you call @ref gm_dialog_show() and wait for your answer. Only the top most dialog in the stack will query user input and 
interact according to use choices. All other dialogs under the top dialog will simply be drawn on the screen. This top most dialog is known as the
active dialog. Instructions should appear at the bottom right corner of the dialog. Those instrucions determines which keys are available.

## Factories overview

Here is the list of factories that has various purposes. The following factories open dialogs with no content

@ref gm_dialog_open_empty
@ref gm_dialog_open_background

Those dialogs will display only text in various manner using a text string. The text can be wrapped, paged, etc.The message dialog had predermined size and screen position.

@ref gm_dialog_open_text
@ref gm_dialog_open_message
@ref gm_dialog_open_pagedtext

The following dialogs includes text features but also display a list of choices that the user can select from. A NULL terminated list of items would be required this allow calculating the size of the list. The content of a list can be static in the program or dynamic (ex: a list of files in a folder). Content could be paged else, the list size would determine the size of the dialog. The question dialog has predetermined size, position and list.

@ref gm_dialog_open_list
@ref gm_dialog_open_pagedlist
@ref gm_dialog_open_question

Those are other specialised dialogs

@ref gm_dialog_open_question_lr 

There are also database dialogs which required using the result of a select query to build up text or list dialogs.

The information will have to be requeried after each cursor movement since to
simplify buffer management, the text buffer will be used. That prevents using
dynamic memory allocation to store DB records. Those methods requires a format (a la printf) and a query. All records will
be printed 1 by line for multi line dialogs. Else 1 field per line will be used for single record display. Any text that exceed the window's frame will not be 	printed. Define the field's "AS" value since they will be used in the header of the list or in field names.
Make sure the number of fields match the nb of variables in the printf format, else it will crash.

TODO




