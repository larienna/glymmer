/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 22th, 2022
   @license: Apache 2.0 License
*/

#include <stdio.h>
#include <stdbool.h>
#include <allegro.h>
#include <gm_struct.h>
#include <component.h>
#include <gm_engine.h>
#include <gm_input.h>
#include <gm_dialog_stack.h>
#include <textprint.h>
#include <draw.h>
#include <gm_common.h>

// _____ private methods _____


/*! @brief Convert use choice as list answers
*/
//TODO not sure if should convert cursor to answer only. Some choices like multi-select could
// make the dialog go on
int gm_validate_dialog_answer ( gms_dialog *dlg, int choice )
{  if ( choice == -1) return -1;

   if ( has_component( dlg, GM_COMPONENT_LIST))
   {  return dlg->lst.items[dlg->nav.cursor].key;
   }

   return 0; // defaut answer for non-cancel choices
}

//_____ Methods _____

/*! @brief Change the internal array of color from a color scheme structure
*/
void gm_change_color_scheme ( gms_glymmer *engine, gms_color_scheme *color_s )
{  engine->glyph.colors [GM_COLOR_BLACK] = makecol ( 0, 0, 0 );
   engine->glyph.colors [GM_COLOR_WHITE] = makecol ( 255, 255, 255 );
   engine->glyph.colors [GM_COLOR_TEXT] =
      makecol ( color_s->text.r, color_s->text.g, color_s->text.b );
   engine->glyph.colors [GM_COLOR_BORDER] =
      makecol ( color_s->border.r, color_s->border.g, color_s->border.b );
   engine->glyph.colors [GM_COLOR_UNDERLINE] =
      makecol ( color_s->underline.r, color_s->underline.g, color_s->underline.b );
   engine->glyph.colors [GM_COLOR_CURSOR] =
      makecol ( color_s->cursor.r, color_s->cursor.g, color_s->cursor.b );
   engine->glyph.colors [GM_COLOR_DISABLED] =
      makecol ( color_s->disabled.r, color_s->disabled.g, color_s->disabled.b );
   engine->glyph.colors [GM_COLOR_MESSAGE] =
      makecol ( color_s->message.r, color_s->message.g, color_s->message.b );
   engine->glyph.colors [GM_COLOR_SIDETEXT] =
      makecol ( color_s->sidetext.r, color_s->sidetext.g, color_s->sidetext.b );

}



/*! @brief Initialise the glymmer engine
   @param display_font[in] Font to be used by the engine, it MUST be a monospaced font.
   @param input_peripherals[in] Bitfield containing all the input devices to read.
*/
//TODO: REconsider if should use loaded font and bitmap, or only supply a string to the filename.
//The main issue is how the file is loaded: Simple file or archive. The advantage is to make it lib independent
//freeing the bitmap will now become the responsability of glymmer which is better.
void gm_initialise ( gms_glymmer *engine, FONT *foreground_font,
                     BITMAP* background_glyph,
                     BITMAP* icon_glyph,
                    unsigned char input_peripherals,
                    gms_color_scheme *color_s,
                    int screen_width, int screen_height )
{  assert_or_exit( foreground_font != NULL, 1, "Text font is NULL"); //TODO Error codes to define
   engine->glyph.text = foreground_font;
   engine->glyph.h = text_height ( foreground_font);
   engine->glyph.w = text_length( foreground_font, "W");
   engine->screen.text_w = screen_width / engine->glyph.w;
   engine->screen.text_h = screen_height / engine->glyph.h;
      /*printf ("DEBUG: SCREEN_W=%d, SCREEN_W=%d, font.w=%d, font.h=%d\n", SCREEN_W, SCREEN_H,
           engine->font.w, engine->font.h );*/

   //Validate that the glyph has the same size
   //assert_or_exit( background_glyph != NULL, 1, "Background Glyph bitmap is NULL");
   if ( background_glyph == NULL ) //use empty glyph set, use 3 rows for now, useful for unit tests.
   {  engine->glyph.bg = create_bitmap( engine->glyph.w*16, engine->glyph.h*3);
      clear_bitmap(engine->glyph.bg);
   }  else engine->glyph.bg = background_glyph;
  // assert_or_exit( engine->glyph.bg->h / 16 == engine->glyph.h,1,"Glyph bitmap does not match font height");
   /*printf ("DEBUG: atlas H=%d, glyph H =%d\nA / B = %d * 16 = %d\n",
       engine->glyph.bg->h,
       engine->glyph.h,
       (engine->glyph.bg->h / engine->glyph.h),
       (engine->glyph.bg->h / engine->glyph.h) * 16);*/
   engine->glyph.nb_bg_glyph = (engine->glyph.bg->h / engine->glyph.h) * 16;
   assert_or_exit( engine->glyph.bg->w / 16 == engine->glyph.w,1,"Glyph bitmap does not match font width");
   assert_or_exit( engine->glyph.nb_bg_glyph != 0, 1, "Number of background glyphs is 0");

   engine->glyph.icon = icon_glyph; //if set to NULL, there is simply no icon glyphs
   engine->glyph.nb_icon_glyph = 1; //default value to avoid division by 0
   if ( icon_glyph != NULL )
   {  assert_or_exit( engine->glyph.bg->w / 16 == engine->glyph.w,1,"Glyph bitmap does not match font width");
      engine->glyph.nb_icon_glyph = (engine->glyph.icon->h / engine->glyph.h) * 16;
      assert_or_exit( engine->glyph.nb_icon_glyph != 0, 1, "Number of icon glyphs is 0");
   }


   //printf ("DEBUG: nb_glyph bg=%d, icon=%d\n", engine->glyph.nb_bg_glyph, engine->glyph.nb_icon_glyph);
  // assert_or_exit( engine->glyph.bg->h / 16 == engine->glyph.h,1,"Glyph bitmap does not match font height");


   //NOTE: there is a division by 2 to split the remaining pixels on each side of the screen.
   engine->screen.pixel_offset_x = (screen_width - (engine->screen.text_w * engine->glyph.w)) / 2;
   engine->screen.pixel_offset_y = (screen_height - (engine->screen.text_h * engine->glyph.h)) / 2;

   engine->screen.text_buffer = (gms_char*)malloc ( sizeof (gms_char)
               * ( engine->screen.text_h * engine->screen.text_w + 1));
   gm_clear_text_buffer( &engine->screen );

   gm_change_color_scheme( engine, color_s);

   gm_dialog_stack_init( &engine->dstack, 1); // use minimum stack size as default

   engine->input_peripherals = input_peripherals;
   engine->screen.db_buffer = create_bitmap ( screen_width, screen_height );
   engine->screen.gm_buffer = create_bitmap ( screen_width, screen_height );
}

/*! @brief draw the entire dialog stack on the screen
   It does not handle any of the user input, it just draws all dialog with the info it has.
*/
void gm_draw_dialog_stack ( BITMAP *bmp, gms_glymmer *engine )
{  static int transparent = -1;
   if ( transparent == -1) transparent = makecol (255, 0, 255);

   gm_clear_screen_bitmap( bmp, transparent);


   //DEBUG: test print characters
   /*int index = 0;
   for ( int i = 0 ; i < 10 ; i++ )
   {  for ( char j = 'A'; j <='z'; j++)
      {  _engine.text_buffer [index].character = j;
         _engine.text_buffer [index].color = 0;
         index++;
      }
   }*/

   //testing windows printing methods
   /*gms_dialog *dlg = gm_dialog_open( dstack, GM_COMPONENT_WINDOW );
   gm_window_init( &_engine, &dlg->win, NULL, 0, 0, _engine.text_screen_w , _engine.text_screen_h);
   gm_clear_text_window(&_engine, &dlg->win );
   gm_print_window_border(&_engine, &dlg->win, NULL);

   dlg = gm_dialog_open ( dstack, GM_COMPONENT_WINDOW );
   gm_window_init( &_engine, &dlg->win, "Bonjour", 5, 5, 20, 10);

   gm_clear_text_window(&_engine, &dlg->win );
   gm_print_window_border(&_engine, &dlg->win, NULL);
   gm_text_print( &_engine, &dlg->win, "Bonjour comment ca va");
   gm_text_printf( &_engine, &dlg->win, "%d: %s: voici une commande qui devrait deborder", 128, "printf" );
   gm_text_printwrap( &_engine, &dlg->win, "Voici un printwrap qui devrait deborder et faire un retour a la ligne", 0 );
*/


   //printf ("Number of dialogs = %d \n", engine->dstack.nb_dialog );
   for ( int i = 0 ; i < engine->dstack.nb_dialog; i++ )
   {  //printf ("_____ Printing dialog %d _____\n", i );
      gms_dialog *dlg = &engine->dstack.dlg[i];
      //printf ("DEBUG 0\n");
      bool is_active_dialog = (i == engine->dstack.nb_dialog - 1);
      //printf ("DEBUG 1\n");
      gm_clear_text_window(&engine->screen, &dlg->win );
      //printf ("DEBUG 2\n");

      gm_print_window_border(&engine->screen, &dlg->win);
      //printf ("DEBUG 3\n");

      if ( is_active_dialog &&
          has_component( dlg, GM_COMPONENT_NAVIGATION ) )
      {  gm_print_window_navigation( &engine->screen, &dlg->win, &dlg->nav);
         //printf ("DEBUG draw_dialog_stack() instructions was %d\n", dlg->nav.instructions );
      }
      //printf ("DEBUG 4\n");

      if ( has_component ( dlg, GM_COMPONENT_TEXT) )
      {  gm_print_window_content_text( &engine->screen, dlg, is_active_dialog );
      }
      //printf ("DEBUG 5\n");

      if ( has_component ( dlg, GM_COMPONENT_LIST | GM_COMPONENT_NAVIGATION) )
      {  gm_print_window_content_list( &engine->screen, dlg, is_active_dialog );
      }
      //printf ("DEBUG 6\n");


   }

   gm_blit_text_buffer( engine, bmp );


   /*//imported code


   for ( int i = 0 ; i <= wui_active_dialog; i++)
   {  if ( dialogs[i].win.hidden == false )
      {
         window_text_clear ( &dialogs[i].win );

            dialog_print_content ( &dialogs[i] );

         window_draw_text ( &dialogs[i].win, color );
      }

   }
   copy_buffer();*/

   //printf ("_____  After blitting text buffer _____\n" );

}

/*! @brief Draws all dialogs, read user input and return the choice
   This is one of the most used function that is called to get the answer of the user.
   It draws the stack of dialog until the player made a choice. It updates the dialogs
   according to user navigation and returns the choice made by the user.
   This is why this function will play the role of mainloop for the glymmer UI.
   This method use double buffering, so the bmp is a copy of the background. An internal
   double buffer in the engine will be used.

   @param bmp[in] bitmap to display in the background. Will remain the same until the user makes a choice.
   @param engine[in] Information about the state of the engine (includes the dialog stack)
   @return the answer made by the user. -1 is used when cancelling.
*/

int gm_show_dialogs ( BITMAP *bg, gms_glymmer *engine )
{
   int choice;
   gms_input input;
   gms_dialog *active_dlg;
   //printf ("before gm_reset_input\n" );
   gm_reset_input( &input, engine->input_peripherals);

   do
   {
      //printf ("before first blit\n" );
      blit(bg, engine->screen.db_buffer, 0, 0, 0, 0, bg->w, bg->h);
      //printf ("before gm_draw_dialog_stack\n" );
      gm_draw_dialog_stack ( engine->screen.gm_buffer, engine );
      //printf ("before second blit\n" );
      masked_blit(engine->screen.gm_buffer, engine->screen.db_buffer, 0, 0, 0, 0,
                    engine->screen.gm_buffer->w, engine->screen.gm_buffer->h);
      //printf ("before third blit\n" );
      blit(engine->screen.db_buffer, screen, 0, 0, 0, 0,
           engine->screen.db_buffer->w, engine->screen.db_buffer->h);

      //printf ("before keypress\n" );
      gm_read_input( &input );

      active_dlg = gm_dialog_get_active( engine );
      //printf ("DEBUG before apply_input() instructions was %d\n", active_dlg->nav.instructions );
   }
   while ( !gm_navigation_apply_input(&active_dlg->nav, &choice, &input) );
   int answer = gm_validate_dialog_answer ( gm_dialog_get_active( engine ), choice);

   return answer;


}

/*! @brief Dispose the resources of the glymmer engine
*/

void gm_dispose ( gms_glymmer *engine )
{  //printf ("_____  Before free engine _____\n" );
   free (engine->screen.text_buffer);
   gm_dialog_close_all( engine);
   gm_dialog_stack_dispose(&engine->dstack);
   destroy_bitmap ( engine->screen.db_buffer);
   destroy_bitmap ( engine->screen.gm_buffer);
   //printf ("_____  After free engine _____\n" );


}
