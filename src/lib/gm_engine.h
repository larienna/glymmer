/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 22th, 2022
   @license: Apache 2.0 License

   Top level module that manage the dialog stack and launch the multiple systems of the ECS.
*/

#ifndef ENGINE_H_INCLUDED
#define ENGINE_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

/*! Constants for quick access to the color in the array,
   use negative that value for inverted colors.
*/


#define GM_COLOR_BLACK  0
#define GM_COLOR_WHITE  1
#define GM_COLOR_TEXT   2
#define GM_COLOR_BORDER 3
#define GM_COLOR_UNDERLINE 4
#define GM_COLOR_CURSOR 5
#define GM_COLOR_DISABLED 6
#define GM_COLOR_MESSAGE 7
#define GM_COLOR_SIDETEXT 8   //unfocused dialogs
#define GM_COLOR_UNDEFINED 9
//do not forget modifying GM_NB_TEXT_COLOR  in gm_struct.h



void gm_initialise ( gms_glymmer *engine, FONT *foreground_font,
                     BITMAP* background_glyph,
                     BITMAP* icon_glyph,
                     unsigned char input_peripherals,
                     gms_color_scheme *color_s,
                    int screen_width, int screen_height );

void gm_draw_dialog_stack ( BITMAP *bmp, gms_glymmer *engine );

int gm_show_dialogs ( BITMAP *bmp, gms_glymmer *engine );

void gm_dispose ( gms_glymmer *engine );



#ifdef __cplusplus
}
#endif

#endif // ENGINE_H_INCLUDED
