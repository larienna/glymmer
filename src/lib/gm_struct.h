/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 24th, 2022
   @license: Apache 2.0 License

   Contains all the structure and types used by the library for easier dependency management.
*/


#ifndef STRUCT_H_INCLUDED
#define STRUCT_H_INCLUDED


#ifdef __cplusplus
extern "C"
{
#endif



//__________ Components __________

/*! @brief Hold the window frame information
   The following structure contains all the following information to handle
   a single window, The positions are recorded as characters.

   Windows are anchored to the edge of the screen, by default it's the top and left
   edges. If negative value x, y values are set, the window will be anchored to the
   right or bottom edge of the screen.

   An internal cursor is stored to allow printing text with multiple commands and remember
   the line after the last line printed so that each window act as a mini console.
*/

typedef struct gms_window
{  int w;              //!< the width in characters of the window
   int h;              //!< the height in characters of the window
   int x;              //!< the x relative position in characters
   int y;              //!< the y relative position in characters
   char *title;        //!< title to draw in the center border. Set to null if no title needed
   //NOTE: If windows loaded from a file, title will have to be dynamically allocated
   //A fixed max size according to screen size can be allocated.
   int line_cursor;    //!< current line position of the cursor, each print operation starts on a new line
   //TODO: Optional border display. Allow making background window with data without any window borders.
   // indirectly allow possitionning different windows for background data without realizing they are
   // separate windows. Try in stock ticker.

}gms_window;

typedef struct gms_navigation
{  int cursor; //!<  Position of the cursor in the list or in the scollable text. Lists have precedence over pagable text.
   int nb_item; //!<  Total number of items in the list
   int nb_page; //!<  precalculated number of pages that will need to be displayed
   int page_size; //!<  number of items to print on the screen at the same time. Must be at least < than window height -2
   int page_offset; //!< Current page start position. Used for fluid list movement.
   unsigned int instructions;  //!<  bitfield list of instruction keys to draw in the frame and to interpret
}gms_navigation;

/*! @brief Choice that can be present in lists and menus
   This is a simple choice for static list of items and menus. They are most of the time defined
   in code, but they could be loaded from a file. The content cannot be dyamic as the text
   field should be pointing on constants. For dynamic content, it is recommended to use the
   database component instead.
*/

typedef struct gms_item
{  int key; //!<  number returned by the choice. -1 is used for Cancel (including cancel button)
   unsigned char mark; //!<  Bitfield to mark certain information for this choice
   //TODO: could use booleans unless required in database to have all marks in 1 field.
   char *text; //!<  Text to be written for this item. Set to NULL to mark last element in list.
}gms_item;



/*! @brief Manage static or dynamic list of items that can be selected
   Components this component is mutually exlusive with data components which gets it's content
   from a database. But the behavior is almost the same. List can be dynamically allocated for
   when the size is unknown. You will need to use item adding routines.
*/
typedef struct gms_list
{  int length; //!<  Number of items in the list
   int size; //!<  Number of allocated items used in dynamic lists
   bool dynamic; //!<  Determines if the list is dynamic or static. dynamic list must be de/allocated
   gms_item *items; //!<  Array of items to be displayed in the list.
}gms_list;

/*! @brief Scrollable text or text printed before any choices.
   The text could be printed with wrapped option, else only the first line is drawn.
   Navigation components could be used to change the starting point, or change the page
   position of the text to display.
*/

typedef struct gms_text
{  char *message; //!< Message that must be printed before the choices as printwrap. For example questions.
   bool wrap; //!< set to true if text must be printed with wrap option on more than 1 line.
   int nb_lines; //!< this is the number of lines that should be required if wrapping enabled.
   bool dynamic; //!< Indicates that the message string is dynamically allocated and must be freed.
}gms_text;


#ifdef GM_ADD_DATABASE_SUPPORT

/*! @brief Data component for database support
*/
typedef struct gms_data
{  char *format; //!< printf format to print on the screen, should contain constants
   char *query; //!< SQL query to use, should be constant, use bound parameters instead of dynamic allocated string
   bool multirecord; //!< set to true if more than 1 record must be displayed
}gms_data;
#endif

/*! @brief Dialog containing all the components of a window.
*/

typedef struct gms_dialog
{  unsigned int components; //!< bitfield containing the components that has been set.
   gms_window win;
   gms_navigation nav;
   gms_list lst;
   gms_text txt;
   //s_wui_data dat;
   //dat,inp,numeric(distributor)
   //TODO: Maybe union lst and dat since mutually exclusive. else group in same struct with union
}gms_dialog;

//_____ Other structures _____

/*! @brief Single character of the text buffer

   Each character in the contains a character and a color to be displayed. A negative color
   means the background and foreground colors will be inverted. Valid colors range from -9 to 9.
*/



typedef struct gms_char
{  unsigned char bg; //!< Background glyph to display.
   unsigned char bg_previous; //!<  previous glyph for overlapping dialogs correctly
   unsigned char letter; //!< Text to be displayed on the screen
   unsigned char icon; //!< only used if the icon glyph set is loaded. Conmume memory, but skip processing.
   //int bg_color; //!<  Background color to display behind the text (NOT SURE if will be used)
   unsigned char color; //!<  Color INDEX to be used to display by the text
   bool underlined; //!<  Set to true if the character will be underlined.
}gms_char;

#define GM_NB_TEXT_COLOR 10
typedef struct gms_glyph
{  BITMAP* bg; //!<  Bitmap glyph used for dialog background and edges
   BITMAP* icon; //!<  Bitmap glyph used for game icons
   FONT *text; //!<  Font to be used for the whole engine.
   int h;  //!<  desired height of the font and glyphs
   int w; //!<  desire width of the font and glyphs
   int colors[GM_NB_TEXT_COLOR]; //!< calculated color table build from color scheme.
   unsigned short nb_bg_glyph; //!<  Set a limit on the number of background glyph if smaller than 256
   unsigned short nb_icon_glyph; //!<  Set a limit on the number of background glyph if smaller than 256
}gms_glyph;
//NOTE: If more gylph or attribute gets added, nb glyph could be it's own structure. h and w are still shared.
//NOTE: Used unsigned short for nb of glyph to avoid bug that it cannot hold value 256. Necessary for modulo operation

typedef struct gms_screen
{  int pixel_offset_x;     //!<  Adjustments that must be made if the font size does not accurately divides the screen
   int pixel_offset_y;     //!<  Adjustments that must be made if the font size does not accurately divides the screen
   gms_char *text_buffer;  //!<  text buffer of characters used print all windows content before blitting.
   int text_w;             //!<  Calculated nb of characters for screen width
   int text_h;             //!<  Calculated nb of characters for screen height
   BITMAP *db_buffer;      //!<  Internal double buffer used before blitting on screen
   BITMAP *gm_buffer;      //!<  Glymmer buffer where the dialogs are drawn
}gms_screen;

/*! @brief A dynamic array of dialogs works like a stack.

   The top most dialog is the active dialog. Dialogs below will still be drawn in reverse order,
   but they cannot receive input from the user. The stack is preallocated, you should allocate the
   amount you intend to use for the entire program. If there is an overflow, the size of the stack
   will be doubled.
*/

typedef struct gms_dialog_stack
{  int nb_dialog; //!<  nb of valid open dialogs, also the index of the last open dialog that will receive input.
   int size;     //!<  Current size of the stack for the allocation of memory.
   gms_dialog *dlg; //!<  Pointer on the dialog stack.
}gms_dialog_stack;

/*! @brief Concrete user input of various peripheral
   There is no abtraction of the input, only a copy of the various data given by the peripherals.
   The programmer can supply his own input system and put the information here.
*/

typedef struct gms_input
{  unsigned char peripherals; //!<  Bitfield Indicating which peripherals to read
   unsigned char modified;    //!<  Indicates as a bit field which input had some activity.
   unsigned char kb_scancode; //!<  Scan code of the key pressed by the keyboard
   unsigned char kb_ascii;    //!<  Ascii code of the key pressed by the keyboard
}gms_input;

/*! @brief Red Green Blue components
   Contains the seperated color components to build the color later.
*/

typedef struct gms_rgb
{  unsigned char r;
   unsigned char g;
   unsigned char b;
}gms_rgb;


/*! @brief Color scheme structure
   Contains the color information regarding how to color the fonts and the background of the fonts.
   It allows having a color schema that matches more precisely the theme of the background glyphs.
   It would also allow the user to customise eventually their color scheme to their preferences if
   the preset values are not desired.

   Internally, the resulting colors will be stored as an array to save space in the text buffer
*/

typedef struct gms_color_scheme
{  gms_rgb text; //!<  Color of the regular text
   gms_rgb border; //!<  Color of the title border and other characters in border
   //gms_rgb emphasis_bg; //!<  Background color for emphasis text
   gms_rgb underline; //!<  Color for words put in emphasis. Could include underline to prevent color coding information.
   gms_rgb cursor; //!<  Colors to be used by navigation elements inside the dialog.
   gms_rgb disabled; //!<  Colors to be used when a choice in the menu is disabled.
   gms_rgb message; //!<  Colors the message at the top of the dialog.
   gms_rgb sidetext; //!<  Colors of non active dialogs
}gms_color_scheme;

/*! @brief Game engine data structure
   This is the one structure to rule them all. It contains every thing the engine could require.
   Create a local instance of this struct, initialise it and dispose it with the appropriate
   methods. It will be required by all dialog factory methods and for drawing the dialog stack.
   By putting everything at the same place, it reduce the number of objects the programmer needs
   to handle.
*/



typedef struct gms_glymmer
{  gms_glyph glyph;
   gms_screen screen;
   gms_dialog_stack dstack;
   unsigned char input_peripherals; //!<  determined which input to read and interpret
}gms_glymmer;


/* Loose variables that could go into another components later

   bool reprint;        //set to true if the window must be redrawn due to initial print or update.
   bool hidden;         //if set to hidden, will not be draw on the screen and will not be updated.
                        //with cursor movement.
*/
#ifdef __cplusplus
}
#endif

#endif // STRUCT_H_INCLUDED
