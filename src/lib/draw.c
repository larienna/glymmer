/**
   Glymmer

   @author: Eric Pietrocupo
   @date: January 16th, 2022
   @license: Apache 2.0 License
*/

#include <allegro.h>
#include <stdbool.h>
#include <gm_struct.h>
#include <gm_engine.h>
#include <stdio.h>




/*! @brief Draws a specific glyph from a bitmap on the target buffer
    @param x position in pixels
    @param y position in pixels
*/

void glyph_blit ( BITMAP *bmp, BITMAP *glyph, int x, int y, int w, int h, char glyph_id, short nb_glyph )
{  //Convert 1D to 2D
   //printf ("DEBUG: Before modulo, glyph_id = %d, nb_glyph = %d\n", glyph_id, nb_glyph);
   glyph_id = glyph_id % nb_glyph; //Constrain within the valid range of glyph (watch out for DIV 0)
   int src_x = glyph_id % 16 * w;
   int src_y = glyph_id / 16 * h;

   masked_blit(glyph, bmp, src_x, src_y, x, y, w, h);
   //NOTE: Masked blit seems to do the same thing, so both applies transparency.
}

//void blit(BITMAP *source, BITMAP *dest, int source_x, int source_y, int dest_x, int dest_y, int width, int height);

//textprintf_ex( bmp, e->fg_font.glyph, x, y, fgcolor, bgcolor, "%c", e->screen.text_buffer [index].letter);


/*! @brief Clear BITMAP to a solid color according to the screen size
*/
void gm_clear_screen_bitmap ( BITMAP *bmp, int color )
{  drawing_mode ( DRAW_MODE_SOLID, NULL, 0, 0 );
   rectfill ( bmp, 0, 0, SCREEN_W, SCREEN_H, color );
}

/*! @brief Blit the text buffer into a BITMAP buffer

*/
void gm_blit_text_buffer ( gms_glymmer *e, BITMAP *bmp )
{  int x,y;
   //int i, j
   int index;
   const  int buffer_end = e->screen.text_h * e->screen.text_w;

   for ( index = 0 ; index < buffer_end; index++)
   {  // convert a 1D buffer as 2D coordinates
      x = (index % e->screen.text_w) * e->glyph.w + e->screen.pixel_offset_x;
      y = (index / e->screen.text_w) * e->glyph.h + e->screen.pixel_offset_y;


      //old code to support inverted colors
      //int fgcolor = makecol ( 225, 225, 225 );
      //int fgcolor = e->glyph.colors [ ABS ( e->screen.text_buffer [index].color) ];
      //int bgcolor = makecol ( 0, 0, 0);


      //swap colors if negative color.
      /*if ( e->screen.text_buffer [index].color < 0)
      {  int swap = fgcolor;
         fgcolor = bgcolor;
         bgcolor = swap;
      }*/

      //rectfill ( bmp, x, y, x+10, y+15, makecol( 0, 0, 255 )); //for debugging purpose
      /*if ( index == 67 )
      {   printf( "DEBUG draw bottom=%d, top=%d\n",
            e->screen.text_buffer [index].bg_glyph_previous,
            e->screen.text_buffer [index].bg_glyph);
      }*/

      int fgcolor = e->glyph.colors [ e->screen.text_buffer [index].color ];
      int bgcolor = -1; // draw transparently since now using bitmap glyphs


      if ( e->screen.text_buffer [index].bg != e->screen.text_buffer [index].bg_previous)
      {  glyph_blit( bmp, e->glyph.bg, x, y, e->glyph.w, e->glyph.h, e->screen.text_buffer [index].bg_previous,
            e->glyph.nb_bg_glyph);
      }
      glyph_blit( bmp, e->glyph.bg, x, y, e->glyph.w, e->glyph.h, e->screen.text_buffer [index].bg, e->glyph.nb_bg_glyph );

      if (e->glyph.icon != NULL && e->screen.text_buffer [index].icon > 0)
      {  //printf ("DEBUG: Icon printing is getting called\n");
         glyph_blit( bmp, e->glyph.icon, x, y, e->glyph.w, e->glyph.h, e->screen.text_buffer [index].icon, e->glyph.nb_icon_glyph );

      }

      if ( e->screen.text_buffer [index].underlined )
      {  /*//Underline using characters
         textprintf_ex( bmp, e->glyph.text, x, y, fgcolor, bgcolor,
                     "%c", '_');*/
         hline (bmp,x,y + e->glyph.h-1, x + e->glyph.w, fgcolor);
      }
      textprintf_ex( bmp, e->glyph.text, x, y, fgcolor, bgcolor,
                     "%c", e->screen.text_buffer [index].letter);
      //printf ("%c", text_buffer [index].character);
      //if ( index % 53 == 0 ) printf ("\n");

   }

}


