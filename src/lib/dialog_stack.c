/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 25th, 2022
   @license: Apache 2.0 License
*/

#include <stdbool.h>
#include <stdio.h>
#include <allegro.h>
#include <gm_struct.h>
#include <component.h>

//_____ private methods _____

void gm_dialog_clear ( gms_dialog *dlg )
{
   memset ( dlg, 0, sizeof ( gms_dialog ) );
}

/*! @brief Internal method that deallocate the dynamic content in the dialog
*/

void gm_dialog_dispose ( gms_dialog *dlg )
{  if ( has_component( dlg, GM_COMPONENT_WINDOW ) ) gm_window_destroy( &dlg->win );
   if ( has_component( dlg, GM_COMPONENT_LIST ) ) gm_list_destroy( &dlg->lst );
   if ( has_component( dlg, GM_COMPONENT_TEXT ) ) gm_text_destroy( &dlg->txt );
   if ( has_component( dlg, GM_COMPONENT_NAVIGATION ) ) gm_navigation_destroy( &dlg->nav );

}

//_____ Public methods _____

void gm_dialog_stack_init ( gms_dialog_stack *dstack, int size )
{  dstack->nb_dialog = 0;
   dstack->size = size;
   dstack->dlg = (gms_dialog*) malloc (sizeof(gms_dialog) * size );
   for ( int i = 0 ; i < size ; i++ ) gm_dialog_clear ( &dstack->dlg[i] );
}


/*! @brief Move the stack pointer to open a new dialog

   On opening of a dialog, you need to setup the required components which will marked as used.
   This method will realloc the buffer if it's too small by doubling it. This has the consequence
   that the returned pointers could eventually become invalid after reallocation.

   @param[in] dstack Dialog stack where to open the dialog
   @param[in] components List of GM_COMPONENT_? constant grouped with bitwise OR
   @return    A pointer on the dialog which should be at the top of the stack.
              Else a NULL if the window could not be opened.
*/

gms_dialog *gm_dialog_open ( gms_glymmer *engine )
{  gms_dialog_stack* dstack = &engine->dstack;
   dstack->nb_dialog++;
   //If the new dialog index exceed the size, reallocate the ram
   if ( dstack->nb_dialog >= dstack->size)
   {  int tmpsize = (dstack->size *2 );
      /*printf ("DEBUG: new size=%d, stack=%d, sizeof=%d address=%p, \n",
              tmpsize * sizeof (gms_dialog), dstack->size, sizeof(gms_dialog), dstack->dlg);*/
      gms_dialog *tmptr = realloc ( dstack->dlg, tmpsize * sizeof (gms_dialog));
      if ( tmptr == NULL)
      {  printf ("ERROR: Reallocating the dialog stack\n");
         return NULL;
      } //else printf ("DEBUG: reallocating memory, tmptr=%p\n", tmptr);
      dstack->dlg = tmptr;
      dstack->size = tmpsize;
      //Zero the new portion of the array.
      for ( int i = dstack->nb_dialog ; i < dstack->size ; i++ ) gm_dialog_clear (&dstack->dlg[i]);
   }

   if ( dstack->nb_dialog > 0 && dstack->nb_dialog < dstack->size) //precaution to avoid invalid index
   {  int dlgindex = dstack->nb_dialog - 1;
      gm_dialog_clear (&dstack->dlg[dlgindex]);
      //dstack->dlg[dlgindex].components = components;
      return &dstack->dlg[dlgindex];
   }

   return NULL;

}

/*! @brief Returns the dialog at the top of the stack
*/
gms_dialog *gm_dialog_get_active ( gms_glymmer *engine )
{  gms_dialog_stack* dstack = &engine->dstack;
   return &dstack->dlg[ dstack->nb_dialog - 1];
}


/*! @brief close a certain number of dialogs on the stack
*/

void gm_dialog_close ( gms_glymmer *engine, int nb )
{  gms_dialog_stack* dstack = &engine->dstack;
   for ( int i = 0 ; i < nb ; i++ )
   {  if ( dstack->nb_dialog > 0)
      {  gm_dialog_dispose ( &dstack->dlg[dstack->nb_dialog-1] );
         dstack->nb_dialog--;
      }
   }
}

void gm_dialog_close_all (gms_glymmer *engine)
{  gms_dialog_stack* dstack = &engine->dstack;
   for ( int i = dstack->nb_dialog-1; i > 0; i-- )
   {  gm_dialog_dispose ( &dstack->dlg[i] );
   }

   dstack->nb_dialog = 0;
}

/*! @brief Clear the dynamic allocation of the stack
*/
void gm_dialog_stack_dispose ( gms_dialog_stack *dstack)
{  free ( dstack->dlg );
}

/*! @brief Add items to dynamic list
   Dynamic list requires items to be added while the program is running. You must call
   this function to add items to the list. Choices will be stored dynamically in a text buffer
   equal to the screen width in characters.
*/
//TODO: A bit out of place. Maybe repurpose the dialog stack header source for library include.
void gm_list_item_add (gms_glymmer *e, gms_dialog *dlg, int key, unsigned char mark, char *text)
{  if ( has_component( dlg, GM_COMPONENT_LIST | GM_COMPONENT_NAVIGATION ) && dlg->lst.dynamic)
   {  if ( dlg->lst.length >= dlg->lst.size)
      {  dlg->lst.size *= 2;
         dlg->lst.items = (gms_item*) realloc ( dlg->lst.items, sizeof(gms_item) * dlg->lst.size);
      }
      dlg->lst.items[dlg->lst.length].key = key;
      dlg->lst.items[dlg->lst.length].mark = mark;
      dlg->lst.items[dlg->lst.length].text = (char*) malloc (sizeof(char) * (e->screen.text_w+1));

      strncpy ( dlg->lst.items[dlg->lst.length].text, text, e->screen.text_w );
      dlg->lst.items[dlg->lst.length].text[e->screen.text_w] = '\0';
      //printf ("DEBUG: Dialog record is [%d]: %s:\n", key, dlg->lst.items[dlg->lst.length].text);
      dlg->lst.length++;

      //updating navigation
      dlg->nav.nb_item++;
      //printf ("DEBUG: %d / %d = ", dlg->nav.nb_item, dlg->nav.page_size );
      int nb_page = dlg->nav.nb_item / dlg->nav.page_size;
      //printf ("%d\n",nb_page );//DEBUG
      if ( dlg->nav.nb_item % dlg->nav.page_size > 0 ) nb_page++;
      dlg->nav.nb_page = nb_page;
   }
}

