/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 24th, 2022
   @license: Apache 2.0 License

   Common routines that can be used by anybody.
*/

#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED


int modulo(int value, int mod);
void make_screen_shot ( char *filename );
//TODO: Would make more sense to be moved into UGLI the GL wrapper
void assert_or_exit ( bool condition, int exitcode, char *message);

//TODO: Debugging printing function with message blocking via constant definition.
//Get inspiration from libGDX. Could also be in UGLI the GL wrapper


#endif // COMMON_H_INCLUDED
