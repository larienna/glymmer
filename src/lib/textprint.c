/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 23rd, 2022
   @license: Apache 2.0 License
*/

#include <stdbool.h>
#include <stdio.h>
//#include <stdarg.h>
#include <allegro.h>
#include <gm_struct.h>
#include <textprint.h>
#include <gm_engine.h> //change if put all constants in a single file.
#include <component.h>

//_____ Constants _____

#define LINE_BUFFER_LEN 256


//______ Methods _____

char get_instruction_code ( int instruction_mask )
{  switch ( instruction_mask )
   {  case GM_INSTRUCTION_ACCEPT:     return GM_SCHAR_ACCEPT;
      case GM_INSTRUCTION_CANCEL:     return GM_SCHAR_CANCEL;
      case GM_INSTRUCTION_UPDOWN:     return GM_SCHAR_UPDOWN;
      case GM_INSTRUCTION_LEFTRIGHT:  return GM_SCHAR_LEFTRIGHT;
      case GM_INSTRUCTION_PGUPDN:     return GM_SCHAR_PGUPDOWN;
      case GM_INSTRUCTION_HELP:       return GM_SCHAR_HELP;
      case GM_INSTRUCTION_SORT:       return GM_SCHAR_SORT;
      default: return GM_SCHAR_EMPTY;
   }
}


/** This is a helper method to simplify the access to the character array to make
    the code cleaner and assign the character and color at the same time. It does not
    touch the background glyph since it's a separate printing process.
*/
//NOTE: Could be pretty slow. Maybe make line drawing functions if need optimisation.
void gm_set_text_char ( gms_screen *e, int x, int y, char character, unsigned char color, bool underlined )
{  if ( y >= 0 && y < e->text_h && x >= 0 && x < e->text_w )
   {  int index = y * e->text_w + x;
      e->text_buffer [index].letter = character;
      e->text_buffer [index].color = color;
      e->text_buffer [index].underlined = underlined;
   }
}


/*! @brief Set the character in the text buffer and adds the offset of the window position
*/

void gm_set_text_char_window ( gms_screen *e, gms_window *win, int x, int y, char character, signed char color, bool underlined )
{  if ( y > 0 && x > 0 && y < win->h -1 &&  x < win->w-1) //prevent printing outside window
   {  gm_set_text_char ( e, win->x + x, win->y + y, character, color, underlined );
   }
}

/* @brief Another variation that allow printing on the edges of the window
   change the clamping limits
*/

void gm_set_text_char_window_edge ( gms_screen *e, gms_window *win, int x, int y, char character, signed char color, bool underlined )
{  if ( y >= 0 && x >= 0 && y < win->h &&  x < win->w) //prevent printing outside window
   {  gm_set_text_char ( e, win->x + x, win->y + y, character, color, underlined );
   }
}

void gm_set_text_icon_window ( gms_screen *e, gms_window *win, int x, int y, char icon, unsigned char color, bool underlined )
{  x = win->x + x;
   y = win->y + y;
   if ( y >= 0 && y < e->text_h && x >= 0 && x < e->text_w )
   {  //printf ("DEBUG: text-icon writting is beign done. icon=%d, underlined=%d\n", icon, underlined);
      int index = y * e->text_w + x;
      e->text_buffer [index].icon = icon;
      e->text_buffer [index].underlined = underlined;
      e->text_buffer [index].color = color;
   }
}

/*! @brief Set the bg glyph in the text buffer and adds the offset of the window position
   @param overlap determine if replace top character or if backup top character behind to simulate
          dialog overlap and manage transparency correctly.
*/

void gm_set_text_bg_window ( gms_screen *e, gms_window *win, int x, int y, unsigned char glyph,
      bool overlap)
{  x = x + win->x;
   y = y + win->y;
   if ( y >= 0 && y < e->text_h && x >= 0 && x < e->text_w )
   {  int index = y * e->text_w + x;
      /*if ( x == 2 && y == 1)
      { printf ("DEBUG old %d, overwrite %d by %d\n",
         e->text_buffer [index].bg_glyph_previous,
         e->text_buffer [index].bg_glyph, glyph );
      }*/
      if ( overlap ) e->text_buffer [index].bg_previous = e->text_buffer [index].bg;
      e->text_buffer [index].bg = glyph;
   }
}


/*! @brief Get the color of a specific character in the buffer.
*/

signed char gm_get_text_color ( gms_screen *e, int x, int y )
{  return e->text_buffer [y * e->text_w + x].color;
}

/*! @brief Get the character of a specific character in the buffer.

   The goal is to encapsulate at one place pointer arithmetics.
*/

char gm_get_text_char ( gms_screen *e, int x, int y )
{  return e->text_buffer [y * e->text_w + x].letter;
}

/*! @brief Get the background glyph in the buffer.

   The goal is to encapsulate at one place pointer arithmetics.
*/

char gm_get_text_bg ( gms_screen *e, int x, int y )
{  return e->text_buffer [y * e->text_w + x].bg;
}


/*! @brief Helper function that analyse color codes and make sure they are valid digits
   It will return a code between 1 and 10 as a char for smaller storage
   or a negative number if the colors must be inverted
   Color codes are capital letters and small cap letters for inverted colors.
   0 is a default color returned in case of error. So color index starts at 1
*/

/*char gm_read_color_code ( char character )
{  if ( character >= '0' && character <= 'J' )
   {  return character - 'A' + 1;
   }
   else if ( character >= 'a' && character <= 'j' )
   {  return ( -1 - ( character - 'a' )); //colors are offset by 1 since 0 cannot be negative
   }

   return 0; //in case of error, return color 0 the default color
}*/

/*! @brief Convert a single hex digit character to an integer value between 0-16
*/

int hextoi ( char digit )
{  if ( digit >= '0' && digit <='9')
   {  return digit - '0';
   }
   if ( digit >= 'A' && digit <= 'F' )
   {  return digit - 'A' + 10;
   }

   return -1; // in case of translation error, return -1
}

/*! @brief Each 2 character hex code to return a value between 0 and 255
*/

unsigned char gm_read_icon_code ( char high, char low )
{
   unsigned char value = 0;
   //printf ("DEBUG: Reading icon hex: [%c:%c]\n", high, low);

   int highval = hextoi ( high);
   if ( highval == -1 ) return 0;
   value += highval * 16;
   int lowval = hextoi( low);
   if ( lowval == -1 ) return 0;
   value += lowval;

   return value;

}


void gm_clear_text_buffer ( gms_screen *scr )
{  int length = scr->text_h * scr->text_w;
   for ( int i = 0 ; i <= length; i++ )
   {  scr->text_buffer[i].bg = GM_GLYPH_EMPTY;
      scr->text_buffer[i].bg_previous = GM_GLYPH_EMPTY;
      scr->text_buffer[i].letter = ' '; //requires to make unit test pass, the graphic output should be the same.
      scr->text_buffer[i].color = 0;
      scr->text_buffer[i].icon = 0;
      scr->text_buffer[i].underlined = false;
   }
   //printf ("DEBUG: reset text buffer\n");
}

/*! @brief Clear the area in the text buffer where the window should be drawn
*/

void gm_clear_text_window ( gms_screen *e, gms_window *win )
{  for ( int j = 0 ; j < win->h ; j++ )
      for ( int i = 0 ; i < win->w ; i++)
      {  gm_set_text_char_window( e, win, i, j, ' ', 0, false );
      }
   for ( int j = 1 ; j < win->h -1; j++ )
      for ( int i = 1 ; i < win->w -1; i++)
      {   gm_set_text_bg_window(e, win, i, j, GM_GLYPH_BACKGROUND, true);
          gm_set_text_icon_window(e, win, i, j, 0, GM_COLOR_TEXT, false);
      }
   //draw shadows (Buggy, would require refinements)
   /*for ( int j = 1 ; j < win->h ; j++ )
   {  gm_set_text_bg_window(e, win, win->w, j, GM_GLYPH_DROP_SHADOW, true);
   }
   for ( int i = 1 ; i < win->w ; i++ )
   {  gm_set_text_bg_window(e, win, i, win->h, GM_GLYPH_DROP_SHADOW, true);
   }
   gm_set_text_bg_window(e, win, win->w, win->h, GM_GLYPH_DROP_SHADOW, true);*/


   win->line_cursor = 1; // must skip over border
}


/*! Helper method used by the print wrapped functions to determine the portion of the
    text buffer that must be printed on a sigle line without overflowing

    @param str string of text to analyse
    @param str_length length of the string using a previously called strlen
    @param w width of the window, so -2 char will be subtracted for text space
    @param position ending position in the string to start the analysis
           also returns the last position read after the function call

    @return the number of characters from the string to put on the line.

    @note Remember that '\n' and '^' will be replaced with regular spaces
*/
int gm_wrapped_nb_char_on_line ( const char *str, int str_length, int w, int *position )
{  int nb_char = 0;
   bool flush_line = false;
   int next_nb_char = 0;
   int line_start = *position;
   int next_word_end = *position;
   //int debug_same = 0;

   while ( flush_line == false && next_word_end < str_length)
   {
      //find the next word and do not special codes
      while ( str[next_word_end] != ' ' && str[next_word_end] != GM_CODE_NEWLINE &&
             str[next_word_end] != GM_CHARCODE_NEWLINE && str[next_word_end] != '\0' &&
              next_word_end < str_length)
      {  //printf ("DEBUG: Reading '%c'\n", str[next_word_end] );
         switch (str[next_word_end])//ignore underline and game icons
         {  case GM_CODE_UNDERLINE:
            case GM_CHARCODE_UNDERLINE:
                  next_word_end++;
            break;
            case GM_CODE_APPLICATION:
            case GM_CHARCODE_APPLICATION:
               next_word_end += 3; //skip control code and 2 icon numbers
            break;
            default:
               next_nb_char++;
               next_word_end++;
            break;

            /*if ( gm_read_color_code( str[next_word_end]) != 0 ) //check if color code valid
            {  next_word_end++; // skip code letter
            }
            else //will need to interpret letter as a regular letter
            {  next_nb_char++;
            }*/
            //next_nb_char++;
         }
      }

      /*if ( str[next_word_end] == '^'
             || str[next_word_end] == GM_CHARCODE_NEWLINE
             || str[next_word_end] == '\0')
      {  flush_line = true;
         next_word_end++;
         //next_nb_char++;
      }*/

      if ( next_nb_char <= (w -2) ) //is within the limit
      {   *position = next_word_end; //comitting values for current valid words that fit
          nb_char = next_nb_char;   //into the desired space.
          //printf ("do not flush : ");
          next_word_end++;
          next_nb_char++;
      }
      else //is outside the limit
      {  flush_line = true;
         //printf ("do flush : ");
      }
      //if ( flush_line ) printf ("DEBUG: Flushing Line\n");

      //DEBUG
      //printf ("nc=%d, ls=%d, nwc=%d, nwe=%d\n", nb_char, line_start, next_nb_char, next_word_end );
      /*{  if ( line_start == line_end ) debug_same++;
         //else debug_same = 0;
         if ( nb_char == line_end ) debug_same++;
         //else debug_same = 0;
         if ( debug_same > 15 ) exit (1);
      }*/

   }

   //if no words has been counted yet, at least 1 word must be put.
   //If word larger than window, will be cut.
   if ( *position == line_start)
   {
      *position = next_word_end;
      nb_char = next_nb_char;
   }

   return nb_char;
}

/*!
   @brief Print text into the window.

   This method is used like printf to copy a new string of text into the buffer of the window.
   All printing is done at the cursor position. Any overflow outside the window is simply ignored.

   @param[in] gms_window The window to print the text info
   @param[in] str String of text copied into the window.
*/

void gm_text_print ( gms_screen *e, gms_window *win, unsigned char colorcode, const char *str )
{  int str_cursor = 0;
   int str_length = strlen ( str );
   int col_cursor = 1;
   int tmpcolor = colorcode;
   bool underlined = false;
   //signed char tmpcode;


   while ( str_cursor < str_length )
   {  //printf (">%c", str[str_cursor] );
      //if ( win->line_cursor < win->h - 1 && col_cursor < win->w -1) //exceeding char will not be printed
      //{//disabled since prevented underline and new line from beign done if out of window.
      //Out of window printing is blocked later.
      switch ( str[str_cursor] )
         {  case GM_CODE_NEWLINE:
            case GM_CHARCODE_NEWLINE:
               win->line_cursor++;
               col_cursor = 1;
            break;
            case GM_CODE_UNDERLINE:
            case GM_CHARCODE_UNDERLINE:
               // flip the underlined switch and force disable underline when color disabled used.
               //alternate version with disabled and side textcode. Overrides param color with underline
               if ( colorcode == GM_COLOR_DISABLED ) tmpcolor = GM_COLOR_DISABLED;
               else if ( colorcode == GM_COLOR_SIDETEXT ) tmpcolor = GM_COLOR_SIDETEXT;
               else tmpcolor = tmpcolor == GM_COLOR_UNDERLINE ? colorcode : GM_COLOR_UNDERLINE;
               underlined = underlined ? false : true;
            break;
            case GM_CODE_APPLICATION:
            case GM_CHARCODE_APPLICATION:
               //printf ("Sending characters [%c:%c]\n", str[str_cursor+1], str[str_cursor+2]);
               //printf ("iconcode = %d\n", iconcode);
               gm_set_text_icon_window( e, win, col_cursor, win->line_cursor,
               gm_read_icon_code(str[str_cursor+1], str[str_cursor+2]),
               tmpcolor, underlined);
               str_cursor += 2; //must skip 2 additional char for the code
               col_cursor++; //only advance 1 since icon use 1 space
            break;

            default:
               gm_set_text_char_window ( e, win, col_cursor , win->line_cursor, str [str_cursor], tmpcolor, underlined);
               col_cursor++;
            break;
         }
      //}
      str_cursor++;
   }


   //makes sure a line is skipped at the end since it's a line by line drawing.
   if ( col_cursor > 1 ) win->line_cursor++;
}

//line ofset is used for multi page text drawing if you want to start printing further away
//in the string of text.
void gm_text_printwrap ( gms_screen *e, gms_window *win, unsigned char colorcode, const char *str, int line_offset )
{  char line [ LINE_BUFFER_LEN ]; // temporary buffer to hold a line + \0
   int str_length = strlen ( str );
   int line_start = 0; //start of text to copy in line buffer
   int line_end = 0; //end of text to copy in line buffer
   //int next_word_end = 0; //place where the next word ends, could be kept or not
   int nb_char = 0; //number of printable characters accumulated so far ( exclude color codes)
   //int next_nb_char = 0; // nb of char of the next word (excluding color codes)
   int nb_padding = 0; //number of characters to add
   //bool flush_line; // set to true if it's time to print the line



   while ( line_end < str_length ) //loop until the end of the string of text is not printed
   {  /*flush_line = false;
      next_nb_char = 0;
      nb_char=0;
      next_word_end = line_end;

      while ( flush_line == false && next_word_end < str_length)
      {
         //find the next word and do not count color characters

         while ( str[next_word_end] != ' ' && str[next_word_end] != GM_CHARCODE_NEWLINE &&
                str[next_word_end] != '^' && str[next_word_end] != '\0' &&
                 next_word_end < str_length)
         {  if ( str[next_word_end] == '^'
                || str[next_word_end] == GM_CHARCODE_NEWLINE
                || str[next_word_end] == '\0') flush_line = true;

            if ( str[next_word_end] == GM_CHARCODE_COLOR )
            {   next_word_end++; //ignore color codes
               if ( read_color_code( str[next_word_end]) != 0 ) //check if color code valid
               {  next_word_end++; // skip code letter
               }
               else //will need to interpret letter as a regular letter
               {  next_nb_char++;
               }
            }
            else
            {  next_nb_char++;
               next_word_end++;
            }
         }

         if ( next_nb_char <= (win->w -2) )
         {   line_end = next_word_end; //comitting values for current valid words that fit
             nb_char = next_nb_char;   //into the desired space.
             //printf ("do not flush : ");
             next_word_end++;
             next_nb_char++;
         }
         else
         {  flush_line = true;
            //printf ("do flush : ");
         }*/

         //DEBUG
        /* printf ("nc=%d, ls=%d, le=%d, nwe=%d\n", nb_char, line_start, line_end, next_word_end );
         {  if ( line_start == line_end ) debug_same++;
            //else debug_same = 0;
            if ( nb_char == line_end ) debug_same++;
            //else debug_same = 0;
            if ( debug_same > 15 ) exit (1);
         }*/

      /*}*/
      //printf ("I flushed the line\n");

      nb_char = gm_wrapped_nb_char_on_line ( str, str_length, win->w, &line_end );
      //printf ("DEBUG Position: %d\n", line_end );

      //build the line to print
      if ( line_offset <= 0 ) //skip a certain number of lines
      {
         int c = 0;
         //if on last line, do not pad the text
         if ( line_end + 1 < str_length ) nb_padding = win->w -2 - nb_char;
         else nb_padding = 0;

         //printf ("DEBUG: w=%d, nc=%d, padding=%d\n", win->w, nb_char, nb_padding );

         while ( line_start < line_end && c < e->text_w )
         {  if ( /*str [line_start] == '^' || */str [line_start] == GM_CODE_NEWLINE )
            {  line [ c ] = ' '; // replace carriage returns with spaces for easier wrapping
               //TODO: Not sure it's the best solution, but could be easier for nb_line anticipation.
            }
            else line [ c ] = str [line_start];

            if ( line [c] == ' ' && nb_padding > 0 && c > 0 && line [c-1] != ' ' )
            {  c++;
               line [c] = ' ';
               nb_padding--;
            }
            c++;
            line_start++;
         }
         line [c] = '\0';
         gm_text_print( e, win, colorcode, line );
      }
      line_offset--;
      line_end++;
      line_start = line_end;

   }
}

/*! Reused the printwrap algorithm to determine how much lines will be printed according to
    window width. Used by dialogs to define height automatically.

    @param str    String of text to analyse
    @param w      width of the window

    @return       number of lines
*/

int gm_anticipate_printwrap_nb_lines ( const char *str , int w)
{  int line_count = 0;
   int line_end = 0;
   int str_length = strlen ( str );

   while ( line_end < str_length )
   {  gm_wrapped_nb_char_on_line( str, str_length, w, &line_end);
      line_count++;
      line_end++;
   }

   //TODO: Maybe add the count of carriage return and do not replace CR with spaces

   return line_count;

}

/*! @brief Simply count the number of '\n' characters to anticipate nb of lines used.
*/
int gm_count_carriage_return ( const char *str )
{  int count = 0;
   for ( int i=0; str[i] != '\0'; i++) if ( str[i] == '\n') count++;
   return count;
}

void gm_text_printf ( gms_screen *e, gms_window *win, unsigned char colorcode, const char *format, ... )
{
   char buffer [ LINE_BUFFER_LEN ]; // screen should not be more than 256 char wide
   va_list valist;

   va_start(valist, format);
   vsnprintf (buffer, sizeof(buffer), format, valist );

   gm_text_print ( e, win, colorcode, buffer );
   va_end(valist);
}

/*! @brief Creates the window border in the text buffer

    If there is no navigation in the dialog, pass NULL as nav
*/

void gm_print_window_border ( gms_screen *scr, gms_window *win )
{  //draw corners
   //gm_set_text_char_window ( e, win, 0, 0, WUI_CHAR_CORNER_UPLEFT, GM_COLOR_LGRAY );
   //gm_set_text_char_window ( e, win, win->w-1, 0, WUI_CHAR_CORNER_UPRIGHT, GM_COLOR_LGRAY );
   //gm_set_text_char_window ( e, win, 0, win->h-1, WUI_CHAR_CORNER_DOWNLEFT, GM_COLOR_LGRAY );
   //gm_set_text_char_window ( e, win, win->w-1, win->h-1, WUI_CHAR_CORNER_DOWNRIGHT, GM_COLOR_LGRAY );

   gm_set_text_bg_window ( scr, win, 0, 0, GM_GLYPH_CORNER_TOPLEFT, true);
   gm_set_text_bg_window ( scr, win, win->w-1, 0, GM_GLYPH_CORNER_TOPRIGHT, true );
   gm_set_text_bg_window ( scr, win, 0, win->h-1, GM_GLYPH_CORNER_BOTTOMLEFT, true );
   gm_set_text_bg_window ( scr, win, win->w-1, win->h-1, GM_GLYPH_CORNER_BOTTOMRIGHT, true );

   //draw lines
   for ( int x = 1 ; x < win->w -1; x++ )
   {  gm_set_text_bg_window ( scr, win, x, 0, GM_GLYPH_EDGE_TOP, true );
      gm_set_text_bg_window ( scr, win, x, win->h-1, GM_GLYPH_EDGE_BOTTOM, true );
   }

   for ( int y = 1 ; y < win->h -1; y++ )
   {  gm_set_text_bg_window ( scr, win, 0, y, GM_GLYPH_EDGE_LEFT, true );
      gm_set_text_bg_window ( scr, win, win->w-1, y, GM_GLYPH_EDGE_RIGHT, true );
   }

   //Draw Title
   int titlelen;
   int xpos = 0;
   int i;
   if ( win->title != NULL )
   {  titlelen = strlen (win->title);
      //Limit length by excluding corner and transition characters
      if ( win->w - 4 < titlelen) titlelen = win->w -4;
      //Add up spacing to center title
      if ( titlelen < win->w ) xpos += ( win->w - 2 - titlelen ) / 2;

      //textprintf_old ( buffer, fnt, x, y, General_COLOR_TEXT, "%c", WUI_CHAR_TITLE_CUT_START );
      gm_set_text_bg_window ( scr, win, xpos, 0, GM_GLYPH_TOP_START, false );
      xpos++;
      for ( i = 0 ; i < titlelen ; i++)
      {  gm_set_text_char_window_edge ( scr, win,xpos + i, 0, win->title [ i ], GM_COLOR_BORDER, false );
         gm_set_text_bg_window( scr, win, xpos+i, 0, GM_GLYPH_HORIZONTAL, false);

         //textprintf_ex( buffer, fnt, x, y, color, General_COLOR_TEXT, "%c", win->title [ i ]);
         //x += WUI_FONT_SIZE;
      }
      gm_set_text_bg_window ( scr, win, xpos + i, 0, GM_GLYPH_TOP_END, false );

      //textprintf_old ( buffer, fnt, x, y, General_COLOR_TEXT, "%c", WUI_CHAR_TITLE_CUT_END );
   }


}

/*! @brief Print window border navigation information
*/

void gm_print_window_navigation (gms_screen *scr, gms_window *win, gms_navigation *nav )
{

   if ( nav->instructions > 0 )
   {  int x = win->w -2;
      //printf ("DEBUG: Starting to Draw instructions\n");
      gm_set_text_bg_window ( scr, win, x, win->h-1, GM_GLYPH_BOTTOM_END, false );

      int inscount = 0;
      x--;
      while ( x > 1 && inscount < 8)
      {  if ( ( (1 << inscount) & nav->instructions ) > 0 )
         {  gm_set_text_char_window_edge ( scr, win, x, win->h-1,
            get_instruction_code ( 1 << inscount ), GM_COLOR_BORDER, false );
            gm_set_text_bg_window( scr, win, x, win->h-1, GM_GLYPH_HORIZONTAL, false);
            //printf ("DEBUG: printinf %d, x=%d\n", 1 << inscount , x);
            x--;
         }
         inscount++;
      }
      gm_set_text_bg_window ( scr, win, x, win->h-1, GM_GLYPH_BOTTOM_START, false );
   }

   //Draw Pages
   if ( nav->nb_page > 1 && win->h >= 6) //need paging and a min height for the window
   {  // old version with numbered pages
      /*char pagestr [6];
      char *format;
      if ( nav->nb_page > 9) format = "%2d/%2d";
      else format = "%1d/%1d";

      snprintf( pagestr, 6, format, (nav->cursor / nav->page_size)+1 , nav->nb_page );
      gm_set_text_bg_window ( scr, win, 1, win->h-1, GM_GLYPH_BOTTOM_START, false );
      int i = 0;
      while  ( pagestr [i] != '\0' )
      {  gm_set_text_char_window_edge ( scr, win, 2+i, win->h-1, pagestr [ i ], GM_COLOR_BORDER  );
         gm_set_text_bg_window( scr, win, 2+i, win->h-1, GM_GLYPH_HORIZONTAL, false);
         i++;
      }
      gm_set_text_bg_window ( scr, win, i+2, win->h-1, GM_GLYPH_BOTTOM_END, false );*/
      //new side icons
      gm_set_text_bg_window ( scr, win, win->w-1, 1, GM_GLYPH_RIGHT_START, false );
      gm_set_text_bg_window ( scr, win, win->w-1, 2, GM_GLYPH_VERTICAL, false );
      gm_set_text_bg_window ( scr, win, win->w-1, 3, GM_GLYPH_VERTICAL, false );
      gm_set_text_bg_window ( scr, win, win->w-1, 4, GM_GLYPH_RIGHT_END, false );

      if ((nav->cursor / nav->page_size) != 0)
      {  gm_set_text_char_window_edge ( scr, win, win->w-1, 2, GM_SCHAR_PAGEUP, GM_COLOR_BORDER, false  );
      }
      /*printf ("DEBUG: cursor=%d, pgsize=%d, nbpg=%d, div=%d\n",
         nav->cursor, nav->page_size, nav->nb_page, (nav->cursor / nav->page_size));*/
      if (nav->cursor < nav->nb_item - nav->page_size )
      {  gm_set_text_char_window_edge ( scr, win, win->w-1, 3, GM_SCHAR_PAGEDOWN, GM_COLOR_BORDER, false  );
      }
   }

}

/*! @brief Print the content for text component
*/

void gm_print_window_content_text ( gms_screen *scr, gms_dialog *dlg, bool is_active_dialog )
{  int color = is_active_dialog ? GM_COLOR_TEXT : GM_COLOR_SIDETEXT;
   //int color = GM_COLOR_TEXT;
   if ( dlg->txt.wrap == true )
   {  if (dlg->txt.nb_lines != 1 && has_component ( dlg, GM_COMPONENT_NAVIGATION)
          && !has_component ( dlg, GM_COMPONENT_LIST) )
      {  gm_text_printwrap ( scr, &dlg->win, color, dlg->txt.message, dlg->nav.cursor );
      }
      else gm_text_printwrap ( scr, &dlg->win, color, dlg->txt.message, 0 );

   }
   else
   {  int color = is_active_dialog ? GM_COLOR_MESSAGE : GM_COLOR_SIDETEXT;
      gm_text_print ( scr, &dlg->win, color, dlg->txt.message );
   }
}

/*! @brief Print the window content for list components
*/

void gm_print_window_content_list ( gms_screen *scr, gms_dialog *dlg, bool is_active_dialog )
{
   //int page_cursor = modulo ( dlg->nav.cursor, dlg->nav.page_size );
   //int page_offset;
   //unsigned char tmpchar;
/*   if ( dlg->nav.page_size != 0)
   {  page_offset = ( dlg->nav.cursor / dlg->nav.page_size) * dlg->nav.page_size;
   }
   else page_offset = 0;*/
   int page_end = dlg->nav.page_offset + dlg->nav.page_size;
   //printf ("DEBUG: page_offset = %d, page_end = %d\n", dlg->nav.page_offset, page_end );

   for ( int i = dlg->nav.page_offset ; i < page_end ; i++ )
   {  int color = is_active_dialog ? GM_COLOR_TEXT : GM_COLOR_SIDETEXT;
      //int color = GM_COLOR_TEXT;
      if ( i >= 0 && i < dlg->lst.length ) //safeguard against out of bound index
      {  gm_text_printf ( scr, &dlg->win, color, " %s", dlg->lst.items[i].text );
      }
      if ( dlg->nav.cursor == i && is_active_dialog) //hard code print cursor
      {  gm_set_text_char_window( scr, &dlg->win, 1, dlg->win.line_cursor-1,
            GM_SCHAR_ARROW_RIGHT, GM_COLOR_CURSOR, false);
         for ( int x = 2; x < dlg->win.w-1; x++) //draw highlighted selection
         {  gm_set_text_bg_window(scr, &dlg->win, x, dlg->win.line_cursor-1, GM_GLYPH_SELECTED, false);
         }
      }

   }

}

/*REFERENCE CODE: handle data components which should now be a sub section of list
if ( has_component (dlg, WUI_COMPONENT_DATA ) )
   {  if ( has_component (dlg, WUI_COMPONENT_NAVIGATION) )
      {  int error = SQLprepare (dlg->dat.query);
         if (error == SQLITE_OK )
         {  error = SQLstep ();
            while (error == SQLITE_ROW)
            {  // printf ("debug: 3");
               tmpchar = ' ';
               //TODO: this is a temporary demo, it's not generic code.
               //will need to create a vector, maybe all as strings, not sure
               window_text_printf ( &dlg->win, "\vF%c\vA%s", tmpchar, SQLcolumn_text (1) );

               //strncpy ( tmpstr, SQLcolumn_text (1), Menu_STR_SIZE);
               //add_item (SQLcolumn_int (0), tmpstr);
               error = SQLstep();
               //TODO: Problem, answer is part of the menuitems, but be stored somewhere
               //unless only the active selection is used
               //easier if stored as menu items, but will require dynamic allocation
            }
            SQLfinalize();
         }
      }
      else
      {  //TODO: print single line data since there is no navigation
      }
   }

*/
