/**
   Glymmer

   @author: Eric Pietrocupo
   @date: January 16th, 2022
   @license: Apache 2.0 License

   Module that is used to draw content on the screen.
*/

#ifndef GMCOLORS_H_INCLUDED
#define GMCOLORS_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

void gm_clear_screen_bitmap ( BITMAP *bmp, int color );
void gm_blit_text_buffer ( gms_glymmer *e, BITMAP *bmp );

#ifdef __cplusplus
}
#endif


#endif // GMCOLORS_H_INCLUDED
