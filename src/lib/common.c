/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 24th, 2022
   @license: Apache 2.0 License
*/

#include <allegro.h>
#include <stdbool.h>
#include <stdio.h>

/*! @brief Makes modulus calculations.
*/
int modulo(int value, int mod)
{//SOURCE: https://stackoverflow.com/questions/11720656/modulo-operation-with-negative-numbers
    int r = value % mod; //get the remainder
    return r < 0 ? r + mod : r; //if negative, add the mod
}

/*! @brief copy the current screen buffer into a file
*/

void make_screen_shot ( char *filename )
{
   BITMAP *scrnshot;
   PALETTE pal;

   scrnshot = create_bitmap ( SCREEN_W, SCREEN_H );

   blit ( screen, scrnshot, 0, 0, 0, 0, SCREEN_W, SCREEN_H );
   get_palette ( pal );


   save_bitmap (filename, scrnshot, pal );

   destroy_bitmap ( scrnshot );
}

void assert_or_exit ( bool condition, int exitcode, char *message)
{  if ( condition == false)
   {  printf ("ASSERTION FAILED: %s\n", message );
      exit (exitcode);
   }
}
