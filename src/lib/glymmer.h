/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 23rd, 2022
   @license: Apache 2.0 License

   This is the public header file that will be used by the program. It only includes what is
   open to the public.
*/
//TODO: Rightnow, only includes essential files to avoid copying multiple .h to the install
//directory, see if like SDL, I could put eveything into a subdirectory.
//TODO: constant header, since need to separate public from private, a file with all constants seems logical.
#ifndef GLYMMER_H_INCLUDED
#define GLYMMER_H_INCLUDED

#include <gm_struct.h>
#include <gm_common.h>
#include <gm_engine.h>
#include <gm_dialog_factory.h>
#include <gm_input.h>
#include <gm_dialog_stack.h>


#endif // GLYMMER_H_INCLUDED
