/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 29th, 2022
   @license: Apache 2.0 License

   List of factory methods to create predefined dialogs into the stack
*/

#ifndef DIALOG_FACTORY_H_INCLUDED
#define DIALOG_FACTORY_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif

//_____ empty based dialogs _____
gms_dialog* gm_dialog_open_empty ( gms_glymmer *e, char *title, int x, int y, int w, int h );
gms_dialog* gm_dialog_open_background ( gms_glymmer *e, char *title );

//_____ Text based dialogs _____
gms_dialog *gm_dialog_open_text ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message, bool wrap );
gms_dialog *gm_dialog_open_message ( gms_glymmer *e, char *title, char *message );
gms_dialog *gm_dialog_open_pagedtext ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message );
gms_dialog *gm_dialog_open_dynamic_pagedtext ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message );
gms_dialog *gm_dialog_open_confirm ( gms_glymmer *e, char *title, char *message );

//_____ List based dialogs _____

gms_dialog *gm_dialog_open_list ( gms_glymmer *e, char *title, int x, int y, char *message, gms_item *items );
gms_dialog *gm_dialog_open_pagedlist ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message, gms_item *items );
gms_dialog *gm_dialog_open_question_yesno ( gms_glymmer *e, char *title, char *message );
gms_dialog *gm_dialog_open_dynamic_pagedlist ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message );

//_____ Other dialogs _____
gms_dialog *gm_dialog_open_question_lr ( gms_glymmer *e, char *title, char *message );

//_____ Database related dialogs _____
#ifdef GM_ADD_DATABASE_SUPPORT
gms_dialog *gm_dialog_open_database_menu ( gms_gymmer *e, char *title, int x, int y, char *header,
                                             char *format, char *query );
//TODO Should be only a single function
gms_dialog *gm_dialog_open_database_list ( gms_glymmer *e, char *title, int x, int y, int w, int h,
                                             char *header, char *format, char *query );
#endif

#ifdef __cplusplus
}
#endif

#endif // DIALOG_FACTORY_H_INCLUDED
