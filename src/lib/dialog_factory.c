/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 29th, 2022
   @license: Apache 2.0 License
*/

#include <stdbool.h>
#include <stdio.h>
#include <allegro.h>
#include <gm_struct.h>
#include <component.h>
#include <gm_dialog_stack.h>
#include <textprint.h>


/* @brief Default list of items for the yes no questions
*/
gms_item yesno_question_items [] = { { 0, 0, "Yes" },
                               { -1, 0, "No" },
                               { 0, 0, NULL }
                             };

/* @brief Get the size of a list of items
   loop around the array until a NULL string as text is found which should mark the end of the
   list. Make sure there is a NULL string, else random size would be returned. The max is 65535.
   If the maximum is reach then something fishy is going on.
*/
unsigned short sizeof_list ( gms_item *items )
{  int count = 0;
   while ( items[count].text != NULL && count < 65535 )
   {  count++;
   }
   if ( count == 65535 )
   {  printf ("WARNING: items list size is 65535 which is very improbabale.\nMaybe you forgot to add a NULL terminator in the _text_ field of the structure\n" );
   }
   return count;
}
//TODO: See if there could be an interest to reduce max size of 256. Is it likely to have bigger
//non-database list size.

//Does not work because length is lost inside the method
//#define SIZEOFLIST(LIST) (sizeof((LIST)) / sizeof (gms_item) )

/*! @brief Open an empty window with at most a title in the border
*/
gms_dialog* gm_dialog_open_empty ( gms_glymmer *e, char *title, int x, int y, int w, int h )
{  gms_dialog *dialog = gm_dialog_open( e );
   if ( dialog != NULL )
   {  gm_window_add ( dialog, &e->screen, title, x, y, w, h );
      gm_navigation_add( dialog, 0, 0, 1, GM_INSTRUCTION_ACCEPT ); //in case ends on top of stack
   }
   return dialog;
}

/*! @brief Predefined dialog: Opens an empty window that covers the entire screen
*/
gms_dialog* gm_dialog_open_background ( gms_glymmer *e, char *title )
{  return gm_dialog_open_empty ( e, title, 0, 0, e->screen.text_w, e->screen.text_h  );
}

/*! @brief Open a dialog with text in it
*/

gms_dialog *gm_dialog_open_text ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message, bool wrap )
{  //printf ("DEBUG: Starting construction\n");
   gms_dialog *dialog = gm_dialog_open ( e);
   if ( dialog != NULL )
   {  //printf ("DEBUG: before window\n");
      gm_window_add ( dialog, &e->screen, title, x, y, w, h );
      //printf ("DEBUG: before navigation\n");

      gm_navigation_add( dialog, 0, 0, 1, GM_INSTRUCTION_ACCEPT );
      //printf ("DEBUG: before text\n");
      gm_text_add( dialog, message, wrap, false );
   }
   //printf ("DEBUG: Construction complete\n");
   return dialog;


}

/*! @brief Predefined dialog: Show a message at the center of the screen.
*/
gms_dialog *gm_dialog_open_message ( gms_glymmer *e, char *title, char *message )
{
   gms_dialog *dialog = NULL;
   int x = 1;
   int y=e->screen.text_h/3;
   int w=e->screen.text_w-2;

   int h= 2 + gm_anticipate_printwrap_nb_lines(message, w);


   /*dialog = gm_dialog_open ( &e->dstack );
   if ( dialog != NULL )
   {  gm_window_add ( dialog, &e->screen, title, x, y, w, h );
      gm_navigation_add( dialog, 0, 0, 1, WUI_INS_ACCEPT );
      gm_text_add( dialog, message, true, false );
   }*/

   dialog = gm_dialog_open_text ( e, title, x, y, w, h, message, true );

   return dialog;

}

/*! @brief An accept / cancel dialog that only use a accept /cancel butttons. No list
   It is must more compact and elegant, but could be confusing to the user.
   The message string is modified to add accept/cancel which required dynamic deallocation.
   Also consider that the message in this dialog does not use word wrapping, so '\n' must be used.
*/

gms_dialog *gm_dialog_open_confirm ( gms_glymmer *e, char *title, char *message )
{  gms_dialog *dialog = NULL;
   int x = 1;
   int y=e->screen.text_h/3;
   int w=e->screen.text_w-2;

   int h= 4 + gm_count_carriage_return(message);

   dialog = gm_dialog_open ( e );
   if ( dialog != NULL )
   {  gm_window_add ( dialog, &e->screen, title, x, y, w, h );
      gm_navigation_add( dialog, 0, 0, 1, GM_INSTRUCTION_ACCEPT | GM_INSTRUCTION_CANCEL );

      //manipulate message to add accept/cancel
      char *newstr = (char*) malloc ( sizeof(char) * ( strlen(message) + e->screen.text_w ));
      strcpy ( newstr, message ); //the message is shorter.
      strcat (newstr, "\n");
      int padding = (( e->screen.text_w-2 ) / 2 ) -8; //requires a min text width of 18
      //printf ("DEBUG: the padding size if %d", padding);
      for ( int i = 0 ; i < padding; i++) strcat ( newstr, " " );
      strcat (newstr, "Accept / Cancel");
      //printf ("DEBUG: the message string is :\n%s", newstr );
      gm_text_add( dialog, newstr, false, true );
      //TODO: gm_dialog_open_text() can use word wrapping with \n without any problems
   }

   return dialog;
}


/*! @brief Open a dialog with text spread on multiple pages
*/

gms_dialog *gm_dialog_open_pagedtext ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message )
{  gms_dialog *dialog = gm_dialog_open ( e );
   if ( dialog != NULL )
   {  gm_window_add ( dialog, &e->screen, title, x, y, w, h );
      gm_text_add( dialog, message, true, false );
      int page_size = h-2;
      int nb_lines = gm_anticipate_printwrap_nb_lines( message, w );
      int nb_page = ( nb_lines / page_size );
      if ( nb_lines % page_size > 0) nb_page++;
      gm_navigation_add ( dialog, nb_lines, page_size, nb_page, GM_INSTRUCTION_ACCEPT | GM_INSTRUCTION_PGUPDN );
   }
   return dialog;
}

/*! @brief Paged text where the source is a dynamic content
   Basically the game thing as gm_dialog_open_pagedtext except that a flag is set to deallocate
   message string on dialog close.
*/

gms_dialog *gm_dialog_open_dynamic_pagedtext ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message )
{  gms_dialog *dialog = gm_dialog_open_pagedtext ( e, title, x, y, w, h, message);
   if ( dialog != NULL ) dialog->txt.dynamic = true;

   return dialog;
}

/*! @brief Display a list of items and autoadjust size according to content.
   That means that the content can make the dialog exceed the size of the screen.

   @param items[in] list of items to display that MUST BE NULL TERMINATED in the text variable.
*/

gms_dialog *gm_dialog_open_list ( gms_glymmer *e, char *title, int x, int y, char *message, gms_item *items )
{  gms_dialog *dialog = NULL;
   int w=2, h=2, tmpw=0;
   int length = sizeof_list(items);
   /*printf ( "DEBUG sizes are %ld, %ld", sizeof(*items), sizeof (gms_item) );
   printf ( "DEBUG gm_open_dialog_list: Length of the list is %d\nItems are:", length );
   for ( int i = 0 ; i < length ; i++ )
   {  printf ( "[%d] Key=%d, Value=%s\n", i, items[i].key, items[i].text );
   }*/




   if ( items != NULL )
   {  /*if ( message == NULL) dialog = dialog_open ( WUI_COMPONENT_MENU | WUI_COMPONENT_NAVIGATION);
      else dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_MENU | WUI_COMPONENT_NAVIGATION);
         */
      //printf("DEBUG: pass 1\n");
      dialog = gm_dialog_open ( e );
      //if ( dialog == NULL) printf("DEBUG: return value is NULL in pass 1\n");

      if ( dialog != NULL)
      {  /*printf("DEBUG: pass 2\n");
         if ( dialog == NULL) printf("DEBUG: return value is NULL in pass 2\n");*/
         gm_navigation_add( dialog, length, length, 1, GM_INSTRUCTION_ACCEPT | GM_INSTRUCTION_CANCEL | GM_INSTRUCTION_UPDOWN );
         /*printf("DEBUG: pass 3\n");
         if ( dialog == NULL) printf("DEBUG: return value is NULL in pass 3\n");*/
         gm_list_static_add( dialog, items, length) ;
         /*printf("DEBUG: pass 4\n");
         if ( dialog == NULL) printf("DEBUG: return value is NULL in pass 4\n");*/
         //calculate width and length using length of title, message or menu
         if (message != NULL)
         {  tmpw = strlen ( message) + 4; //add border and space before/after
            h++; //TODO: potential bug if a \n is inserted in the question.
                 // could count the number of \n or ^ in the string.
                 //Maybe used wrapped text instead with anticipation of nb of lines
                 //OR add a flag to printf to interpret or not the \n char.
                 //could make 2 wrapper routines for a flag.
            /*printf("DEBUG: pass 5\n");
            if ( dialog == NULL) printf("DEBUG: return value is NULL in pass 5\n");*/
         }
         if ( tmpw > w ) w = tmpw;
         if ( title != NULL) tmpw = strlen (title) + 4;
         if ( tmpw > w ) w = tmpw;
         /*printf("DEBUG: pass 6\n");
         if ( dialog == NULL) printf("DEBUG: return value is NULL in pass 6\n");*/

         for ( int i = 0; i < dialog->lst.length ; i++ )
         {  tmpw = strlen ( dialog->lst.items[i].text ) + 3; //cursor + border
            if ( tmpw > w ) w = tmpw;
            h++;
         }
         /*printf("DEBUG: pass 7\n");
         if ( dialog == NULL) printf("DEBUG: return value is NULL in pass 7\n");*/

         gm_window_add ( dialog, &e->screen, title, x, y, w, h );

         /*printf("DEBUG: pass 8\n");
         if ( dialog == NULL) printf("DEBUG: return value is NULL in pass 8\n");*/

         if ( message != NULL ) gm_text_add( dialog, message, false, false );

         /*printf("DEBUG: pass 9\n");
         if ( dialog == NULL) printf("DEBUG: return value is NULL in pass 9\n");*/
      }
   }
   //if ( dialog == NULL) printf("DEBUG: return value is NULL\n");
   return dialog;

}

/*! @brief Display a list of predefined size that can be paged

   @param items[in] list of items to display that MUST BE NULL TERMINATED in the text variable.
*/

gms_dialog *gm_dialog_open_pagedlist ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message, gms_item *items )
{
   gms_dialog *dialog = NULL;
   int page_size = h - 2;
   int length = sizeof_list (items);

   if ( items != NULL )
   {  gms_dialog *dialog = gm_dialog_open ( e );

      /*if ( message == NULL) dialog = dialog_open ( WUI_COMPONENT_MENU | WUI_COMPONENT_NAVIGATION);
      else
      {  dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_MENU | WUI_COMPONENT_NAVIGATION);
         page_size = h - 3;
      }*/
      if ( message != NULL ) page_size = h - 3;

      if ( dialog != NULL)
      {  int nb_page = length / page_size;
         if ( length % page_size > 0 ) nb_page++;
         gm_navigation_add( dialog, length, page_size, nb_page,
                         GM_INSTRUCTION_ACCEPT | GM_INSTRUCTION_CANCEL | GM_INSTRUCTION_UPDOWN | GM_INSTRUCTION_PGUPDN );
         gm_list_static_add ( dialog, items, length );
         gm_window_add ( dialog, &e->screen, title, x, y, w, h );

         if ( message != NULL ) gm_text_add( dialog, message, false, false );
      }
   }
   return dialog;
}

gms_dialog *gm_dialog_open_question_yesno ( gms_glymmer *e, char *title, char *message )
{  gms_dialog *dialog = NULL;
   int x = 1;
   int y=e->screen.text_h/3;
   int w=e->screen.text_w-2;
   int h= 4 + gm_anticipate_printwrap_nb_lines(message, w);

   dialog = gm_dialog_open ( e);
   if ( dialog != NULL )
   {  gm_navigation_add( dialog, 2, 2, 1, GM_INSTRUCTION_ACCEPT | GM_INSTRUCTION_CANCEL | GM_INSTRUCTION_UPDOWN );
      gm_list_static_add( dialog, yesno_question_items, sizeof_list(yesno_question_items) );
      gm_window_add ( dialog, &e->screen, title, x, y, w, h );
      gm_text_add( dialog, message, true, false );
   }

   return dialog;
}

gms_dialog *gm_dialog_open_dynamic_pagedlist ( gms_glymmer *e, char *title, int x, int y, int w, int h, char *message )
{  gms_dialog *dialog = NULL;
   int page_size = h - 2;
   if ( message != NULL ) page_size = h - 3;

   dialog = gm_dialog_open ( e );

   if ( dialog != NULL)
   {  gm_list_dynamic_add ( dialog );
      gm_navigation_add( dialog, 0, page_size, 1,
                      GM_INSTRUCTION_ACCEPT | GM_INSTRUCTION_CANCEL | GM_INSTRUCTION_UPDOWN | GM_INSTRUCTION_PGUPDN );
      gm_window_add ( dialog, &e->screen, title, x, y, w, h );

      if ( message != NULL ) gm_text_add( dialog, message, false, false );
   }

   return dialog;
}

gms_dialog *gm_dialog_open_question_lr ( gms_glymmer *e, char *title, char *message )
{  //TODO: Complete this dialog with new navigation. Not sure already have the confirm dialog
   //which does not duplicate input methods
   //NOTE: hide warning, does nothing
   e+=0;
   title+=0;
   message+=0;

   return NULL;
}


#ifdef GM_ADD_DATABASE_SUPPORT

//TODO: How to determine the answer from PK field when actually joining multiple tables. Maybe
// you could create a field named AS 'answer' which is not printed but used in the list. If not
// defined, it would return -1 as answer choice. This could give flexibility on which answer to return.
//Maybe choices could be a separate query than the print query. Bit it requires more query.

//TODO: Old code from WUI. Adapt once database support is added.
s_wui_ecs_dialog *dialog_open_database_menu ( char *title, int x, int y, char *header,
                                             char *format, char *query )
{  s_wui_ecs_dialog *dialog = NULL;
   int w=2, h=2, tmpw=0;

   if ( header == NULL)
   {  dialog = dialog_open ( WUI_COMPONENT_DATA | WUI_COMPONENT_NAVIGATION);
   }
   else dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_DATA | WUI_COMPONENT_NAVIGATION);
   if ( dialog != NULL)
   {  //TODO: calculate length
      //navigation_init( &dialog->nav, length, 1, WUI_INS_ACCEPT | WUI_INS_CANCEL | WUI_INS_UPDOWN );
      data_init ( &dialog->dat, format, query, true );
      //calculate width and length using length of title, message or menu
      if (header != NULL)
      {  tmpw = strlen ( header) + 4; //add border and space before/after
         h++; //TODO: potential bug if a \n is inserted in the question.
              // could count the number of \n or ^ in the string.
              //Maybe used wrapped text instead with anticipation of nb of lines

      }
      if ( tmpw > w ) w = tmpw;
      if ( title != NULL) tmpw = strlen (title) + 4;
      if ( tmpw > w ) w = tmpw;

      //TODO: Calculate the width and height according to a test querying
      /*for ( int i = 0; i < dialog->mnu.length ; i++ )
      {  tmpw = strlen ( dialog->mnu.items[i].text ) + 3; //cursor + border
         if ( tmpw > w ) w = tmpw;
         h++;
      }*/

      window_init ( &dialog->win, title, x, y, w, h );

      if ( header != NULL ) text_init( &dialog->txt, &dialog->win, header, false );
   }

   return dialog;
}
s_wui_ecs_dialog *dialog_open_database_list ( char *title, int x, int y, int w, int h,
                                             char *header, char *format, char *query )
{  s_wui_ecs_dialog *dialog = NULL;
   int page_size = h - 2;

   if ( header == NULL)
   {  dialog = dialog_open ( WUI_COMPONENT_NAVIGATION | WUI_COMPONENT_DATA);
   }
   else
   {  dialog = dialog_open ( WUI_COMPONENT_TEXT | WUI_COMPONENT_NAVIGATION | WUI_COMPONENT_DATA);
      page_size = h - 3;
   }

   if ( dialog != NULL)
   {  //TODO: Calculate length
      //int nb_page = length / page_size;
      int nb_page = 1;
      //if ( length % page_size > 0 ) nb_page++;
      navigation_init( &dialog->nav, page_size, nb_page,
                      GM_INSTRUCTION_ACCEPT | GM_INSTRUCTION_CANCEL | GM_INSTRUCTION_UPDOWN | GM_INSTRUCTION_LEFTRIGHT );
      data_init ( &dialog->dat, format, query, true );

      window_init ( &dialog->win, title, x, y, w, h );

      if ( header != NULL ) text_init( &dialog->txt, &dialog->win, header, false );
   }

   return dialog;
}
#endif

