/**
   Glymmer

   @author: Eric Pietrocupo
   @date: June 11th, 2022
   @license: Apache 2.0 License
*/

#include <stdbool.h>
#include <allegro.h>
#include <stdio.h>
#include <gm_struct.h>
#include <gm_input.h>
#include <gm_common.h>

/*! @brief Verify and read a key from the keyboard
*/

void read_keyboard ( gms_input *input)
{  input->kb_ascii = 0;
   input->kb_scancode = 0; // Key code 0 will be used as no input, not key is attached to that value
   if ( keypressed() )
   {  int tmpkey = readkey();

      input->kb_scancode = tmpkey >> 8;
      input->kb_ascii = tmpkey & 0xff;
      //printf ( "DEBUG Read key: %d : %d\n", input->kb_scancode, input->kb_ascii );
      input->modified += GM_INPUT_KEYBOARD;
      //old code from wizardry legacy, related to task switching. Maybe disable input
      //when a task switching input is made like ATL+TAB
      //TODO: Verify if there is a bug with the task switching and implement safeguard like in Wizardry Legacy
      // Very GL specific
      /*if ( switch_redraw == true)
      {
         switch_redraw = false;
         return ( -1 );
      }
      */
   }

}

/*! @brief source bitfield reading encapsulation
*/

bool peripherals_to_read ( unsigned char to_read, unsigned char to_verify )
{  return ( ( to_read & to_verify ) > 0);
}

/*! @brief Reads specified input source
   Can read the keyboard, joystick or mouse. The user can specify which input to consider.
   This method is considered the main loop, so it will loop until a valid input is found.
   Mouse is more tricky as a change is hovering will trigger a new input. In fact anything
   that should trigger a refresh of the screen besides mouse movement will break the loop.

   The programmer does not need to use this routine if for example he is using a realtime
   engine. He can fill up the input structure himself and supply it to the glymmer engine.

   @param input will contains the status of all inputs to be considered by the engine for
                redrawing the screen.
   @param source a bitfield containing the sources to read. Use appropriate contants
*/

void gm_read_input ( gms_input *input )
{  input->modified = 0;
   //printf ("DEBUG gm_read_input() peripherals = %d\n", input->peripherals );
   assert_or_exit( input->peripherals > 0 && input->peripherals <= GM_INPUT_MAX_VALUE, 1,
                  "gm_read_input(): no peripherals has been defined for reading");
   /*if ( input->peripherals == 0 || input->peripherals > GM_INPUT_MAX_VALUE )
   {  printf ( "ERROR: gm_read_input(): no peripherals has been defined for reading\n" );
      exit(1);
   }*/

   while ( input->modified == 0 )
   {  if ( peripherals_to_read( input->peripherals, GM_INPUT_KEYBOARD) )
      {  //printf ( "DEBUG Reading keyboard\n" );
         read_keyboard ( input );
      }
      //TODO: Add joystick support

   }


}

/*! @brief Initialise input structure and reset peripherals
*/
//void gm_reset_input ( gms_input *input, bool read_keyboard, bool read_mouse, bool read_joystick )
void gm_reset_input ( gms_input *input, unsigned char peripherals )
{  clear_keybuf();

   input->peripherals = peripherals;
/*   input->peripherals = 0;
   if ( read_keyboard ) input->peripherals += GM_INPUT_KEYBOARD;
   if ( read_mouse ) input->peripherals += GM_INPUT_MOUSE;
   if ( read_joystick ) input->peripherals += GM_INPUT_JOYSTICK;*/
   //printf ("DEBUG gm_reset_input(): peripherals = %d\n", input->peripherals );
   assert_or_exit( input->peripherals > 0 && input->peripherals <= GM_INPUT_MAX_VALUE, 1,
                  "gm_reset_input(): no peripherals has been defined for reading");
   /*if ( input->peripherals == 0 || input->peripherals > GM_INPUT_MAX_VALUE )
   {  printf ( "ERROR: gm_reset_input(): no peripherals has been defined for reading\n" );
      exit(1);
   }*/

   input->modified = 0;
   input->kb_ascii = 0;
   input->kb_scancode = 0;
}

/*! @brief Shortcut function to wait for a specific key to be pressed
   Should be mostly used for debugging purpose as it only consider the keyboard

   @param scancode of the key to wait for
*/

void gm_wait_for_keypress ( unsigned char scancode )
{  gms_input input;
   gm_reset_input( &input, GM_INPUT_KEYBOARD);
   do
   {  gm_read_input( &input);
   }
   while ( input.kb_scancode != scancode );
}
