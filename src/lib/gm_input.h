/*!
   Glymmer

   @author: Eric Pietrocupo
   @date: January 22th, 2022
   @license: Apache 2.0 License

   Mainloop input reading routine.
*/

#ifndef GM_INPUT_H_INCLUDED
#define GM_INPUT_H_INCLUDED


#ifdef __cplusplus
extern "C"
{
#endif

#define GM_INPUT_KEYBOARD  0b00000001
#define GM_INPUT_JOYSTICK  0b00000010
#define GM_INPUT_MOUSE     0b00000100
/*! In order to determine if a peripheral configuration is valid
    the value should range between 1 and GM_INPUT_MAX_VALUE
*/
#define GM_INPUT_MAX_VALUE 0b00000111

//TODO: Like struct, maybe put all constants into a separate header. THen could put all functions
//into a separate header. That should dramatically reduce headers for the user only, not internal
//functions and definition. In other word, anything that has a GM in front of it.

/*
// game input constants
#define GM_INPUT_SELECT       1  //! Accepting a choice
#define GM_INPUT_CANCEL       2  //! Moving back, cancelling a choice
#define GM_INPUT_UP           3  //! Move up
#define GM_INPUT_DOWN         4  //! Move down in the menu
#define GM_INPUT_RIGHT        5  //! Move right: Various behaviors
#define GM_INPUT_LEFT         6  //! Move left: Various behaviors
#define GM_INPUT_SCREENSHOT   7  //! Take a screen shot
//#define GM_INPUT_HELP         8  //! Invoque help for that dialog
//#define GM_INPUT_SORT         9  //! Change sorting field for database dialog
//#define GM_INPUT_MENU         10 //! Invoke the top game menu if elsewhere
*/
//TODO: L/R can have multiple usage: Menu navigation, value adjustments, Jump pages, show more data
//TODO: Y/N dialog could use left right or accept cancel button instead of menu. Faster.

void gm_read_input ( gms_input *input );
//void gm_reset_input ( gms_input *input, bool read_keyboard, bool read_mouse, bool read_joystick );
void gm_reset_input ( gms_input *input, unsigned char peripherals );
void gm_wait_for_keypress ( unsigned char scancode );

//TODO INPUT CONVERSION add function to convert peripheral input into glymmer input like:
// accept, cancel, help, page up, page down, up, down, left, right, menu, etc
// lot of input like the mouse is context sensitive to the content of the text buffer, so it
// might not be possible to do an isolated input conversion function without access to the hot
// spot of the text buffer. PROBLEM many are context sensitive


#ifdef __cplusplus
}
#endif

#endif // GM_INPUT_H_INCLUDED
