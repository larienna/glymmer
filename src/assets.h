/**
   assets.h

   @author Eric Pietrocupo
   @since  January 2021
   license GNU General Public License

   This module is now a wrapping around allegro datafiles. Instead of using indexes to access
   individual assets, full name will be used, and assets will be searched an set before
   getting used. This will prevent breaking when objetcs have moved around in the datafile
   also allowing more flexibility. So the implementation will be hidden behind functions for
   faster access.

   Also, assets have been masively reduced, so there will not be adventure datafile anymore,
   everything will be a system datafile, and adventures will save texture file names into their
   databases. The only problem that can occur is if a file with a specific name is removed. In
   in that case a dummy file is returned instead of NULL. Remember that assets name are
   capitalised and dots are replaced by underscore so the following conversion will occur.

   file_name.bmp     -> FILE_NAME_BMP

   The datafiles themselves are privately hidden in the module you need to use accessor methods
   below to get the information. One of the objective is to keep the asset call small and avoid tons
   of indirections.

   Also take note that loading datafiles must be done after the color depth has been set. So
   graphic mode must be initialised first.

*/

#ifndef ASSETS_H_INCLUDED
#define ASSETS_H_INCLUDED

#ifdef __cplusplus
extern "C"
{
#endif


/** The first this to do is to load datafiles. Module user are not aware of how many data
files there is to load as this can change. So load_datafiles will load all datafiles and
return false if there has been a problem during loading. In that case, you should abort the
game. Unload_datafiles will be used when the game is closed.

Note that load datafile will print on the screen the name of the datafile currently loaded.
This is another reason why graphic ode needs to be activated.

*/
bool load_game_datafiles ( void );
void unload_game_datafiles ( void );

/*Another important thing to do at file creation is to create dummy assets. Those are
assets that are returned when an asset is missing to avoid null pointers. All asset accessor
methods will return one of those assets.  If a dummy cannot be returned, it will have no effect,
like it's the case when playing music and sounds. Must also be called after graphic mode is
enabled. There is also a method to free those assets at game close. build_default_assets return
false if there is a problem creating those assets which should terminate the program.
*/

bool build_default_assets ( void );
void free_default_assets ( void );

/* one datafile loading has been made, you can display which datafile has been loaded.
This can be used either in a successful or failed datafile loading. It will log loading
information on the console.
*/

void print_loaded_datafiles ( void );

/* ---------- BITMAP Accessor Methods ----------

   this is the list of routine that programmer can use to access assets. The naming convention
   is used to make it short to call since assets access have been previously pretty long with
   lot of indirection. There will be methods for each datafile. Using those methods implies that
   there is a search algorith called after each function call. So better get a copy of those
   asset pointers once, and then reuse them.

   Remember that asset name are all in caps and dots are replaced by underscore. The object
   name is the only parameter. If the file cannot be found, the default asset is returned to
   prevent NULL pointers.

*/

BITMAP *asset_monster ( const char *name );
BITMAP *asset_texture ( const char *name );
BITMAP *asset_editor  ( const char *name );
BITMAP *asset_image   ( const char *name );
BITMAP *asset_object  ( const char *name );

/* ---------- FONT accessor methods ----------
   Accessor used to get fonts. The system font it used if the font is missing.

*/

FONT *asset_font ( const char *name );


/* ---------- Audio assets methods ----------

   since audio assets can only be used to be played, there is no need to keep a pointer on
   a MIDI or a SAMPLE. So instead, the accessor will play the sounds or do nothing if missing.
*/

void asset_play_music ( const char *name, bool loop );
void asset_play_sound ( const char *name );

BITMAP* asset_default_bitmap256 ( void );
BITMAP* asset_default_bitmap160 ( void );
BITMAP* asset_default_bitmap16 ( void );


/* ---------- Iterators ----------
   In certain rare situations, more precisely when using the editor, you will need to pass
   through an entire datafile for the user to select a texture for example. In that case, you
   need to access the datatile directly using the index without overflowing. This is only
   useful for images, so only BITMAP* are returned.

   A special iterator object will be used to simplify the access once it is initialised.
   Here is the structure of the iterator
*/

typedef struct s_datafile_iterator
{  int datafileid; //identification number the datafile to read.
   int index;      //Entry number that was last read.
   //int nb_entry;   //This is the maximum number of entries available in the datafile
}s_datafile_iterator;

/* Datafiles have an ID, I will put here only the public ID that are likely to be used in the
   map editor.
*/

#define DATBMP_MONSTER  0
#define DATBMP_OBJECT   5
#define DATBMP_TEXTURE  6

/* You will need method to create an iterator in the right datafile using the datafile ID.
   It's more an initialisation than a creation. Pass your own iterator structure. The
   index will be positionned at 0 by default.
*/

void init_iterator ( s_datafile_iterator *iter, int datafileid );

/* Then you need to use the following method to get the BITMAP using the current iterator
   position. Inc_iterator will increment the iterator if not already at the end of the datafile.
   The incrementor will return false if the end of datafile is reached.
*/

BITMAP* get_iterator_asset ( s_datafile_iterator *iter );
bool inc_iterator ( s_datafile_iterator *iter );

/* Considering the WUI will require an index as an answer, it might be more likely to access
datafiles using the index. Again, it's mainly for the editor, and some adjustments could be
required. Here is a routine that returns a BITMAP from a specific datafileid using an index.
Be warned that it will not protect you from invalid index, as there is no way to know the nb
of elements in a datafile.
*/

BITMAP* asset_bitmap_byindex ( int datafileid, int index );




//TODO: quick fix to make the old UI code compile.
extern FONT *text_font;



#ifdef __cplusplus
}
#endif

#endif // ASSETS_H_INCLUDED
