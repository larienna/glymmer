#!/bin/bash
rm -r glyph/
wget -r -np -A bmp --cut-dirs=2 -nH --tries=3 http://lariennalibrary.com/ArtifactDepot/glymmer/glyph/

cd glyph/
#loop through all folders and pack glyph using folder names.
for f in *; do
    if [ -d "$f" ]; then
        echo "Packing $f"
	rm "../$f.bmp"
       ../gpack -c 16 -w 10 -h 15 -l 2 "../$f.bmp" "$f/"
    fi
done


#./gpack -c 16 -w 10 -h 15 -l 2 GreenGold.bmp glyph/GreenGold/
#./gpack -c 16 -w 10 -h 15 -l 1 GameIcon.bmp glyph/GameIcon/
